package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * Classe che permette di creare un pannello con immagine di sfondo
 */
class PanelWithBackground extends JPanel {
	
	private static final long serialVersionUID = -5955818218147935374L;
	
	/**
	 * immagine di sfondo che avrà il pannello
	 */
	private Image imgSfondo;

	/**
	 * Costruttore
	 * @param imgSfondo: l'immagine che sarà settata come sfondo
	 */
	public PanelWithBackground(Image imgSfondo) {
		super();
		this.imgSfondo = imgSfondo;
		}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imgSfondo, 0, 0, this);
		}
	}