package view;

import javax.swing.JButton;

/**
 * Bottone usato per le strade, ha un bordo rotondo e contiene l'id della strada e una variabile 
 * booleana che indica se la strada è libera o no 
 */
public class BottoneStrada extends JButton {


	private static final long serialVersionUID = 3558014061477435378L;
	
	/**
	 * Id relativo alla strada
	 */
	private final String id; 
	
	/**
	 * Variabile booleana che indica se la strada è libera o no
	 */
	private boolean libero = true;

	/**
	 * costruttore
	 * @param id: id della strada
	 */
	public BottoneStrada(String id) {
		super();
		this.id = id;  
		setBorder(new BordoRotondo()); 
	}

	public String getId() {
		return this.id;
	}

	public boolean getLibero() {
		return this.libero; 
	}

	public void setLiberoFalse() {
		this.libero = false; 
	}

}
