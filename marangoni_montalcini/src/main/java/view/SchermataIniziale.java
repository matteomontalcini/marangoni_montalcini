package view;

import it.polimi.marangoni_montalcini.Gioco;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import rete.ClientRMI;
import rete.ImplementazioneClientSocket;
import rete.RaccoltaSocket;
import rete.ServerRMI;

public class SchermataIniziale extends Observable{

	class ListenerConferma implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			indice = selezioneNumeroGiocatori.getSelectedIndex();
			if(indice == 0) {
				scelta = DUEGIOCATORI;
			} else {
				if(indice ==1) {
					scelta = TREGIOCATORI;
				} else {
					scelta = QUATTROGIOCATORI;
				}
			}
			panelNumeroGiocatori.setVisible(false);
			panelNumeroGiocatori.add(numeroGiocatori, BorderLayout.PAGE_START);
			panelNumeroGiocatori.add(selezioneNumeroGiocatori, BorderLayout.CENTER);
			panelNumeroGiocatori.add(conferma, BorderLayout.PAGE_END);
			labelImmagine.add(panelNumeroGiocatori);

			panelInserimentoLocale = new JPanel(new BorderLayout());
			panelInserimentoLocale.setBounds(240, 290, 500, 180);
			panelInserimentoLocale.setBackground(Color.WHITE);
			nomiGiocatori = new JPanel(new GridLayout(4,2));


			model1 = new UtilDateModel();
			JDatePanelImpl datePanel1 = new JDatePanelImpl(model1);
			final JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1);
			datePicker1.setBackground(Color.WHITE);

			model2 = new UtilDateModel();
			JDatePanelImpl datePanel2 = new JDatePanelImpl(model2);
			final JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2);
			datePicker2.setBackground(Color.WHITE);

			model3 = new UtilDateModel();
			JDatePanelImpl datePanel3 = new JDatePanelImpl(model3);
			final JDatePickerImpl datePicker3 = new JDatePickerImpl(datePanel3);
			datePicker3.setBackground(Color.WHITE);


			model4 = new UtilDateModel();
			JDatePanelImpl datePanel4 = new JDatePanelImpl(model4);
			final JDatePickerImpl datePicker4 = new JDatePickerImpl(datePanel4);
			datePicker4.setBackground(Color.WHITE);

			panelDate = new JPanel(new GridLayout(4,2));
			panelDate.setBackground(Color.WHITE);
			panelDate.add(datePicker1);
			panelDate.add(datePicker2);


			nomiGiocatori.setBackground(Color.WHITE);
			nomeGiocatore1 = new JLabel("Nome Giocatore 1: ");
			scriviNome1 = new JTextField(10);
			controlPanel1 = new JPanel();
			controlPanel1.setBackground(Color.WHITE);
			controlPanel1.setName("Giocatore1");
			controlPanel1.add(nomeGiocatore1);
			controlPanel1.add(scriviNome1);
			nomiGiocatori.add(controlPanel1);
			nomeGiocatore2 = new JLabel("Nome Giocatore 2: ");
			scriviNome2 = new JTextField(10);
			controlPanel2 = new JPanel();
			controlPanel2.setBackground(Color.WHITE);
			controlPanel2.setName("Giocatore2");
			controlPanel2.add(nomeGiocatore2);
			controlPanel2.add(scriviNome2);
			nomiGiocatori.add(controlPanel2);
			controlPanel3 = new JPanel();
			controlPanel3.setBackground(Color.WHITE);
			controlPanel4 = new JPanel();
			controlPanel4.setBackground(Color.WHITE);
			if(scelta.equals(TREGIOCATORI)) {
				nomeGiocatore3 = new JLabel("Nome Giocatore 3: ");
				scriviNome3 = new JTextField(10);

				controlPanel3.setName("Giocatore3");
				controlPanel3.add(nomeGiocatore3);
				controlPanel3.add(scriviNome3);
				nomiGiocatori.add(controlPanel3);
				panelDate.add(datePicker3);
			}
			if(scelta.equals(QUATTROGIOCATORI)){
				nomeGiocatore3 = new JLabel("Nome Giocatore 3: ");
				scriviNome3 = new JTextField(10);

				controlPanel3.setName("Giocatore3");
				controlPanel3.add(nomeGiocatore3);
				controlPanel3.add(scriviNome3);
				nomiGiocatori.add(controlPanel3);
				panelDate.add(datePicker3);

				nomeGiocatore4 = new JLabel("Nome Giocatore 4: ");
				scriviNome4 = new JTextField(10);

				controlPanel4.setName("Giocatore4");
				controlPanel4.add(nomeGiocatore4);
				controlPanel4.add(scriviNome4);
				nomiGiocatori.add(controlPanel4);
				panelDate.add(datePicker4);
			}

			//questo bottone deve scatenare l'inizio partita 
			aggiungiGiocatori = new JButton("Aggiungi Giocatori");

			class ListenerAggiuntaGiocatori implements ActionListener {
				public void actionPerformed(ActionEvent arg0) {
					boolean okContinuare=true, nomiRiempiti = true, spazioNonUsato = true, nomiDiversi = true, dataSettata = true, caratteriAmmessi = true, dataMinoreOdierna = true;
					Date dataOdierna = new Date();
					switch(scelta){
					case DUEGIOCATORI:
						if (scriviNome1.getText().isEmpty() || scriviNome2.getText().isEmpty()) {
							okContinuare = false;
							nomiRiempiti = false;
						} else if(scriviNome1.getText().startsWith(" ") || scriviNome2.getText().startsWith(" ")) {
							okContinuare = false;
							spazioNonUsato = false;
						} else if(scriviNome1.getText().equals(scriviNome2.getText())){
							okContinuare = false;
							nomiDiversi = false;
						} else if(!controlloCaratteriAmmessi(scriviNome1.getText()) || !controlloCaratteriAmmessi(scriviNome2.getText())){
							okContinuare = false;
							caratteriAmmessi = false;
						}
						if(datePicker1.getModel().getValue() == null || datePicker2.getModel().getValue() == null){
							okContinuare = false;
							dataSettata = false;
						} else if(dataOdierna.before((Date) datePicker1.getModel().getValue()) || dataOdierna.before((Date) datePicker2.getModel().getValue())) {
							okContinuare = false;
							dataMinoreOdierna = false;
						}

						break;
					case TREGIOCATORI:
						if (scriviNome1.getText().isEmpty() || scriviNome2.getText().isEmpty() || scriviNome3.getText().isEmpty()){
							okContinuare = false;
							nomiRiempiti = false;
						} else if(scriviNome1.getText().startsWith(" ") || scriviNome2.getText().startsWith(" ") || scriviNome3.getText().startsWith(" ")) {
							okContinuare = false;
							spazioNonUsato = false;
						} else if(scriviNome1.getText().equals(scriviNome2.getText()) || scriviNome1.getText().equals(scriviNome3.getText()) || scriviNome2.getText().equals(scriviNome3.getText())){
							okContinuare = false;
							nomiDiversi = false;
						} else if(!controlloCaratteriAmmessi(scriviNome1.getText()) || !controlloCaratteriAmmessi(scriviNome2.getText()) || !controlloCaratteriAmmessi(scriviNome3.getText())){
							okContinuare = false;
							caratteriAmmessi = false;
						}
						if(datePicker1.getModel().getValue() == null || datePicker2.getModel().getValue() == null || datePicker3.getModel().getValue() == null){
							okContinuare = false;
							dataSettata = false;
						} else if(dataOdierna.before((Date) datePicker1.getModel().getValue()) || dataOdierna.before((Date) datePicker2.getModel().getValue()) || dataOdierna.before((Date) datePicker3.getModel().getValue())) {
							okContinuare = false;
							dataMinoreOdierna = false;
						}
						break;
					case QUATTROGIOCATORI:
						if (scriviNome1.getText().isEmpty() || scriviNome2.getText().isEmpty() || scriviNome3.getText().isEmpty() || scriviNome4.getText().isEmpty()){
							okContinuare = false;
							nomiRiempiti = false;
						}else if(scriviNome1.getText().startsWith(" ") || scriviNome2.getText().startsWith(" ") || scriviNome3.getText().startsWith(" ") || scriviNome4.getText().startsWith(" ")) {
							okContinuare = false;
							spazioNonUsato = false;
						} else if(scriviNome1.getText().equals(scriviNome2.getText()) || scriviNome1.getText().equals(scriviNome3.getText()) ||scriviNome1.getText().equals(scriviNome4.getText()) || scriviNome2.getText().equals(scriviNome3.getText()) || scriviNome2.getText().equals(scriviNome4.getText())){
							okContinuare = false;
							nomiDiversi = false;
						} else if(!controlloCaratteriAmmessi(scriviNome1.getText()) || !controlloCaratteriAmmessi(scriviNome2.getText()) || !controlloCaratteriAmmessi(scriviNome3.getText()) || !controlloCaratteriAmmessi(scriviNome4.getText())){
							okContinuare = false;
							caratteriAmmessi = false;
						}
						if(datePicker1.getModel().getValue() == null || datePicker2.getModel().getValue() == null || datePicker3.getModel().getValue() == null || datePicker4.getModel().getValue() == null){
							okContinuare = false;
							dataSettata = false;
						} else if(dataOdierna.before((Date) datePicker1.getModel().getValue()) || dataOdierna.before((Date) datePicker2.getModel().getValue()) || dataOdierna.before((Date) datePicker3.getModel().getValue()) || dataOdierna.before((Date) datePicker4.getModel().getValue())) {
							okContinuare = false;
							dataMinoreOdierna = false;
						}
						break;
					default: 
						break; 
					}

					if(okContinuare) {
						 
						nomi.add(scriviNome1.getText());
						if((Date) datePicker1.getModel().getValue() != null){
							dateGiocatori.add((Date) datePicker1.getModel().getValue());
						} else {
							dateGiocatori.add(null);
						}
						nomi.add(scriviNome2.getText()); 
						if((Date) datePicker2.getModel().getValue() != null){
							dateGiocatori.add((Date) datePicker2.getModel().getValue());
						} else {
							dateGiocatori.add(null);
						}
						if(scelta.equals(TREGIOCATORI)) {
							nomi.add(scriviNome3.getText());
							if((Date) datePicker3.getModel().getValue() != null){
								dateGiocatori.add((Date) datePicker3.getModel().getValue());
							} else {
								dateGiocatori.add(null);
							}
						}
						if(scelta.equals(QUATTROGIOCATORI)) {
							nomi.add(scriviNome3.getText());
							if((Date) datePicker3.getModel().getValue() != null){
								dateGiocatori.add((Date) datePicker3.getModel().getValue());
							} else {
								dateGiocatori.add(null);
							}
							nomi.add(scriviNome4.getText()); 
							if((Date) datePicker4.getModel().getValue() != null){
								dateGiocatori.add((Date) datePicker4.getModel().getValue());
							} else {
								dateGiocatori.add(null);
							}
						}
						
						panelInserimentoLocale.setVisible(false);
						mainFrame.setVisible(false);
						mainFrame.setEnabled(false);

						gioco.giocoLocale();
					} else {
						if(!nomiRiempiti){
							JOptionPane.showMessageDialog(null,
									"Inserire i nomi mancanti.",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						} else if(!spazioNonUsato) {
							JOptionPane.showMessageDialog(null,
									"Eliminare il carattere 'spazio' dai nomi.",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						} else if(!nomiDiversi) {
							JOptionPane.showMessageDialog(null,
									"Inserire nomi diversi.",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						} else if(!caratteriAmmessi) {
							JOptionPane.showMessageDialog(null,
									"I caratteri di seguito mostrati non sono ammessi: (; . , ; : ' | £ % / = ^ ç @ §)",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						} else if(!dataSettata){
							JOptionPane.showMessageDialog(null,
									"Inserire tutte le date, se non si è mai accarezzata una pecora impostare propria data di nascita.",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						} else if(!dataMinoreOdierna) {
							JOptionPane.showMessageDialog(null,
									"Una delle date inserite non è corretta.",
									"Errore",
									JOptionPane.ERROR_MESSAGE,
									new ImageIcon(this.getClass().getResource("/errore.png")));
						}

					}
				}

			}

			ListenerAggiuntaGiocatori listenerAggiuntaGiocatori = new ListenerAggiuntaGiocatori(); 
			aggiungiGiocatori.addActionListener(listenerAggiuntaGiocatori); 

			panelNomiEDate = new JPanel();
			panelNomiEDate.setBounds(20,20,600,600);
			panelNomiEDate.setLayout(new BorderLayout());
			panelNomiEDate.add(nomiGiocatori, BorderLayout.LINE_START);
			panelNomiEDate.add(panelDate, BorderLayout.LINE_END);
			panelNomiEDate.setBackground(Color.WHITE);


			panelIndicazione = new JPanel();
			panelIndicazione.setLayout(new BorderLayout());

			labelIndicazione = new JLabel();
			labelIndicazione.setText("Inserire i nomi dei giocatori e la data in cui hanno accarezzato");
			labelIndicazione.setHorizontalAlignment(SwingConstants.CENTER);
			labelIndicazione.setVisible(true);

			labelIndicazione1 = new JLabel();
			labelIndicazione1.setText("per l'ultima volta una pecora.");
			labelIndicazione1.setHorizontalAlignment(SwingConstants.CENTER);
			labelIndicazione1.setVisible(true);

			panelIndicazione.add(labelIndicazione,BorderLayout.PAGE_START);
			panelIndicazione.add(labelIndicazione1, BorderLayout.PAGE_END);
			panelIndicazione.setBackground(Color.WHITE);

			panelInserimentoLocale.add(panelIndicazione, BorderLayout.PAGE_START);
			panelInserimentoLocale.add(panelNomiEDate, BorderLayout.CENTER);
			panelInserimentoLocale.add(aggiungiGiocatori, BorderLayout.PAGE_END);
			labelImmagine.add(panelInserimentoLocale);

			panelNumeroGiocatori.setVisible(false);
			panelInserimentoLocale.setVisible(true);

		}
	}



	private JFrame mainFrame;
	private JPanel panel;
	private JPanel panelInserimentoLocale, panelInserimentoRemoto, panelNumeroGiocatori, localeORemoto, controlPanel1, controlPanel2, controlPanel3, controlPanel4, nomiGiocatori, panelNomiEDate, panelDate, panelIndicazione, panelSceltaRuolo, panelNumeroGiocatoriOnline, panelIndirizzoIP;
	private JLabel numeroGiocatori,nomeGiocatore, nomeGiocatore1, nomeGiocatore2, nomeGiocatore3, nomeGiocatore4, numeroGiocatoriOnlineRMI, numeroGiocatoriOnlineServerSocket; 
	private JComboBox selezioneNumeroGiocatori, selezioneNumeroGiocatoriOnlineRMI, selezioneNumeroGiocatoriOnlineServerSocket;
	private JLabel labelImmagine, labelIndicazione, labelIndicazione1;
	private JButton bottoneInizio, bottoneGiocoLocale, bottoneGiocoRemoto, aggiungiGiocatori, conferma, confermaServer, confermaClient, confermaOnline;
	static final String DUEGIOCATORI = "2 giocatori";
	static final String TREGIOCATORI = "3 giocatori";
	static final String QUATTROGIOCATORI = "4 giocatori";
	private JTextField scriviNome, scriviNome1, scriviNome2, scriviNome3, scriviNome4, inserimentoNomeServer; 
	private String scelta;
	private int indice;
	private boolean schermataInizialeAttiva = true; 
	private List<String> nomi = new ArrayList<String>(); 
	private List<Date> dateGiocatori = new ArrayList<Date>();
	private UtilDateModel model1, model2, model3, model4;
	private ListenerConferma listenerConferma = new ListenerConferma(); 
	private Gioco gioco; 
	private static boolean sceltaSocket; 
	private static final Logger LOGGER = Logger.getLogger("view"); 


	public SchermataIniziale(Gioco gioco) {
		this.gioco = gioco; 

		panel = new JPanel();
		mainFrame = new JFrame();
		labelImmagine = new JLabel();
		labelImmagine.setLayout(null);
		labelImmagine.setIcon(new ImageIcon(this.getClass().getResource("/initialscreen.jpg")));
		bottoneInizio = new JButton("Gioca!");
		bottoneInizio.setBounds(380, 355, 200, 60); 
		bottoneInizio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bottoneInizio.setVisible(false);
				localeORemoto.setVisible(true);   
			}
		});
		labelImmagine.add(bottoneInizio);


		panelNumeroGiocatoriOnline = new JPanel(new BorderLayout());
		panelNumeroGiocatoriOnline.setBounds(280, 330, 500, 100);
		panelNumeroGiocatoriOnline.setBackground(Color.WHITE);

		numeroGiocatoriOnlineRMI = new JLabel("Seleziona numero giocatori: ");
		numeroGiocatoriOnlineRMI.setFont(new Font("", Font.BOLD,14));
		numeroGiocatoriOnlineRMI.setHorizontalAlignment(SwingConstants.CENTER);
		Integer[] possibiliScelteOnline = { 2, 3, 4};
		selezioneNumeroGiocatoriOnlineRMI = new JComboBox<Integer>(possibiliScelteOnline);
		selezioneNumeroGiocatoriOnlineRMI.setEditable(false);

		JLabel nomeServer = new JLabel("Inserisci nome e data in cui hai accarezzato per l'ultima volta una pecora:");
		inserimentoNomeServer = new JTextField();
		UtilDateModel dataServer = new UtilDateModel(); 
		JDatePanelImpl datePanelServer = new JDatePanelImpl(dataServer); 
		final JDatePickerImpl dataPickerServer = new JDatePickerImpl(datePanelServer); 
		dataPickerServer.setBackground(Color.WHITE);



		confermaOnline = new JButton("Conferma");
		confermaOnline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean continuare = true; 
				boolean dataSettata= true, dataMinoreOdierna = true;
				Date dataOdierna = new Date();
				String nomeGiocatoreOnline = inserimentoNomeServer.getText();
				Date dataInserita = (Date) dataPickerServer.getModel().getValue();
				if (nomeGiocatoreOnline.isEmpty()){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"Inserire il nome!",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(nomeGiocatoreOnline.startsWith(" ")){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"Eliminare il carattere 'spazio' dal nome.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(!controlloCaratteriAmmessi(nomeGiocatoreOnline)){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"I caratteri di seguito mostrati non sono ammessi: (; . , ; : ' | £ % / = ^ ç @ §)",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(dataInserita == null){
					dataSettata = false; 
					JOptionPane.showMessageDialog(null,
							"Inserire la data, se non si è mai accarezzata una pecora impostare propria data di nascita.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(dataOdierna.before(dataInserita)) {
					dataMinoreOdierna = false;
					JOptionPane.showMessageDialog(null,
							"La data inserita non è corretta.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				}
				if(continuare && dataSettata && dataMinoreOdierna) {
					ServerRMI serverRMI = new ServerRMI((int)selezioneNumeroGiocatoriOnlineRMI.getSelectedItem());
					serverRMI.avviaServer(nomeGiocatoreOnline, dataInserita); 
					panelNumeroGiocatoriOnline.setVisible(false);
					mainFrame.dispose();
				}
			}

		}); 




		JPanel compilazioneServer = new JPanel();
		compilazioneServer.setBackground(Color.WHITE);
		compilazioneServer.setLayout(new GridLayout(1,3,10,10));
		compilazioneServer.add(inserimentoNomeServer);
		compilazioneServer.add(dataPickerServer);
		compilazioneServer.add(confermaOnline);
		JPanel nomeServerENumeroOnline = new JPanel();

		JLabel indirizzoIPServer = new JLabel();
		try {
			indirizzoIPServer = new JLabel("Indirizzo IP da comunicare ai client: " + Inet4Address.getLocalHost().getHostAddress());
			indirizzoIPServer.setFont(new Font("", Font.BOLD,14));
		} catch (UnknownHostException e1) {
			LOGGER.log(Level.SEVERE, "Errore nella comunicazione del proprio IP", e1 );
		}
		nomeServerENumeroOnline.setBackground(Color.WHITE);
		nomeServerENumeroOnline.setLayout(new BorderLayout());
		nomeServerENumeroOnline.add(indirizzoIPServer, BorderLayout.PAGE_START);
		nomeServerENumeroOnline.add(numeroGiocatoriOnlineRMI, BorderLayout.LINE_START);
		nomeServerENumeroOnline.add(selezioneNumeroGiocatoriOnlineRMI, BorderLayout.LINE_END);

		panelNumeroGiocatoriOnline.add(nomeServerENumeroOnline, BorderLayout.PAGE_START);
		panelNumeroGiocatoriOnline.add(nomeServer, BorderLayout.CENTER);
		panelNumeroGiocatoriOnline.add(compilazioneServer, BorderLayout.PAGE_END);
		panelNumeroGiocatoriOnline.setVisible(false);
		labelImmagine.add(panelNumeroGiocatoriOnline);

		final JPanel panelNumeroGiocatoriSocket = new JPanel(new BorderLayout());
		panelNumeroGiocatoriSocket.setBounds(280, 330, 500, 100);
		panelNumeroGiocatoriSocket.setBackground(Color.WHITE);
		JButton confermaServerSocket = new JButton("Conferma"); 


		JLabel indirizzoIPServerSocket = new JLabel();
		try {
			indirizzoIPServerSocket = new JLabel("Indirizzo IP da inserire e da comunicare ai client: " + Inet4Address.getLocalHost().getHostAddress());
			indirizzoIPServerSocket.setFont(new Font("", Font.BOLD,14));
		} catch (UnknownHostException e1) {
			LOGGER.log(Level.SEVERE, "Errore nella comunicazione del proprio IP", e1 );
		}
		numeroGiocatoriOnlineServerSocket = new JLabel("Seleziona numero giocatori: ");
		numeroGiocatoriOnlineServerSocket.setFont(new Font("", Font.BOLD,14));
		numeroGiocatoriOnlineServerSocket.setHorizontalAlignment(SwingConstants.CENTER);
		selezioneNumeroGiocatoriOnlineServerSocket = new JComboBox<Integer>(possibiliScelteOnline);
		selezioneNumeroGiocatoriOnlineServerSocket.setEditable(false);

		confermaServerSocket.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelNumeroGiocatoriSocket.setVisible(false);
				panelInserimentoRemoto.setVisible(true);
				(new Thread( new Runnable () {

					@Override
					public void run() {
						RaccoltaSocket raccoltaSocket = new RaccoltaSocket((int)selezioneNumeroGiocatoriOnlineServerSocket.getSelectedItem(), 8000); 
						try {
							raccoltaSocket.avviaServerSocket();
						} catch (ClassNotFoundException | IOException e) {
							LOGGER.log(Level.SEVERE, "errore nella creazione di raccoltaSocket o nell'avvio della sua funzione avviaServerSocket()", e); 
						}	
					}
				})).start();
			}
		});

		panelNumeroGiocatoriSocket.add(indirizzoIPServerSocket, BorderLayout.PAGE_START);
		panelNumeroGiocatoriSocket.add(numeroGiocatoriOnlineServerSocket, BorderLayout.LINE_START);
		panelNumeroGiocatoriSocket.add(selezioneNumeroGiocatoriOnlineServerSocket, BorderLayout.LINE_END);
		panelNumeroGiocatoriSocket.add(confermaServerSocket, BorderLayout.PAGE_END);
		panelNumeroGiocatoriSocket.setVisible(false);
		labelImmagine.add(panelNumeroGiocatoriSocket);




		final JPanel panelSceltaRete = new JPanel();
		panelSceltaRete.setLayout(null);
		panelSceltaRete.setBounds(250, 245, 430, 200);
		panelSceltaRete.setBackground(Color.WHITE);

		JButton confermaRMI = new JButton("RMI");
		confermaRMI.setBounds(110, 110, 100, 60);
		confermaRMI.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sceltaSocket = false; 
				panelSceltaRete.setVisible(false);
				panelSceltaRuolo.setVisible(true);
			}
		});

		JButton confermaSocket = new JButton("Socket");
		confermaSocket.setBounds(250, 110, 100, 60);
		confermaSocket.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sceltaSocket = true;
				panelSceltaRete.setVisible(false);
				panelSceltaRuolo.setVisible(true);
			}
		});

		panelSceltaRete.add(confermaRMI);
		panelSceltaRete.add(confermaSocket);
		panelSceltaRete.setVisible(false);
		labelImmagine.add(panelSceltaRete);



		panelSceltaRuolo = new JPanel();
		panelSceltaRuolo.setLayout(null);
		panelSceltaRuolo.setBounds(250, 245, 430, 200);
		panelSceltaRuolo.setBackground(Color.WHITE);

		confermaServer = new JButton("Server");
		confermaServer.setBounds(110, 110, 100, 60);
		confermaServer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelSceltaRuolo.setVisible(false);
				if(!sceltaSocket) {
					panelNumeroGiocatoriOnline.setVisible(true);
				} else {
					panelNumeroGiocatoriSocket.setVisible(true); 
				}
			}
		});

		confermaClient = new JButton("Client");
		confermaClient.setBounds(250, 110, 100, 60);
		confermaClient.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelSceltaRuolo.setVisible(false);
				panelInserimentoRemoto.setVisible(true);
			}
		});

		panelSceltaRuolo.add(confermaServer);
		panelSceltaRuolo.add(confermaClient);
		panelSceltaRuolo.setVisible(false);
		labelImmagine.add(panelSceltaRuolo);


		//panel locale o remoto
		localeORemoto = new JPanel();
		localeORemoto.setLayout(null);
		localeORemoto.setBounds(200, 296, 600, 200);
		localeORemoto.setBackground(Color.WHITE);
		bottoneGiocoLocale = new JButton("Locale");
		bottoneGiocoLocale.setBounds(150, 60, 100, 60); 
		bottoneGiocoLocale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				localeORemoto.setVisible(false);
				panelNumeroGiocatori.setVisible(true);
			}
		});
		bottoneGiocoRemoto = new JButton("Remoto");
		bottoneGiocoRemoto.setBounds(300, 60, 100, 60); 
		bottoneGiocoRemoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				localeORemoto.setVisible(false);
				panelSceltaRete.setVisible(true);
			}
		});
		localeORemoto.add(bottoneGiocoLocale);
		localeORemoto.add(bottoneGiocoRemoto);
		localeORemoto.setVisible(false);
		labelImmagine.add(localeORemoto);

		//panel inserimento locale
		panelNumeroGiocatori = new JPanel(new BorderLayout());
		panelNumeroGiocatori.setBounds(330, 352, 300, 71);
		panelNumeroGiocatori.setBackground(Color.WHITE);
		numeroGiocatori = new JLabel("Seleziona numero giocatori: ");
		numeroGiocatori.setFont(new Font("", Font.BOLD,14));
		numeroGiocatori.setHorizontalAlignment(SwingConstants.CENTER);
		String[] possibiliScelte = { DUEGIOCATORI, TREGIOCATORI, QUATTROGIOCATORI };
		selezioneNumeroGiocatori = new JComboBox<String>(possibiliScelte);
		selezioneNumeroGiocatori.setEditable(false);
		conferma = new JButton("Conferma");
		conferma.addActionListener(listenerConferma); 
		panelNumeroGiocatori.add(numeroGiocatori, BorderLayout.PAGE_START);
		panelNumeroGiocatori.add(selezioneNumeroGiocatori, BorderLayout.CENTER);
		panelNumeroGiocatori.add(conferma, BorderLayout.PAGE_END);
		panelNumeroGiocatori.setVisible(false);
		labelImmagine.add(panelNumeroGiocatori);


		panelInserimentoRemoto = new JPanel();
		panelInserimentoRemoto.setBackground(Color.WHITE);
		panelInserimentoRemoto.setLayout(new BorderLayout());
		panelInserimentoRemoto.setBounds(200, 330, 600, 90);



		UtilDateModel data = new UtilDateModel(); 
		JDatePanelImpl datePanel = new JDatePanelImpl(data); 
		final JDatePickerImpl dataPicker = new JDatePickerImpl(datePanel); 
		dataPicker.setBackground(Color.WHITE);

		panelIndirizzoIP = new JPanel();
		panelIndirizzoIP.setBackground(Color.WHITE);
		panelIndirizzoIP.setLayout(new GridLayout(1, 2, 10,10));
		JLabel inserimentoIndirizzoIPLabelRMI = new JLabel("Inserire Indirizzo IP del Server a cui connettersi");
		final JTextField inserimentoIndirizzoIPRMI = new JTextField();
		panelIndirizzoIP.add(inserimentoIndirizzoIPLabelRMI);
		panelIndirizzoIP.add(inserimentoIndirizzoIPRMI);


		nomeGiocatore = new JLabel("Inserisci nome giocatore e la data in cui hai accarezzato per l'ultima volta una pecora: ");
		nomeGiocatore.setBackground(Color.WHITE);
		scriviNome = new JTextField(10);

		confermaServer = new JButton("Conferma");
		confermaServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean continuare = true;
				Date dataOdierna = new Date();
				String nomeGiocatoreOnline = scriviNome.getText(); 
				Date data = (Date) dataPicker.getModel().getValue();
				if (nomeGiocatoreOnline.isEmpty()){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"Inserire il nome!",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(nomeGiocatoreOnline.startsWith(" ")){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"Eliminare il carattere 'spazio' dal nome.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(!controlloCaratteriAmmessi(nomeGiocatoreOnline)){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"I caratteri di seguito mostrati non sono ammessi: (; . , ; : ' | £ % / = ^ ç @ §)",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(data == null){
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"Inserire la data, se non si è mai accarezzata una pecora impostare propria data di nascita.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				} else if(dataOdierna.before(data)) {
					continuare = false;
					JOptionPane.showMessageDialog(null,
							"La data inserita non è corretta.",
							"Errore",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(this.getClass().getResource("/errore.png")));
				}
				if(continuare) {
					String indirizzoIPACuiConnettersi = inserimentoIndirizzoIPRMI.getText();
					if(indirizzoIPACuiConnettersi.length() < 11 && !("localhost").equals(indirizzoIPACuiConnettersi)) {
						JOptionPane.showMessageDialog(null,
								"L'indirizzo IP inserito non è valido",
								"Errore",
								JOptionPane.ERROR_MESSAGE,
								new ImageIcon(this.getClass().getResource("/errore.png")));
					} else {
						if(sceltaSocket) {
							new ImplementazioneClientSocket(indirizzoIPACuiConnettersi, 8000, nomeGiocatoreOnline, data);
						} else {
							ClientRMI clientRMI = new ClientRMI(indirizzoIPACuiConnettersi);
							clientRMI.avviaClient(nomeGiocatoreOnline, data); 
						}
						nomi.add(scriviNome.getText()); 
						panelInserimentoRemoto.setVisible(false);
						mainFrame.setEnabled(false);
						mainFrame.setVisible(false);
					}
				}
			}
		});

		JPanel dati = new JPanel();
		dati.setBackground(Color.WHITE);
		dati.setLayout(new GridLayout(1,3,10,10));
		dati.add(scriviNome); 
		dati.add(dataPicker);
		dati.add(confermaServer);





		panelInserimentoRemoto.add(panelIndirizzoIP, BorderLayout.PAGE_START);
		panelInserimentoRemoto.add(nomeGiocatore, BorderLayout.CENTER);
		panelInserimentoRemoto.add(dati, BorderLayout.PAGE_END);

		panelInserimentoRemoto.setVisible(false);
		labelImmagine.add(panelInserimentoRemoto);

		panel.add(labelImmagine);
		panel.setMaximumSize(new Dimension(950,710));
		panel.setMinimumSize(new Dimension(950,710));

		mainFrame.add(panel);
		mainFrame.setResizable(false);
		mainFrame.setMaximumSize(new Dimension(950,710));
		mainFrame.setMinimumSize(new Dimension(950,710));
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	public boolean controlloCaratteriAmmessi(String string) {
		return !(string.contains(".") || string.contains(",") || string.contains(";") || string.contains(":") || string.contains("'") || string.contains("|") || string.contains("£") || string.contains("%") || string.contains("/") || string.contains("=") || string.contains("^") || string.contains("ç") || string.contains("@") || string.contains("§")); 
	}

	public boolean getSchermataInizialeAttiva () {
		return this.schermataInizialeAttiva; 
	}

	public JFrame getJFrame () {
		return this.mainFrame; 
	}


	public void runSchermataIniziale() {
		mainFrame.setVisible(true);
	}

	public List<String> getNomiGiocatori() {
		return this.nomi; 
	}

	public List<Date> getDate() {
		return this.dateGiocatori; 
	}

}