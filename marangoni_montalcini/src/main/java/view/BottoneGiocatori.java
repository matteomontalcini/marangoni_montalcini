package view;

import java.awt.Font;

import javax.swing.JButton;

/**
 * Classe che permette di associare ad un bottone il nome, 
 * i danari e l'indice del giocatore corrispondente
 */
public class BottoneGiocatori extends JButton {
	
	private static final long serialVersionUID = 6380159562033997145L;
	
	/**
	 * Danari del giocatore
	 */
	private int totaleDanari; 
	
	/**
	 * Indice del giocatore nell'array dei giocatori
	 */
	private int indice; 
	
	public BottoneGiocatori(int totaleDanari,String nome, int indice) {
		super(nome); 
		super.setFont(new Font(nome, Font.BOLD,16));
		this.totaleDanari = totaleDanari; 	
		this.indice = indice; 
	}
	
	public void setTotaleDanari(int totaleDanari) {
		this.totaleDanari = totaleDanari; 
	}
	
	public int getTotaleDanari() {
		return this.totaleDanari; 
	}
	
	public int getIndice() {
		return this.indice; 
	}
}
