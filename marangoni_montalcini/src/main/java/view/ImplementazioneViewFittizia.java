package view;

import java.util.List;

/**
 * View fittizia usata per i test
 */
public class ImplementazioneViewFittizia  implements InterfacciaView {
	
	private String idStradaSelezionata, idRegioneSelezionata, animaleSelezionato, mossaSelezionata; 
	private int tipoDiTerrenoSelezionato;

	
	
	
	@Override
	public void attivaBottoniTessereSelezionabili(List<Integer> numeriTessereCorrispondenti) {
	
	}

	@Override
	public void disattivaBottoniTessereTerreno() {
	}

	@Override
	public void attivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		
	}

	@Override
	public void disattivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
	}

	@Override
	public void setIconaGiocatoreInStrada(String idStradaArrivo,int coordinataXPartenza, int coordinataYPartenza, int coordinataXArrivo, int coordinataYArrivo, int posizioneGiocatoreInArray) {
	}

	@Override
	public void setIconaGiocatoreInizio(String idStradaArrivo, int posizioneGiocatoreInArray) {
	}

	@Override
	public void eseguiLancioDado(int valoreLancioDado) {
	}

	@Override
	public void removeLancioDado() {
	}

	@Override
	public void setLabelComunicazioni(String string) {
	}

	@Override
	public void attivaAnimaliSelezionabili(List<Integer> animaliSelezionabili) {
		
	}

	@Override
	public void attivaBottoniRegioni() {
		
	}

	@Override
	public void disattivaBottoniRegioni() {
	}

	@Override
	public void attivaBottoniMosseSelezionabili(List<Integer> mosseSelezionabili) {
	
	}

	@Override
	public void disattivaBottoniMosse(int numero) {
	}

	@Override
	public void setDanariGiocatore(int indice, int danari) {
	}

	@Override
	public void setCostoTesseraTerreno(int tipo, int nuovoCosto) {
	}

	@Override
	public void setTotalePecoreBiancheInRegione(String idRegione, int totalePecoreBianche) {
	}

	@Override
	public void setTotaleAgnelliInRegione(String idRegione, int totaleAgnelli) {
	}

	@Override
	public void setTotaleArietiInRegione(String idRegione, int totaleArieti) {
	}

	@Override
	public void setPosizioneLupo(String idRegionePartenza, String idRegioneArrivo) {
	}

	@Override
	public void setPosizionePecoraNera(String idRegionePartenza, String idRegioneArrivo) {
	}

	@Override
	public void setGiocatoreDelTurno(String nomeGiocatore) {
	}

	@Override
	public void inserisciGiocatore(int indice) {
	}

	

	@Override
	public void disattivaBottoniMosse() {
	}

	@Override
	public void visibilityBottoniAnimali(String idRegione, boolean condizione) {
	}

	@Override
	public void spostamentoAnimale(String idRegionePartenza, String idRegioneArrivo, String animale) {
	}

	@Override
	public void disattivaPannelloSelezioneAnimali() {
	}

	@Override
	public void morteAnimale(String tipoAnimale, String idRegione) {
	}

	@Override
	public void nascitaAgnello(String idRegione) {
	}

	@Override
	public void setTessereInPossesso(List<Integer> tessereInPossesso, int indiceGiocatore) {
	}

	@Override
	public void generaRisultato(List<Integer> punteggiGiocatori, int punteggioMassimo) {
	}

	@Override
	public void avvisoMosseNonDisponibili() {
		
	}

	@Override
	public void disattivaPannelloMosseNonDisponibili() {
	}

	@Override
	public void chiudiSchermata() {
	}
	

	@Override
	public void posizionaCancello(String idStradaPartenza, boolean faseFinale) {
	}

	public String getIdStradaSelezionata() {
		return idStradaSelezionata;
	}


	public void setIdStradaSelezionata(String idStradaSelezionata) {
		this.idStradaSelezionata = idStradaSelezionata;
	}


	public String getIdRegioneSelezionata() {
		return idRegioneSelezionata;
	}


	public void setIdRegioneSelezionata(String idRegioneSelezionata) {
		this.idRegioneSelezionata = idRegioneSelezionata;
	}


	public String getAnimaleSelezionato() {
		return animaleSelezionato;
	}


	public void setAnimaleSelezionato(String animaleSelezionato) {
		this.animaleSelezionato = animaleSelezionato;
	}


	public String getMossaSelezionata() {
		return mossaSelezionata;
	}


	public void setMossaSelezionata(String mossaSelezionata) {
		this.mossaSelezionata = mossaSelezionata;
	}


	public int getTipoDiTerrenoSelezionato() {
		return tipoDiTerrenoSelezionato;
	}


	public void setTipoDiTerrenoSelezionato(int tipoDiTerrenoSelezionato) {
		this.tipoDiTerrenoSelezionato = tipoDiTerrenoSelezionato;
	}

}