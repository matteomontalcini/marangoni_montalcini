package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;


class ButtonWithBackground extends JButton {
	
	private static final long serialVersionUID = -2677932750054590447L;
	private Image imgSfondo;

	public ButtonWithBackground(Image imgSfondo) {
		super();
		this.imgSfondo = imgSfondo;
		}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imgSfondo, 0, 0, this);
		}
	}