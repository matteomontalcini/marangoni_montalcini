package view;

import javax.swing.JButton;

/**
 * Bottone usato per le regioni contiene l'id della regione.
 */
public class BottoneRegione extends JButton {
	
	private static final long serialVersionUID = -7605393201314061208L;
	
	/**
	 * Id relativo alla regione
	 */
	private final String id;
	
	/**
	 * Costruttore
	 * @param id: id della regione
	 */
	public BottoneRegione(String id) {
		super();
		this.id = id;  
	}
	
	public String getId() {
		return this.id;
	}
	
	

}
