package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eventi.*;

/**
 * questa classe rappresenta la schermata di gioco
 */
public class SchermataPrincipale extends Observable implements InterfacciaView {
	
	/**
	 * questa classe interna rappresenta il listener del mouse per la pressione delle regioni
	 */
	class AzioniMouse extends MouseAdapter{
		public void mouseClicked(MouseEvent e) {
			int x=e.getX();
			int y=e.getY();
			Color color;
			int a = imgColori.getRGB(x, y);
			color = new Color(a); 
			String regioneCorrispondente = mappaColoriId.get(color);
			RegioneSelezionata evento = new RegioneSelezionata(); 
			evento.setIdRegione(regioneCorrispondente);
			processEvento(evento);
		}
	}
	
	/**
	 * è il JFrame della schermata
	 */
	private JFrame frame;
	private JPanel mainPanel, panelComunicazioni, panelMosse, panelTessereTerreno, panelTerreniEGiocatori,panelPrimaLineaTerreni, panelSecondaLineaTerreni; 
	private PanelWithBackground panelMappa;  
	private JLabel labelComunicazioni, labelNomeGiocatore;
	private JButton tesseraTerreno0, tesseraTerreno1, tesseraTerreno2, tesseraTerreno3, tesseraTerreno4, tesseraTerreno5;
	private JButton mossa0, mossa1, mossa2, mossa3, mossa4;
	private JLabel dado;
	private String comunicazione = "Ciao";
	
	/**
	 * Contiene tutti i bottoni che sono associati alle strade sulla mappa
	 */
	private List<BottoneStrada> bottoniStrada = new ArrayList<BottoneStrada>(); 
	
	/**
	 * Contiene tutti i bottoni che in ogni regione sono associati agli animali: nonostante il nome, riguardano
	 * pecore bianche, arieti, agnelli
	 */
	private List<BottoneElementi> bottoniPecoreBianche = new ArrayList<BottoneElementi>(); 
	
	/**
	 * Contiene tutti i bottoni, regione per regione, del lupo: viene visualizzato solo un bottone alla volta, 
	 * corrispondente alla posizione effettiva del lupo
	 */
	private List<BottoneElementi> bottoniLupo = new ArrayList<BottoneElementi>(); 
	
	/**
	 * Contiene tutti i bottoni, regione per regione, della pecora nera: viene visualizzato solo un bottone alla volta, 
	 * corrispondente alla posizione effettiva della pecora nera
	 */
	private List<BottoneElementi> bottoniPecoraNera = new ArrayList<BottoneElementi>(); 
	
	/**
	 * contiene tutti i bottoni delle tessere terreno, ordinati in modo che all'indice dell'array corrisponda 
	 * un determinato tipo di terreno (indice 0 - terreno di tipo 0, ...)
	 */
	private List<JButton> bottoniTessereTerreno = new ArrayList<JButton>(); 
	
	/**
	 * contiene tutti i bottoni dei giocatori, ordinati in base all'indice dei giocatori
	 */
	private List<BottoneGiocatori> bottoniGiocatori = new ArrayList<BottoneGiocatori>(); 
	
	/**
	 * contiene tutti i bottoni delle mosse, ordinati in base al tipo di mossa
	 */
	private List<JButton> bottoniMosse = new ArrayList<JButton>();  
	
	/**
	 * contiene tutte le label che permettono di visualizzare quante pecore bianche ci sono in una regione.
	 */
	private List<LabelAnimali> labelPecoreBianche = new ArrayList<LabelAnimali>(); 
	
	/**
	 * contiene tutte le label che permettono di visualizzare quanti agnelli ci sono in una regione.
	 */
	private List<LabelAnimali> labelAgnelli = new ArrayList<LabelAnimali>(); 
	
	/**
	 * contiene tutte le label che permettono di visualizzare quanti arieti ci sono in una regione.
	 */
	private List<LabelAnimali> labelArieti = new ArrayList<LabelAnimali>(); 
	
	/**
	 * contiene tutti i listener dei bottoni strada, in modo da poter attivare o disattivare quelli corrispondenti
	 * a particolari strade.
	 */
	private List<ActionListener> listenerStrade = new ArrayList<ActionListener>(); 
	private BufferedImage imgColori;
	private Map<Color, String> mappaColoriId = new HashMap<Color,String>(); 
	private static final Logger LOGGER = Logger.getLogger("view"); 
	
	/**
	 * contiene tutte le label che permettono di sapere quanti danari possiede un giocatore
	 */
	private List<JLabel> labelDanari = new ArrayList<JLabel>(); 
	
	/**
	 * è il listener delle regioni: quando una regione viene cliccata, esso effettua determinate operazioni
	 */
	private AzioniMouse listenerRegioni = new AzioniMouse(); 
	
	/**
	 * contiene i popup relativi a tutte le regioni: questi permettono di conoscere quanti e quali animali ci sono in 
	 * una regione
	 */
	private List<JPopupMenu> listaPopupAnimali = new ArrayList<JPopupMenu>(); 
	
	/**
	 * questo pannello si attiva quando bisogna selezionare un animale: contiene i bottoni degli animali che 
	 * possono essere selezionati
	 */
	private List<JPanel> panelAnimali = new ArrayList<JPanel>(); 
	
	/**
	 * contiene le associazioni tra l'indice della regione e il suo id
	 */
	Map<String, Integer> posizioniArrayListaPopoUp = new HashMap<String, Integer>(); 
	
	/**
	 * contiene tutti i popup relativi ai vari giocatori
	 */
	private List<JPopupMenu> listaPopupGiocatori = new ArrayList<JPopupMenu>(); 
	
	/**
	 * è il pannello in cui vengono aggiunti i bottoni dei giocatori
	 */
	private List<JPanel> panelGiocatori = new ArrayList<JPanel>(); 
	
	/**
	 * contiene le associazioni tra i bottoni dei giocatori e il loro indice
	 */
	Map<BottoneGiocatori, Integer> posizioniArrayListaPopupGiocatori = new HashMap<BottoneGiocatori, Integer>();
	
	/**
	 * contiene tutti i nomi dei giocatori della partita
	 */
	private List<String> nomiGiocatori = new ArrayList<String>(); 
	
	/**
	 * sono i bottoni che vengono aggiunti al pannello di selezione degli animali: servono rispettivamente 
	 * per selezionare la pecora bianca, la pecora nera o l'ariete.
	 */
	private JButton bottoneSelezionePecoraBianca, bottoneSelezionePecoraNera, bottoneSelezioneAriete;
	
	/**
	 * sono i pannelli per la selezione degli animali, per il lancio del dado, per la visualizzazione dei punteggi, per 
	 * l'avviso che non ci sono mosse disponibili
	 */
	private PanelWithBackground panelSelezioneAnimale, panelLancioDado, panelPunteggi, panelMosseNonDisponibili;
	private JLabel costo, labelPunteggioNumericoRosso, labelPunteggioNumericoBlu, labelPunteggioNumericoVerde, labelPunteggioNumericoGiallo; 
	private List<ActionListener> listenerTessereTerreno = new ArrayList<ActionListener>(); 
	private LabelWithBackground labelPunteggioBlu, labelPunteggioRosso, labelPunteggioVerde, labelPunteggioGiallo; 
	private final JPanel barraRossa, barraBlu, barraVerde, barraGialla; 
	private JLayeredPane layeredPane;
	private static final String MUOVI_PEDINA = "MUOVI_PEDINA", EFFETTUA_ACCOPPIAMENTO = "EFFETTUA_ACCOPPIAMENTO", EFFETTUA_SPARATORIA = "EFFETTUA_SPARATORIA",
			ACQUISTA_TESSERA_TERRENO = "ACQUISTA_TESSERA_TERRENO", MUOVI_ANIMALE = "MUOVI_ANIMALE", PECORA_BIANCA = "PECORA_BIANCA", ARIETE = "ARIETE", 
			PECORA_NERA = "PECORA_NERA"; 

	
	public SchermataPrincipale(int numGiocatori, List<String> nomiGiocatori) {

		this.nomiGiocatori = nomiGiocatori;

		//creazione nuova finestra
		frame = new JFrame("Sheepland");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//creazione pannello generale

		mainPanel = new JPanel();
		mainPanel.setBackground(new Color(33,162,246));

		//Creazione pannello relativo alle tessere terreno e lo organizzo in tabella da 6 righe ed 1 colonna
		panelTessereTerreno = new JPanel();
		panelTessereTerreno.setBackground(new Color(33,162,246));
		panelTessereTerreno.setLayout(new GridLayout(6, 1, 0, 20));

		//creazione bottoni relativi alle tessere terreno con relativo costo in BOLD
		tesseraTerreno0 = new JButton("0");
		tesseraTerreno0.setLayout(null);
		costo = new JLabel();
		costo.setText("0");
		costo.setFont(new Font(comunicazione, Font.BOLD, 20));
		costo.setForeground(Color.RED);
		costo.setBounds(10, 24, 10,10);
		costo.setBackground(Color.RED);
		tesseraTerreno0.add(costo);
		tesseraTerreno0.setContentAreaFilled(false);
		tesseraTerreno0.setBorderPainted(false);
		tesseraTerreno0.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno0.setIcon(new ImageIcon(this.getClass().getResource("/terreno0.jpg")));
		ActionListener listenerTesseraTerreno0 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(0);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno0);

		tesseraTerreno1 = new JButton("0");
		tesseraTerreno1.setContentAreaFilled(false);
		tesseraTerreno1.setBorderPainted(false);
		tesseraTerreno1.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno1.setIcon(new ImageIcon(this.getClass().getResource("/terreno1.jpg")));
		ActionListener listenerTesseraTerreno1 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(1);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno1);

		tesseraTerreno2 = new JButton("0");
		tesseraTerreno2.setContentAreaFilled(false);
		tesseraTerreno2.setBorderPainted(false);
		tesseraTerreno2.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno2.setIcon(new ImageIcon(this.getClass().getResource("/terreno2.jpg")));
		ActionListener listenerTesseraTerreno2 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(2);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno2);

		tesseraTerreno3 = new JButton("0");
		tesseraTerreno3.setContentAreaFilled(false);
		tesseraTerreno3.setBorderPainted(false);
		tesseraTerreno3.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno3.setIcon(new ImageIcon(this.getClass().getResource("/terreno3.jpg")));
		ActionListener listenerTesseraTerreno3 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(3);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno3);

		tesseraTerreno4 = new JButton("0");
		tesseraTerreno4.setContentAreaFilled(false);
		tesseraTerreno4.setBorderPainted(false);
		tesseraTerreno4.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno4.setIcon(new ImageIcon(this.getClass().getResource("/terreno4.jpg")));
		ActionListener listenerTesseraTerreno4 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(4);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno4);

		tesseraTerreno5 = new JButton("0");
		tesseraTerreno5.setContentAreaFilled(false);
		tesseraTerreno5.setBorderPainted(false);
		tesseraTerreno5.setFont(new Font(comunicazione, Font.BOLD,16));
		tesseraTerreno5.setIcon(new ImageIcon(this.getClass().getResource("/terreno5.jpg")));
		ActionListener listenerTesseraTerreno5 = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TesseraTerrenoSelezionata evento = new TesseraTerrenoSelezionata(); 
				evento.setTipoDiTerreno(5);
				processEvento(evento); 
			}
		};  
		listenerTessereTerreno.add(listenerTesseraTerreno5);


		bottoniTessereTerreno.add(tesseraTerreno0); 
		bottoniTessereTerreno.add(tesseraTerreno1); 
		bottoniTessereTerreno.add(tesseraTerreno2); 
		bottoniTessereTerreno.add(tesseraTerreno3); 
		bottoniTessereTerreno.add(tesseraTerreno4); 
		bottoniTessereTerreno.add(tesseraTerreno5); 


		//creo un pannello contenente sia le tessere terreno che i giocatori.
		panelTerreniEGiocatori = new JPanel();
		panelTerreniEGiocatori.setBackground(new Color(33,162,246));
		panelTerreniEGiocatori.setLayout(new GridLayout(6,1,10, 10));
		panelTerreniEGiocatori.setOpaque(false);

		//creo due pannelli che corrispondono alle prime due linee del pannello panelTerreniEGiocatori
		panelPrimaLineaTerreni = new JPanel();
		panelPrimaLineaTerreni.setBackground(new Color(33,162,246));
		panelPrimaLineaTerreni.setOpaque(false);
		panelSecondaLineaTerreni = new JPanel();
		panelSecondaLineaTerreni.setOpaque(false);
		panelSecondaLineaTerreni.setBackground(new Color(33,162,246));

		//imposto un layout a griglia nella linea dei terreni
		panelPrimaLineaTerreni.setLayout(new GridLayout(1, 3, 10, 10));
		panelSecondaLineaTerreni.setLayout(new GridLayout(1, 3, 10, 10));

		//setto le posizioni dei nuovi bottoni terreni nel pannello
		panelPrimaLineaTerreni.add(tesseraTerreno0);
		panelPrimaLineaTerreni.add(tesseraTerreno1);
		panelPrimaLineaTerreni.add(tesseraTerreno2);
		panelSecondaLineaTerreni.add(tesseraTerreno3);
		panelSecondaLineaTerreni.add(tesseraTerreno4); 
		panelSecondaLineaTerreni.add(tesseraTerreno5);


		//aggiunto i due pannelli al pannelGiocatoriETerreni
		panelTerreniEGiocatori.add(panelPrimaLineaTerreni);
		panelTerreniEGiocatori.add(panelSecondaLineaTerreni);

		//creazione bottoni relativi ai giocatori minimi
		for(int i = 0; i < numGiocatori; i++) {
			BottoneGiocatori giocatore; 
			if(numGiocatori == 2) {
				giocatore = new BottoneGiocatori(30, nomiGiocatori.get(i), i);
				giocatore.setContentAreaFilled(false);
				giocatore.setBorderPainted(false);
				bottoniGiocatori.add(giocatore);
			} else {
				giocatore = new BottoneGiocatori(20, nomiGiocatori.get(i), i);
				giocatore.setContentAreaFilled(false);
				giocatore.setBorderPainted(false);
				bottoniGiocatori.add(giocatore); 
			}
			switch(i) {
			case 0: giocatore.setIcon(new ImageIcon(this.getClass().getResource("/giocatoreRossoBordo.png")));
			break;
			case 1: giocatore.setIcon(new ImageIcon(this.getClass().getResource("/giocatoreBluBordo.png")));
			break; 
			case 2: giocatore.setIcon(new ImageIcon(this.getClass().getResource("/giocatoreVerdeBordo.png")));
			break;
			case 3: giocatore.setIcon(new ImageIcon(this.getClass().getResource("/giocatoreGialloBordo.png")));
			break;
			default: 
				break; 
			}
			giocatore.setHorizontalAlignment(SwingConstants.LEFT);

			//assegno menù a comparsa ai tasti giocatori
			JPanel menuItem = new JPanel();
			panelGiocatori.add(menuItem);
			panelGiocatori.get(i).setBackground(Color.WHITE);
			JPopupMenu popMenu = new JPopupMenu();
			listaPopupGiocatori.add(popMenu); 

			panelGiocatori.get(i).setLayout(new GridLayout(1,2,5,5));
			JLabel danari = new JLabel(new ImageIcon(this.getClass().getResource("/soldi1.png")));
			panelGiocatori.get(i).add(danari); 
			JLabel totDanari; 
			if(numGiocatori == 2) {
				totDanari = new JLabel("30");
			} else {
				totDanari = new JLabel("20");
			}
			labelDanari.add(totDanari); 

			panelGiocatori.get(i).add(totDanari);
			listaPopupGiocatori.get(i).add(panelGiocatori.get(i));
			bottoniGiocatori.add(giocatore);
			final int indice = giocatore.getIndice(); 
			posizioniArrayListaPopupGiocatori.put(giocatore, i); 
			giocatore.addMouseListener(new MouseAdapter() {
				public void mouseEntered(MouseEvent e) {
					listaPopupGiocatori.get(indice).show(e.getComponent(), e.getX(), e.getY());
				}});
			giocatore.addMouseListener(new MouseAdapter(){
				public void mouseExited(MouseEvent e) {
					listaPopupGiocatori.get(indice).setVisible(false);
				}
			});
			panelTerreniEGiocatori.add(giocatore); 
		}

		
		//creazione pannello mappa con la sola immagine della mappa
		BufferedImage img = null;
		try {
			img = ImageIO.read(this.getClass().getResourceAsStream("/mappaConCimiteroESquali.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Fallito assegnamento immagine mappa", e);
		}
		panelMappa = new PanelWithBackground(img);
		panelMappa.setMaximumSize(new Dimension(450,646));
		panelMappa.setLayout(null);

		panelMappa.setBounds(0, 0, 450, 676);
		layeredPane = new JLayeredPane();
		layeredPane.add(panelMappa, Integer.valueOf(0));
		
		//carico una immagine della mappa in cui ogni regione ha un colore diverso
		try {
			imgColori = ImageIO.read(this.getClass().getResourceAsStream("/mappaColorata.jpg"));

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento dell'immagine della mappa colorata", e); 
		}
		//faccio il parse dei colori delle varie regioni da xml e metto i colori in una HashMap
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/Regioni.xml")); 
			document.getDocumentElement().normalize();
			NodeList regioni = document.getElementsByTagName("Regione"); 

			for(int i=0; i<regioni.getLength(); i++) {
				Node nodo = regioni.item(i); 
				Element element = (Element) nodo; 
				String idRegione =  element.getAttribute("id"); 
				int coloreR = Integer.parseInt(element.getElementsByTagName("coloreR").item(0).getTextContent()); 	
				int coloreG = Integer.parseInt(element.getElementsByTagName("coloreG").item(0).getTextContent()); 	
				int coloreB = Integer.parseInt(element.getElementsByTagName("coloreB").item(0).getTextContent()); 					
				Color color = new Color(coloreR, coloreG, coloreB);
				mappaColoriId.put(color, idRegione);
			}
		} catch(Exception e) {
			LOGGER.log(Level.SEVERE, "errore nel riempimento della mappaColoriId", e);
		}

		// creo ed inserisco i bottoni alle strade della mappa
		try{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse( this.getClass().getResourceAsStream("/Strade.xml")); 
			document.getDocumentElement().normalize();
			NodeList strade = document.getElementsByTagName("Strada"); 
			List<Integer> coordinateX = new ArrayList<Integer>();
			List<Integer> coordinateY = new ArrayList<Integer>(); 

			for(int i=0; i<strade.getLength(); i++) {
				Node nodo = strade.item(i); 
				Element element = (Element) nodo; 
				String idStrada =  element.getAttribute("id"); 
				int coordinataX = Integer.parseInt(element.getElementsByTagName("coordinatax").item(0).getTextContent()); 
				int coordinataY = Integer.parseInt(element.getElementsByTagName("coordinatay").item(0).getTextContent()); 
				coordinateX.add(coordinataX); 
				coordinateY.add(coordinataY); 
				final BottoneStrada bottone = new BottoneStrada(idStrada);
				bottone.setBounds(coordinataX - 13, coordinataY - 13, 26, 26);
				bottone.setBorderPainted(true);
				bottone.setContentAreaFilled(false);
				bottone.setVisible(false);
				bottone.setEnabled(true);
				bottoniStrada.add(bottone); 
				ActionListener listener = new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						StradaSelezionata evento = new StradaSelezionata(); 
						evento.setIdStrada(bottone.getId());
						processEvento(evento);
						bottone.setLiberoFalse();
						for(int i = 0; i < bottoniStrada.size(); i++) {
							bottoniStrada.get(i).removeActionListener(listenerStrade.get(i));
						}
					}	
				};
				listenerStrade.add(listener); 
				panelMappa.add(bottone);
			}

		} catch(Exception e) {
			LOGGER.log(Level.SEVERE, "errore nell'inserimento dei bottoni alle strade", e);
		}

		//caricamento di varie immagine che serviranno in seguito
		BufferedImage imgPunteggi = null;
		try {
			imgPunteggi = ImageIO.read(this.getClass().getResourceAsStream("/punteggi.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine punteggi", e);
		}

		BufferedImage imgScorrimentoRosso = null;
		try {
			imgScorrimentoRosso = ImageIO.read(this.getClass().getResourceAsStream("/labelMovimentoRosso.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine scorrimento rosso", e);
		}

		BufferedImage imgScorrimentoBlu = null;
		try {
			imgScorrimentoBlu = ImageIO.read(this.getClass().getResourceAsStream("/labelMovimentoBlu.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine scorrimento blu", e);
		}

		BufferedImage imgScorrimentoVerde = null;
		try {
			imgScorrimentoVerde = ImageIO.read(this.getClass().getResourceAsStream("/labelMovimentoVerde.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine scorrimento verde", e);
		}

		BufferedImage imgScorrimentoGiallo = null;
		try {
			imgScorrimentoGiallo = ImageIO.read(this.getClass().getResourceAsStream("/labelMovimentoGiallo.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine scorrimento giallo", e);
		}

		BufferedImage imgLancioDado = null;
		try {
			imgLancioDado = ImageIO.read(this.getClass().getResourceAsStream("/lancioDado.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine lancio dado", e);
		}

		BufferedImage imgMosseNonDisponibili = null;
		try {
			imgMosseNonDisponibili = ImageIO.read(this.getClass().getResourceAsStream("/mosseNonDisponibili.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine mosse non disponibili", e);
		}
		
		BufferedImage imgSelezione = null;
		try {
			imgSelezione = ImageIO.read(this.getClass().getResourceAsStream("/sceltaAnimale.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento immagine selezione", e);
		}

		BufferedImage imgLupo = null;
		try {
			imgLupo = ImageIO.read(this.getClass().getResourceAsStream("/lupoa.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento dell'immagine del lupo", e);
		}
		BufferedImage imgPecoraNera = null;
		try {
			imgPecoraNera = ImageIO.read(this.getClass().getResourceAsStream("/pecoraNera.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento dell'immagine della pecora nera", e);
		}

		BufferedImage imgPecoraBianca = null; 
		try { 
			imgPecoraBianca= ImageIO.read(this.getClass().getResourceAsStream("/pecoraBianca.png"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nel caricamento dell'immagine del lupo", e);
		}

		//creo ed inserisco i bottoni relativi alla pecora bianca, alla pecora nera ed al lupo alle regioni della mappa
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/Regioni.xml")); 
			document.getDocumentElement().normalize();
			NodeList regioni = document.getElementsByTagName("Regione"); 
			List<Integer> coordinateX = new ArrayList<Integer>();
			List<Integer> coordinateY = new ArrayList<Integer>(); 

			for(int i=0; i<regioni.getLength(); i++) {

				Node nodo = regioni.item(i); 
				Element element = (Element) nodo; 
				String idRegione =  element.getAttribute("id"); 

				int coordinataX = Integer.parseInt(element.getElementsByTagName("coordinatax").item(0).getTextContent()); 						
				int coordinataY = Integer.parseInt(element.getElementsByTagName("coordinatay").item(0).getTextContent()); 						
				coordinateX.add(coordinataX); 
				coordinateY.add(coordinataY); 
				final BottoneElementi bottoneLupo = new BottoneElementi(imgLupo, idRegione); 
				bottoneLupo.setBounds(coordinataX - 28, coordinataY - 22, 32, 25); 
				bottoneLupo.setCoordinataX(coordinataX - 28);
				bottoneLupo.setCoordinataY(coordinataY - 22);
				bottoneLupo.setBorderPainted(true);
				if("Sheepsburg".equals(idRegione)) {
					bottoneLupo.setVisible(true);
				} else {
					bottoneLupo.setVisible(false); 
				}
				bottoniLupo.add(bottoneLupo); 
				panelMappa.add(bottoneLupo);
				bottoneLupo.setEnabled(true);
				bottoneLupo.setContentAreaFilled(false);
				bottoneLupo.setBorderPainted(false);
				final BottoneElementi bottonePecoraNera = new BottoneElementi(imgPecoraNera, idRegione);
				bottonePecoraNera.setBounds(coordinataX + 3 , coordinataY - 22, 32, 22);
				bottonePecoraNera.setCoordinataX(coordinataX + 3);
				bottonePecoraNera.setCoordinataY(coordinataY - 22);
				bottonePecoraNera.setBorderPainted(true);
				if("Sheepsburg".equals(idRegione)) {
					bottonePecoraNera.setVisible(true);
				} else {
					bottonePecoraNera.setVisible(false); 
				}
				bottoniPecoraNera.add(bottonePecoraNera);
				bottonePecoraNera.setContentAreaFilled(false);
				bottonePecoraNera.setBorderPainted(false);
				panelMappa.add(bottonePecoraNera); 				

				final BottoneElementi bottonePecoraBianca = new BottoneElementi(imgPecoraBianca, idRegione);
				bottonePecoraBianca.setBounds(coordinataX - 6, coordinataY - 4 ,32, 23);
				bottonePecoraBianca.setCoordinataX(coordinataX - 6);
				bottonePecoraBianca.setCoordinataY(coordinataY - 4);
				JPanel menuItem1 = new JPanel();
				panelAnimali.add(menuItem1); 
				panelAnimali.get(i).setBackground(Color.WHITE);
				JPopupMenu popMenu1 = new JPopupMenu();
				listaPopupAnimali.add(popMenu1);
				panelAnimali.get(i).setLayout(new GridLayout(2,3,5,5));
				JLabel pecoraBianca = new JLabel(new ImageIcon(this.getClass().getResource("/pecoraBiancaNumero.png")));
				JLabel arieti = new JLabel(new ImageIcon(this.getClass().getResource("/arieteNumero.png")));
				panelAnimali.get(i).add(pecoraBianca); 
				panelAnimali.get(i).add(arieti); 
				LabelAnimali numeroPecoraBianca; 
				LabelAnimali numeroArieti; 
				if("Sheepsburg".equals(idRegione)) {
					numeroPecoraBianca = new LabelAnimali("Pecore: " + "0", idRegione);
					labelPecoreBianche.add(numeroPecoraBianca);
					numeroArieti = new LabelAnimali("Arieti: " + "0", idRegione);
					labelArieti.add(numeroArieti); 
				} else {
					numeroPecoraBianca = new LabelAnimali("Pecore: " + "1", idRegione);
					labelPecoreBianche.add(numeroPecoraBianca);
					numeroArieti = new LabelAnimali("Arieti: " + "1", idRegione);
					labelArieti.add(numeroArieti); 
				}
				JLabel agnello = new JLabel(new ImageIcon(this.getClass().getResource("/agnelloNumero.png")));
				panelAnimali.get(i).add(agnello);
				LabelAnimali numeroAgnello = new LabelAnimali("Agnelli: " + "0", idRegione);
				labelAgnelli.add(numeroAgnello);
				panelAnimali.get(i).add(numeroPecoraBianca);
				panelAnimali.get(i).add(numeroArieti);
				panelAnimali.get(i).add(numeroAgnello);
				listaPopupAnimali.get(i).add(panelAnimali.get(i));
				posizioniArrayListaPopoUp.put(idRegione, i);
				bottonePecoraBianca.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						String idRegione = bottonePecoraBianca.getIdRegione(); 
						listaPopupAnimali.get(posizioniArrayListaPopoUp.get(idRegione)).show(e.getComponent(), e.getX(), e.getY());
					}});
				if("Sheepsburg".equals(idRegione)) {
					bottonePecoraBianca.setVisible(false);
				} else {
					bottonePecoraBianca.setVisible(true);
				}
				bottonePecoraBianca.setEnabled(true);
				bottoniPecoreBianche.add(bottonePecoraBianca); 
				bottonePecoraBianca.setContentAreaFilled(false);
				bottonePecoraBianca.setBorderPainted(false);
				panelMappa.add(bottonePecoraBianca);
			}


		} catch(Exception e) {
			LOGGER.log(Level.SEVERE, "inserimento bottoni nelle regioni fallito", e);
		}

		//creazione pannello comunicazioni
		panelComunicazioni = new JPanel();
		panelComunicazioni.setBackground(new Color(33,162,246));
		panelComunicazioni.setLayout(new BorderLayout());

		labelNomeGiocatore = new JLabel("Scelta posizioni"); 

		labelNomeGiocatore.setHorizontalAlignment(SwingConstants.CENTER);
		labelComunicazioni = new JLabel(comunicazione);
		labelComunicazioni.setHorizontalAlignment(SwingConstants.CENTER);
		labelComunicazioni.setFont(new Font(comunicazione, Font.BOLD,16));

		panelComunicazioni.add(labelNomeGiocatore, BorderLayout.PAGE_START);
		panelComunicazioni.add(labelComunicazioni, BorderLayout.PAGE_END);


		//creazione dei bottoni relativi alle mosse
		mossa0 = new JButton();
		mossa0.setContentAreaFilled(false);
		mossa0.setBorderPainted(false);
		mossa0.setIcon(new ImageIcon(this.getClass().getResource("/mossaMuoviPedina.png")));
		mossa0.setHorizontalAlignment(SwingConstants.LEFT);
		mossa0.setEnabled(false);
		mossa0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MossaSelezionata evento = new MossaSelezionata(); 
				evento.setMossaSelezionata(MUOVI_PEDINA);
				processEvento(evento); 
			}
		});

		mossa1 = new JButton();
		mossa1.setHorizontalAlignment(SwingConstants.LEFT);
		mossa1.setContentAreaFilled(false);
		mossa1.setBorderPainted(false);
		mossa1.setIcon(new ImageIcon(this.getClass().getResource("/mossaMuoviAnimale.png")));
		mossa1.setEnabled(false);
		mossa1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MossaSelezionata evento = new MossaSelezionata(); 
				evento.setMossaSelezionata(MUOVI_ANIMALE);
				processEvento(evento); 
			}
		});

		mossa2 = new JButton();
		mossa2.setHorizontalAlignment(SwingConstants.LEFT);
		mossa2.setContentAreaFilled(false);
		mossa2.setBorderPainted(false);
		mossa2.setIcon(new ImageIcon(this.getClass().getResource("/mossaCompraTesseraTerreno.png")));
		mossa2.setEnabled(false);
		mossa2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MossaSelezionata evento = new MossaSelezionata(); 
				evento.setMossaSelezionata(ACQUISTA_TESSERA_TERRENO);
				processEvento(evento); 
			}
		});

		mossa3 = new JButton();
		mossa3.setHorizontalAlignment(SwingConstants.LEFT);
		mossa3.setContentAreaFilled(false);
		mossa3.setBorderPainted(false);
		mossa3.setIcon(new ImageIcon(this.getClass().getResource("/mossaEffettuaSparatoria.png")));
		mossa3.setEnabled(false);
		mossa3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MossaSelezionata evento = new MossaSelezionata(); 
				evento.setMossaSelezionata(EFFETTUA_SPARATORIA);
				processEvento(evento); 
			}
		});

		mossa4 = new JButton();
		mossa4.setHorizontalAlignment(SwingConstants.LEFT);
		mossa4.setContentAreaFilled(false);
		mossa4.setBorderPainted(false);
		mossa4.setIcon(new ImageIcon(this.getClass().getResource("/mossaEffettuaAccoppiamento.png")));
		mossa4.setEnabled(false);
		mossa4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MossaSelezionata evento = new MossaSelezionata(); 
				evento.setMossaSelezionata(EFFETTUA_ACCOPPIAMENTO);
				processEvento(evento); 
			}
		});

		bottoniMosse.add(mossa0); 
		bottoniMosse.add(mossa1); 
		bottoniMosse.add(mossa2); 
		bottoniMosse.add(mossa3); 
		bottoniMosse.add(mossa4); 

		//creazione pannello scelta mosse
		panelMosse = new JPanel();
		panelMosse.setBackground(new Color(33,162,246));
		panelMosse.setLayout(new GridLayout(6, 1, 0, 20));
		panelMosse.setMaximumSize(new Dimension(80,400));

		//setto le posizioni delle mosse nel pannello
		panelMosse.add(mossa0);
		panelMosse.add(mossa1);
		panelMosse.add(mossa2);
		panelMosse.add(mossa3);
		panelMosse.add(mossa4); 

		//creazione di alcuni pannelli che verranno visualizzati sopra la mappa in alcune occasioni
		//pannello relativo a caso mosse non disponibili		
		panelMosseNonDisponibili = new PanelWithBackground(imgMosseNonDisponibili);
		panelMosseNonDisponibili.setBounds(60, 100, 400, 130);
		panelMosseNonDisponibili.setOpaque(false);
		panelMosseNonDisponibili.setVisible(false);
		panelMosseNonDisponibili.setLayout(null);
		
		JButton bottoneConfermaMosseNonDisponibili = new JButton("Ok");
		bottoneConfermaMosseNonDisponibili.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ConfermaRicezioneAvvisoNoMosse evento = new ConfermaRicezioneAvvisoNoMosse(); 
				processEvento(evento); 
			}
		});
		bottoneConfermaMosseNonDisponibili.setBounds(135, 80, 50, 40);
		panelMosseNonDisponibili.add(bottoneConfermaMosseNonDisponibili);
		layeredPane.add(panelMosseNonDisponibili, Integer.valueOf(3));
		
		//pannello relativo alla selezione della pecora bianca, dell'ariete o della pecora nera
		panelSelezioneAnimale = new PanelWithBackground(imgSelezione);
		panelSelezioneAnimale.setBounds(60, 100, 400, 130);
		panelSelezioneAnimale.setOpaque(false);
		panelSelezioneAnimale.setVisible(false);
		panelSelezioneAnimale.setLayout(null);
		layeredPane.add(panelSelezioneAnimale, Integer.valueOf(3));

		bottoneSelezioneAriete = new JButton();
		bottoneSelezioneAriete.setIcon(new ImageIcon(this.getClass().getResource("/arieteNumero.png")));
		bottoneSelezioneAriete.setBounds(55, 53, 50, 50);
		bottoneSelezioneAriete.setContentAreaFilled(false);
		bottoneSelezioneAriete.setBorderPainted(false);
		bottoneSelezioneAriete.setVisible(false);
		bottoneSelezioneAriete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AnimaleSelezionato evento = new AnimaleSelezionato(); 
				evento.setAnimaleSelezionato(ARIETE);
				processEvento(evento);
			}
		}

				);
		panelSelezioneAnimale.add(bottoneSelezioneAriete);

		bottoneSelezionePecoraBianca = new JButton();
		bottoneSelezionePecoraBianca.setIcon(new ImageIcon(this.getClass().getResource("/pecoraBiancaNumero.png")));
		bottoneSelezionePecoraBianca.setBounds(135, 60, 50, 50);
		bottoneSelezionePecoraBianca.setContentAreaFilled(false);
		bottoneSelezionePecoraBianca.setBorderPainted(false);
		bottoneSelezionePecoraBianca.setVisible(false);
		bottoneSelezionePecoraBianca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AnimaleSelezionato evento = new AnimaleSelezionato(); 
				evento.setAnimaleSelezionato(PECORA_BIANCA);
				processEvento(evento);
			}
		}

				);
		panelSelezioneAnimale.add(bottoneSelezionePecoraBianca);

		
		bottoneSelezionePecoraNera= new JButton();
		bottoneSelezionePecoraNera.setIcon(new ImageIcon(this.getClass().getResource("/pecoraNeraSelezione.png")));
		bottoneSelezionePecoraNera.setBounds(215, 60, 50, 50);
		bottoneSelezionePecoraNera.setContentAreaFilled(false);
		bottoneSelezionePecoraNera.setBorderPainted(false);
		bottoneSelezionePecoraNera.setVisible(false);
		bottoneSelezionePecoraNera.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AnimaleSelezionato evento = new AnimaleSelezionato(); 
				evento.setAnimaleSelezionato(PECORA_NERA);
				processEvento(evento);
			}
		}

				);
		panelSelezioneAnimale.add(bottoneSelezionePecoraNera);

		//pannello relativo al lancio del dado
		panelLancioDado = new PanelWithBackground(imgLancioDado);
		panelLancioDado.setBounds(60, 100, 340, 340);
		panelLancioDado.setOpaque(false);
		panelLancioDado.setVisible(false);
		panelLancioDado.setLayout(null);
		layeredPane.add(panelLancioDado, Integer.valueOf(3));

		dado = new JLabel();
		dado.setOpaque(false);
		dado.setIcon(new ImageIcon(this.getClass().getResource("/dado.png")));
		dado.setEnabled(true); 
		dado.setBounds(130, 90, 108, 108);

		panelLancioDado.add(dado);


		//inizio pannello relativo ai punteggi
		panelPunteggi = new PanelWithBackground(imgPunteggi);
		panelPunteggi.setBounds(5, 50, 440, 440);
		panelPunteggi.setOpaque(false);
		panelPunteggi.setVisible(false);
		panelPunteggi.setLayout(null);
		layeredPane.add(panelPunteggi, Integer.valueOf(3));

		//vengono usati per visualizzare delle barre colorate associare al punteggio del giocatore
		barraRossa = new JPanel();
		barraRossa.setBounds(20, 50, 260, 20);
		barraRossa.setVisible(false);
		barraRossa.setBackground(new Color(225, 3, 3));

		barraBlu = new JPanel();
		barraBlu.setBounds(20, 100, 260, 20);
		barraBlu.setVisible(false);
		barraBlu.setBackground(new Color(26,26,228));

		barraVerde = new JPanel();
		barraVerde.setBounds(20, 150, 260, 20);
		barraVerde.setVisible(false);
		barraVerde.setBackground(new Color(2, 182, 2));

		barraGialla = new JPanel();
		barraGialla.setBounds(20, 200, 260, 20);
		barraGialla.setVisible(false);
		barraGialla.setBackground(new Color(236, 227, 36));

		//label che scoprono la barra relativa al punteggio a seconda del punteggio totalizzato
		labelPunteggioRosso = new LabelWithBackground(imgScorrimentoRosso);
		labelPunteggioRosso.setVisible(false);
		labelPunteggioRosso.setBounds(10, 45, 407, 28);

		labelPunteggioBlu = new LabelWithBackground(imgScorrimentoBlu);
		labelPunteggioBlu.setVisible(false);
		labelPunteggioBlu.setBounds(10, 95, 407, 28);

		labelPunteggioVerde = new LabelWithBackground(imgScorrimentoVerde);
		labelPunteggioVerde.setVisible(false);
		labelPunteggioVerde.setBounds(10, 145, 407, 28);

		labelPunteggioGiallo = new LabelWithBackground(imgScorrimentoGiallo);
		labelPunteggioGiallo.setVisible(false);
		labelPunteggioGiallo.setBounds(10, 195, 407, 28);

		//label che indicano il punteggio numerico totalizzato dal giocatore
		labelPunteggioNumericoRosso = new JLabel();
		labelPunteggioNumericoRosso.setBounds(290, 53, 30, 10);
		labelPunteggioNumericoBlu = new JLabel(); 
		labelPunteggioNumericoBlu.setBounds(290, 103, 30, 10);
		labelPunteggioNumericoVerde = new JLabel(); 
		labelPunteggioNumericoVerde.setBounds(290, 153, 30, 10);
		labelPunteggioNumericoGiallo = new JLabel(); 
		labelPunteggioNumericoGiallo.setBounds(290, 203, 30, 10);

		JPanel panelBarre = new JPanel();
		panelBarre.setBackground(new Color(146, 231, 250));
		panelBarre.setBounds(10, 60, 385, 380);
		panelBarre.setVisible(true);

		JLayeredPane layerBarra = new JLayeredPane();

		layerBarra.add(panelBarre, Integer.valueOf(0));

		layerBarra.add(barraRossa, Integer.valueOf(1));
		layerBarra.add(barraBlu, Integer.valueOf(1));
		layerBarra.add(barraVerde, Integer.valueOf(1));
		layerBarra.add(barraGialla, Integer.valueOf(1));

		layerBarra.add(labelPunteggioRosso, Integer.valueOf(2));
		layerBarra.add(labelPunteggioBlu, Integer.valueOf(2));
		layerBarra.add(labelPunteggioVerde, Integer.valueOf(2));
		layerBarra.add(labelPunteggioGiallo, Integer.valueOf(2));

		layerBarra.add(labelPunteggioNumericoRosso, Integer.valueOf(3));
		layerBarra.add(labelPunteggioNumericoBlu, Integer.valueOf(3));
		layerBarra.add(labelPunteggioNumericoVerde, Integer.valueOf(3));
		layerBarra.add(labelPunteggioNumericoGiallo, Integer.valueOf(3));


		layerBarra.setBounds(50, 50, 345, 230);

		panelPunteggi.add(layerBarra);

		//fine pannello punteggi

		mainPanel.setLayout(new BorderLayout());
		mainPanel.setMaximumSize(new Dimension(900,646));
		//setto posizione dei nuovi pannelli nel mainPanel
		mainPanel.add(panelComunicazioni,BorderLayout.PAGE_START);
		mainPanel.add(panelTerreniEGiocatori, BorderLayout.LINE_START);
		mainPanel.add(panelMosse, BorderLayout.LINE_END);
		mainPanel.add(layeredPane,BorderLayout.CENTER);
		
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent){
	               ChiusuraFrameGiocatore evento = new ChiusuraFrameGiocatore(); 
	               processEvento(evento);
				
	         }  
		});
		frame.add(mainPanel);
		frame.setMinimumSize(new Dimension(1020, 646));
		frame.setMaximumSize(new Dimension(1020, 646));
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}
	
	/**
	 * invia l'evento generato ai propri observers
	 * @param evento: è l'evento che viene generato
	 */
	protected void processEvento(EventoGenerico evento) {
		setChanged(); 
		notifyObservers(evento); 
	}

	/**
	 * visualizza a video quali sono i punteggi realizzati dai giocatori
	 * @param punteggiGiocatori: contiene l'informazione relativa ai punteggi realizzati dai giocatori
	 * @param punteggioMassimo: è il massimo punteggio realizzato da un giocatore nella partita
	 */
	@Override
	public void generaRisultato(List<Integer> punteggiGiocatori, int punteggioMassimo) {
		panelPunteggi.setVisible(true);
		int coefficiente = (int) (220 / punteggioMassimo); 
		int xRosso = 0, xBlu = 0, xVerde = 0, xGiallo = 0, punteggioGiocatoreVerde = 0, punteggioGiocatoreGiallo = 0; 
		for (int i = 0; i < punteggiGiocatori.size(); i++) {
			switch(i) {
			case 0: xRosso = punteggiGiocatori.get(i) * coefficiente; 
			break; 
			case 1: xBlu = punteggiGiocatori.get(i) * coefficiente;
			break; 
			case 2: xVerde = punteggiGiocatori.get(i) * coefficiente;
			break; 
			case 3: xGiallo = punteggiGiocatori.get(i) * coefficiente;
			break; 
			default: 
				break; 
			}
		}
		final int xArrivoRosso = xRosso; 
		final int xArrivoBlu = xBlu; 
		final int xArrivoVerde = xVerde;
		final int xArrivoGiallo = xGiallo; 
		final int punteggioRosso = punteggiGiocatori.get(0); 
		final int punteggioBlu = punteggiGiocatori.get(1); 
		if(punteggiGiocatori.size() > 2) {
			punteggioGiocatoreVerde = punteggiGiocatori.get(2); 
		}
		final int punteggioVerde = punteggioGiocatoreVerde; 
		if(punteggiGiocatori.size() > 3) {
			punteggioGiocatoreGiallo = punteggiGiocatori.get(3); 
		}
		final int punteggioGiallo = punteggioGiocatoreGiallo; 


		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try{
							//rosso
							int posizioneXRosso=10;
							int posizioneYRosso= 45;
							labelPunteggioRosso.setBounds(posizioneXRosso, posizioneYRosso, 407, 28);
							labelPunteggioRosso.setVisible(true);
							barraRossa.setVisible(true);
							for (int x=0; x<xArrivoRosso; x++){
								posizioneXRosso++;
								labelPunteggioRosso.setBounds(posizioneXRosso, posizioneYRosso, 407, 28);

								Thread.sleep(10);
							} 
							labelPunteggioNumericoRosso.setText("" + punteggioRosso);
							labelPunteggioNumericoRosso.setVisible(true);
							//fine rosso

							//blu
							int posizioneXBlu=10;
							int posizioneYBlu= 95;
							labelPunteggioBlu.setBounds(posizioneXBlu, posizioneYBlu, 407, 28);
							labelPunteggioBlu.setVisible(true);
							barraBlu.setVisible(true);
							for (int x=0; x<xArrivoBlu; x++){
								posizioneXBlu++;
								labelPunteggioBlu.setBounds(posizioneXBlu, posizioneYBlu, 407, 28);

								Thread.sleep(10); 
							}
							labelPunteggioNumericoBlu.setText("" + punteggioBlu);
							labelPunteggioNumericoBlu.setVisible(true);
							//fine blu

							//verde
							int posizioneXVerde=10;
							int posizioneYVerde= 145;
							if(xArrivoVerde != 0) {
								barraVerde.setVisible(true);
								labelPunteggioVerde.setBounds(posizioneXVerde, posizioneYVerde, 407, 28);
								labelPunteggioVerde.setVisible(true);
								for (int x=0; x<xArrivoVerde; x++){
									posizioneXVerde++;
									labelPunteggioVerde.setBounds(posizioneXVerde, posizioneYVerde, 407, 28);

									Thread.sleep(10);
								} 
								labelPunteggioNumericoVerde.setText("" + punteggioVerde);
								labelPunteggioNumericoVerde.setVisible(true);
							}

							//fine verde	

							//giallo
							int posizioneXGiallo=10;
							int posizioneYGiallo= 195;
							if(xArrivoGiallo != 0) {
								labelPunteggioGiallo.setBounds(posizioneXGiallo, posizioneYGiallo, 407, 28);
								barraGialla.setVisible(true);
								labelPunteggioGiallo.setVisible(true);
								for (int x=0; x<xArrivoGiallo; x++){
									posizioneXGiallo++;
									labelPunteggioGiallo.setBounds(posizioneXGiallo, posizioneYGiallo, 407, 28);

									Thread.sleep(10);
								} 
								labelPunteggioNumericoGiallo.setText("" + punteggioGiallo);
								labelPunteggioNumericoGiallo.setVisible(true);
							}
							
						}catch(Exception e){
							LOGGER.log(Level.SEVERE, "errore nella generazione della classifica finale", e); 
						}
					}
				});
				t.start();
			}
		}, 1500);
	}

	/**
	 * visualizza a video una comunicazione
	 * @param string: è la comunicazione che si vuole visualizzare a video
	 */
	@Override
	public void setLabelComunicazioni(String string) {
		labelComunicazioni.setText(string);

	}
	
	/**
	 * attiva i bottoni delle mosse selezionabili
	 * @param mosseSelezionabili: contiene gli integer corrispondenti alle mosse selezionabili
	 */
	@Override
	public void attivaBottoniMosseSelezionabili(List<Integer> mosseSelezionabili) {
		for(JButton bottone : bottoniMosse) {
			bottone.setEnabled(false);
		}
		for(Integer numero : mosseSelezionabili) {
			bottoniMosse.get(numero).setEnabled(true);
		}
	}
	
	/**
	 * disattiva i bottoni delle mosse, eccetto uno (quello della mossa in corso)
	 * @param numero: è l'intero corrispondente alla mossa da non disattivare
	 */
	@Override
	public void disattivaBottoniMosse(int numero) {
		for(int j = 0; j<bottoniMosse.size(); j++ ) {
			if(j != numero) {
				bottoniMosse.get(j).setEnabled(false);
			}
		}
	}


	public void run() {
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * visualizza il valore del lancio del dado a video
	 * @param valoreLancioDado: risultato del lancio del dado
	 */
	@Override
	public void eseguiLancioDado(int valoreLancioDado) {
		dado.setIcon(new ImageIcon(this.getClass().getResource("/" + valoreLancioDado + ".png")));
		panelLancioDado.setVisible(true);
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				panelLancioDado.setVisible(false);

			}
		}, 1500);
	}

	/**
	 * rimuove la visualizzazione del risultato del lancio del dado
	 */
	@Override
	public void removeLancioDado() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			LOGGER.log(Level.SEVERE, "errore durante l'attesa del lancio", e); 
		}
		dado.setEnabled(false);
	}

	/**
	 * attiva i bottoni delle regioni
	 */
	@Override
	public void attivaBottoniRegioni() {
		panelMappa.addMouseListener(listenerRegioni);
	}
	
	/**
	 * disattiva i bottoni delle regioni 
	 */
	@Override
	public void disattivaBottoniRegioni() {
		panelMappa.removeMouseListener(listenerRegioni); 
	}

	/**
	 * attiva i bottoni delle strade selezionabili
	 * @param idStradeSelezionabili: id delle strade che possono essere selezionate
	 */
	@Override
	public void attivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		for(int i = 0; i < bottoniStrada.size(); i ++) {
			if(idStradeSelezionabili.contains(bottoniStrada.get(i).getId())) {
					bottoniStrada.get(i).setVisible(true);
					bottoniStrada.get(i).addActionListener(listenerStrade.get(i));
			} else {
				bottoniStrada.get(i).removeActionListener(listenerStrade.get(i));
			}
		
		}

	} 

	/**
	 * disattiva i bottoni delle strade selezionabili
	 * @param idStradeSelezionabili: id delle strade che possono essere selezionate
	 */
	@Override
	public void disattivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		for(String idStrada : idStradeSelezionabili) {
			for(int i = 0; i < bottoniStrada.size(); i++) {
				if(bottoniStrada.get(i).getId().equals(idStrada)) {
					bottoniStrada.get(i).setVisible(false);
					bottoniStrada.get(i).removeActionListener(listenerStrade.get(i));
				}
			}
		}
	}

	/**
	 * posiziona la pedina del giocatore in una strada
	 * @param idStradaArrivo: id della strada selezionata
	 * @param posizioneGiocatoreInArray: indice del giocatore
	 */
	@Override
	public void setIconaGiocatoreInizio(String idStradaArrivo, int posizioneGiocatoreInArray) {
		for(BottoneStrada bottone: bottoniStrada) {
			if(idStradaArrivo.equals(bottone.getId())) {
				switch(posizioneGiocatoreInArray) {
				case 0: bottone.setIcon(new ImageIcon(this.getClass().getResource("/pedinaRossaBordo.png")));
				break; 
				case 1: bottone.setIcon(new ImageIcon(this.getClass().getResource("/pedinaBluBordo.png")));
				break; 
				case 2: bottone.setIcon(new ImageIcon(this.getClass().getResource("/pedinaVerdeBordo.png")));
				break; 
				case 3: bottone.setIcon(new ImageIcon(this.getClass().getResource("/pedinaGiallaBordo.png")));
				break; 
				default: break; 
				}
				bottone.setBorderPainted(false);
			}
		}
	}

	/**
	 * posiziona la pedina del giocatore in una determinata strada
	 * @param idStradaArrivo: id della strada in cui arriva la pedina del giocatore
	 * @param coordinataXPartenza: coordinata x della strada di partenza
	 * @param coordinataYPartenza: coordinata y della strada di partenza
	 * @param coordinataXArrivo: coordinata x della strada di arrivo
	 * @param coordinataYArrivo: coordinata y della strada di arrivo
	 * @param posizioneGiocatoreInArray: indice del giocatore 
	 */
	@Override
	public void setIconaGiocatoreInStrada(String idStradaArrivo, int coordinataXPartenza, int coordinataYPartenza, int coordinataXArrivo, int coordinataYArrivo, int posizioneGiocatoreInArray) {
		final double xPartenza = coordinataXPartenza + 6; 
		final double xArrivo = coordinataXArrivo + 6; 
		final double yPartenza = coordinataYPartenza; 
		final double yArrivo = coordinataYArrivo ; 
		final int i = posizioneGiocatoreInArray;
		final String idArrivo = idStradaArrivo; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 
					JLabel labelMovimento = new JLabel();
					Icon icona; 
					switch(i) {
					case 0:
						icona = new ImageIcon(this.getClass().getResource("/pedinaRossaBordo.png")); 
						break;
					case 1:
						icona = new ImageIcon(this.getClass().getResource("/pedinaBluBordo.png")); 
						break;
					case 2:
						icona = new ImageIcon(this.getClass().getResource("/pedinaVerdeBordo.png")); 
						break;
					case 3:
						icona = new ImageIcon(this.getClass().getResource("/pedinaGiallaBordo.png")); 
						break;
					default:
						icona = new ImageIcon("resorces/pedinaBluBordo.png");
						break;
					}

					labelMovimento.setIcon(icona);
					labelMovimento.setVisible(true);
					labelMovimento.setBounds((int)xPartenza, (int)yPartenza, 16, 25);
					final double coeffX = (xArrivo-xPartenza)/100; 
					final double coeffY = (yArrivo - yPartenza)/100;
					
					layeredPane.add(labelMovimento, Integer.valueOf(1));

					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) Math.sqrt(Math.hypot(xArrivo - xPartenza, yArrivo - yPartenza)); 
					for(int i = 0; i < 100; i++) {
						x += coeffX; 
						y += coeffY; 
						labelMovimento.setBounds((int)x,(int) y, 30, 30);
						Thread.sleep(tempo); 
					}
					labelMovimento.setVisible(false);
					for(BottoneStrada bottone: bottoniStrada) {
						if(idArrivo.equals(bottone.getId())) {
							bottone.setIcon(icona);
							bottone.setBorderPainted(false);
						}
					}

				}catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		});
		t.start(); 
	}
	
	/**
	 * Posiziona un cancello su una strada
	 * @param idStradaPartenza: strada in cui posizionare il cancello
	 * @param faseFinale: se true, bisogna posizionare un cancello iniziale, altrimenti uno finale
	 */
	@Override
	public void posizionaCancello(String idStradaPartenza, boolean faseFinale) {
		for(BottoneStrada bottone: bottoniStrada) {
			if(idStradaPartenza.equals(bottone.getId()) && !faseFinale) {
				bottone.setIcon(new ImageIcon(this.getClass().getResource("/recinto.png")));
			} else if (idStradaPartenza.equals(bottone.getId()) && faseFinale) {
				bottone.setIcon(new ImageIcon(this.getClass().getResource("/recintoFinale.png")));
			}
		}

	}
	
	/**
	 * visualizza il totale degli agnelli nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale degli agnelli
	 * @param totaleAgnelli: totale agnelli presenti nella regione
	 */
	@Override
	public void setTotaleAgnelliInRegione(String idRegione, int totaleAgnelli) {
		for(LabelAnimali label : labelAgnelli) {
			if(idRegione.equals(label.getIdRegione())) {
				label.setText("Agnelli: " + totaleAgnelli);
			}	
		}
	}

	/**
	 * visualizza il totale degli arieti nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale degli arieti
	 * @param totaleArieti: totale arieti presenti nella regione
	 */
	@Override
	public void setTotaleArietiInRegione(String idRegione, int totaleArieti) {
		for(LabelAnimali label : labelArieti) {
			if(idRegione.equals(label.getIdRegione())) {
				label.setText("Arieti: " + totaleArieti);
			}	
		}
	}
	
	/**
	 * cambia la posizione del lupo
	 * @param idRegionePartenza: regione da cui è partito il lupo
	 * @param idRegioneArrivo: regione in cui è arrivato il lupo
	 */
	@Override
	public void setPosizioneLupo(String idRegionePartenza, String idRegioneArrivo) {
		BottoneElementi bottonePartenza = null; 
		BottoneElementi bottoneArrivo = null; 
		Integer indice = null; 

		final double xPartenza; 
		final double xArrivo;
		final double yPartenza;
		final double yArrivo;
		for(int i = 0; i < bottoniLupo.size(); i++)  {
			if(idRegionePartenza.equals(bottoniLupo.get(i).getIdRegione())) {
				bottoniLupo.get(i).setVisible(false);
				bottonePartenza = bottoniLupo.get(i); 
			}
			if (idRegioneArrivo.equals(bottoniLupo.get(i).getIdRegione())) {
				indice = i; 
				bottoneArrivo = bottoniLupo.get(i); 
			}
		}		
		final int posizioneInArray = indice; 
		xPartenza = bottonePartenza.getCoordinataX(); 
		yPartenza = bottonePartenza.getCoordinataY() - 3;
		xArrivo = bottoneArrivo.getCoordinataX(); 
		yArrivo = bottoneArrivo.getCoordinataY() - 3; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 
					JLabel labelMovimentoLupo = new JLabel();
					labelMovimentoLupo.setIcon(new ImageIcon(this.getClass().getResource("/lupoa.png")));
					labelMovimentoLupo.setVisible(true);
					labelMovimentoLupo.setBounds((int)xPartenza, (int)yPartenza, 16, 25);
					final double coeffX = (xArrivo-xPartenza)/100; 
					final double coeffY = (yArrivo - yPartenza)/100;

					layeredPane.add(labelMovimentoLupo, Integer.valueOf(1));

					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) Math.sqrt(Math.hypot(xArrivo - xPartenza, yArrivo - yPartenza)); 
					for(int i = 0; i < 100; i++) {
						x += coeffX; 
						y += coeffY; 
						labelMovimentoLupo.setBounds((int)x,(int) y, 30, 30);
						Thread.sleep(tempo); 
					}
					labelMovimentoLupo.setVisible(false);
					bottoniLupo.get(posizioneInArray).setVisible(true);
				} catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		});
		t.start(); 
	}

	/**
	 * cambia la posizione della pecora nera
	 * @param idRegionePartenza: regione da cui è partita la pecora nera
	 * @param idRegioneArrivo: regione in cui è arrivata la pecora nera
	 */
	@Override
	public void setPosizionePecoraNera(String idRegionePartenza, String idRegioneArrivo) {
		BottoneElementi bottonePartenza = null; 
		BottoneElementi bottoneArrivo = null; 
		Integer indice = null; 

		final double xPartenza; 
		final double xArrivo;
		final double yPartenza;
		final double yArrivo;
		for(int i = 0; i < bottoniPecoraNera.size(); i++)  {
			if(idRegionePartenza.equals(bottoniPecoraNera.get(i).getIdRegione())) {
				bottoniPecoraNera.get(i).setVisible(false);
				bottonePartenza = bottoniPecoraNera.get(i); 
			}
			if (idRegioneArrivo.equals(bottoniPecoraNera.get(i).getIdRegione())) {
				indice = i; 
				bottoneArrivo = bottoniPecoraNera.get(i); 
			}
		}		
		final int posizioneInArray = indice; 
		xPartenza = bottonePartenza.getCoordinataX(); 
		yPartenza = bottonePartenza.getCoordinataY() - 3;
		xArrivo = bottoneArrivo.getCoordinataX(); 
		yArrivo = bottoneArrivo.getCoordinataY() - 3; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 
					JLabel labelMovimentoPecoraNera = new JLabel();
					labelMovimentoPecoraNera.setIcon(new ImageIcon(this.getClass().getResource("/pecoraNera.png")));
					labelMovimentoPecoraNera.setVisible(true);
					labelMovimentoPecoraNera.setBounds((int)xPartenza, (int)yPartenza, 16, 25);
					final double coeffX = (xArrivo-xPartenza)/100; 
					final double coeffY = (yArrivo - yPartenza)/100;

					layeredPane.add(labelMovimentoPecoraNera, Integer.valueOf(1));

					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) Math.sqrt(Math.hypot(xArrivo - xPartenza, yArrivo - yPartenza)); 
					for(int i = 0; i < 100; i++) {
						x += coeffX; 
						y += coeffY; 
						labelMovimentoPecoraNera.setBounds((int)x,(int) y, 30, 30);
						Thread.sleep(tempo); 
					}
					labelMovimentoPecoraNera.setVisible(false);
					bottoniPecoraNera.get(posizioneInArray).setVisible(true);
				} catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		});
		t.start(); 
	}
	
	/**
	 * visualizza a video lo spostamento di un animale
	 * @param idRegionePartenza: regione da cui parte l'animale
	 * @param idRegioneArrivo: regione in cui arriva l'animale
	 * @param animale: animale che è stato spostato
	 */
	@Override
	public void spostamentoAnimale(String idRegionePartenza, String idRegioneArrivo, String animale) {
		BottoneElementi bottonePartenza = null; 
		BottoneElementi bottoneArrivo = null; 
		int indice = 0; 
		final double xPartenza; 
		final double xArrivo;
		final double yPartenza;
		final double yArrivo;
		for(int i = 0; i < bottoniPecoreBianche.size(); i++)  {
			if(idRegionePartenza.equals(bottoniPecoreBianche.get(i).getIdRegione())) {
				bottonePartenza = bottoniPecoreBianche.get(i); 
			}
			if (idRegioneArrivo.equals(bottoniPecoreBianche.get(i).getIdRegione())) {
				bottoneArrivo = bottoniPecoreBianche.get(i); 
				indice = i; 
			}
		}
		final int posizioneInArrayBottoneArrivo = indice; 
		final String animaleScelto = animale; 
		xPartenza = bottonePartenza.getCoordinataX() ; 
		yPartenza = bottonePartenza.getCoordinataY() ;
		xArrivo = bottoneArrivo.getCoordinataX() ; 
		yArrivo = bottoneArrivo.getCoordinataY() ; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 
					JLabel labelMovimentoAnimale = new JLabel();
					Icon icona; 
					if(PECORA_BIANCA.equals(animaleScelto)) {
						icona = new ImageIcon(this.getClass().getResource("/pecoraBianca.png")); 
					} else {
						icona = new ImageIcon(this.getClass().getResource("/arieteMovimento.png")); 
					}

					labelMovimentoAnimale.setIcon(icona);
					labelMovimentoAnimale.setVisible(true);
					labelMovimentoAnimale.setBounds((int)xPartenza, (int)yPartenza, 16, 25);
					final double coeffX = (xArrivo-xPartenza)/100; 
					final double coeffY = (yArrivo - yPartenza)/100;

					layeredPane.add(labelMovimentoAnimale, Integer.valueOf(1));


					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) Math.sqrt(Math.hypot(xArrivo - xPartenza, yArrivo - yPartenza)); 
					for(int i = 0; i < 100; i++) {
						x += coeffX; 
						y += coeffY; 
						labelMovimentoAnimale.setBounds((int)x,(int) y, 30, 30);
						Thread.sleep(tempo); 
					}
					labelMovimentoAnimale.setVisible(false);
					bottoniPecoreBianche.get(posizioneInArrayBottoneArrivo).setVisible(true);
				} catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		}); 
		t.start(); 
	}

	
	/**
	 * modifica quello che è il giocatore del turno
	 * @param nomeGiocatore: nome del giocatore del turno
	 */
	@Override
	public void setGiocatoreDelTurno(String nomeGiocatore) {
		labelNomeGiocatore.setText("E' il turno di: " + nomeGiocatore); 
	}

	/**
	 * attiva i bottoni delle tessere terreno che sono selezionabili
	 * @param numeriTessereCorrispondenti: sono gli interi che corrispondono alle tessere che possono essere selezionate
	 */
		@Override
	public void attivaBottoniTessereSelezionabili(List<Integer> numeriTessereCorrispondenti) {
		for(int i = 0; i < bottoniTessereTerreno.size(); i++) {
			if(numeriTessereCorrispondenti.contains(i)) {
				bottoniTessereTerreno.get(i).setEnabled(true);
				bottoniTessereTerreno.get(i).addActionListener(listenerTessereTerreno.get(i));
			} else {
				bottoniTessereTerreno.get(i).setEnabled(false);
				bottoniTessereTerreno.get(i).addActionListener(listenerTessereTerreno.get(i));
			}
		}
	}
	
	/**
	 * disattiva i bottoni delle tessere terreno
	 */
	@Override
	public void disattivaBottoniTessereTerreno() {
		for(int i = 0; i < bottoniTessereTerreno.size(); i++) {
			bottoniTessereTerreno.get(i).setEnabled(true);
			bottoniTessereTerreno.get(i).removeActionListener(listenerTessereTerreno.get(i));
		}
	}

	/**
	 * visualizza quali sono i danari in possesso del giocatore
	 * @param indice: indice del giocatore di cui bisogna visualizzare i danari in possesso
	 * @param danari: totale danari posseduti dal giocatore
	 */
	@Override
	public void setDanariGiocatore (int indice, int danari) {
		labelDanari.get(indice).setText("" + danari);
	}

	/**
	 * visualizza il totale di pecore bianche nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale delle pecore bianche
	 * @param totalePecoreBianche: totale pecore bianche presenti nella regione
	 */
	@Override
	public void setTotalePecoreBiancheInRegione(String idRegione, int totalePecoreBianche) {
		for(LabelAnimali label : labelPecoreBianche) {
			if(idRegione.equals(label.getIdRegione())) {
				label.setText("Pecore: " + totalePecoreBianche);
			}

		}
	}

	/**
	 * modifica la visibilità del bottone degli animali della regione
	 * @param idRegione: regione in cui modificare la visibilità
	 * @param condizione: se è true il bottone è settato come visibile, se è false non visibile
	 */
	@Override
	public void visibilityBottoniAnimali(String idRegione, boolean condizione) {
		for (BottoneElementi bottone: bottoniPecoreBianche) {
			if(idRegione.equals(bottone.getIdRegione())) {
				bottone.setVisible(condizione);
			}

		}
	}

	/**
	 * disattiva tutti i bottoni delle mosse
	 */
	@Override
	public void disattivaBottoniMosse() {
		for(JButton bottone: bottoniMosse) {
			bottone.setEnabled(false);
		}
	}

	/**
	 * inserisce il giocatore graficamente
	 * @param indice: indice del giocatore.
	 */
	@Override
	public void inserisciGiocatore(int indice) {
		bottoniGiocatori.get(indice).setText(nomiGiocatori.get(indice));
	}
	
	

	/**
	 * disattiva il pannello di selezione degli animali
	 */
	@Override
	public void disattivaPannelloSelezioneAnimali() {
		panelSelezioneAnimale.setVisible(false); 
		bottoneSelezioneAriete.setEnabled(false);
		bottoneSelezioneAriete.setVisible(false);
		bottoneSelezionePecoraBianca.setEnabled(false);
		bottoneSelezionePecoraBianca.setVisible(false);
		bottoneSelezionePecoraNera.setEnabled(false);
		bottoneSelezionePecoraNera.setVisible(false);

	}

	/**
	 * visualizza il costo della tessera terreno, settando l'immagine appropriata
	 * @param tipo: tipo di terreno di cui è cambiato il costo
	 * @param nuovoCosto: nuovo costo di quel tipo di terreno
	 */
	@Override
	public void setCostoTesseraTerreno(int tipo, int nuovoCosto) {
		switch (nuovoCosto) {
		case 1: bottoniTessereTerreno.get(tipo).setIcon(new ImageIcon(this.getClass().getResource("/terreno"+ tipo +"/costo1.png")));
		break; 
		case 2: bottoniTessereTerreno.get(tipo).setIcon(new ImageIcon(this.getClass().getResource("/terreno" + tipo + "/costo2.png")));
		break; 
		case 3: bottoniTessereTerreno.get(tipo).setIcon(new ImageIcon(this.getClass().getResource("/terreno" + tipo + "/costo3.png")));
		break; 
		case 4: bottoniTessereTerreno.get(tipo).setIcon(new ImageIcon(this.getClass().getResource("/terreno" + tipo + "/costo4.png")));
		break; 
		default: 
			break;
		}
	}


	/**
	 * attiva i bottoni degli animali selezionabili
	 * @param animaliSelezionabili: contiene gli integer corrispondenti agli animali selezionabili
	 */
	@Override
	public void attivaAnimaliSelezionabili(List<Integer> animaliSelezionabili) {
		panelSelezioneAnimale.setVisible(true);
		for(int i = 0; i< animaliSelezionabili.size(); i++) {
			if(animaliSelezionabili.get(i)==0) {
				bottoneSelezionePecoraBianca.setEnabled(true);
				bottoneSelezionePecoraBianca.setVisible(true);
			} else if(animaliSelezionabili.get(i)==1){
				bottoneSelezioneAriete.setEnabled(true);
				bottoneSelezioneAriete.setVisible(true);
			} else if(animaliSelezionabili.get(i)==2) {
				bottoneSelezionePecoraNera.setEnabled(true);
				bottoneSelezionePecoraNera.setVisible(true);
			}
		}
	}

	/**
	 * visualizza a video la morte di un animale
	 * @param tipoAnimale: animale che è morto
	 * @param idRegione: regione in cui è morto l'animale
	 */
	@Override
	public void morteAnimale(String tipoAnimale, String idRegione) {
		BottoneElementi bottonePartenza = null; 
		for(int i = 0; i < bottoniPecoreBianche.size(); i++)  {
			if(idRegione.equals(bottoniPecoreBianche.get(i).getIdRegione())) {
				bottonePartenza = bottoniPecoreBianche.get(i); 
			}
		}
		final String animale = tipoAnimale; 
		final double xPartenza = bottonePartenza.getCoordinataX(); 
		final double yPartenza = bottonePartenza.getCoordinataY(); 
		final double xArrivo = 260; 
		final double yArrivo = 600; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 

					JLabel animaleMorto = new JLabel(); 
					Icon icona; 
					if(PECORA_BIANCA.equals(animale)) {
						icona = new ImageIcon(this.getClass().getResource("/pecoraBiancaMortaMovimento.png")); 
					} else {
						icona = new ImageIcon(this.getClass().getResource("/arieteMortoMovimento.png"));
					}
					animaleMorto.setIcon(icona);

					animaleMorto.setVisible(true);
					animaleMorto.setBounds((int)xPartenza, (int)yPartenza, 16, 25);
					final double coeffX = (xArrivo-xPartenza)/100; 
					final double coeffY = (yArrivo - yPartenza)/100;

					layeredPane.add(animaleMorto, Integer.valueOf(1));


					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) Math.sqrt(Math.hypot(xArrivo - xPartenza, yArrivo - yPartenza)); 
					for(int i = 0; i < 100; i++) {
						x += coeffX; 
						y += coeffY; 
						animaleMorto.setBounds((int)x,(int) y, 30, 30);
						Thread.sleep(tempo); 
					}
					animaleMorto.setVisible(false);
				} catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		}); 
		t.start(); 
	}

	/**
	 * visualizza a video la nascita di un agnello
	 * @param idRegione: regione in cui è nato l'agnello
	 */
	@Override
	public void nascitaAgnello(String idRegione) {
		BottoneElementi bottoneArrivo = null; 
		for(int i = 0; i < bottoniPecoreBianche.size(); i++)  {
			if(idRegione.equals(bottoniPecoreBianche.get(i).getIdRegione())) {
				bottoneArrivo = bottoniPecoreBianche.get(i); 
			}
		}
		final double xPartenza = 0; 
		final double yPartenza = bottoneArrivo.getCoordinataY() - 80;  
		final double xIntermedio = bottoneArrivo.getCoordinataX(); 
		final double xArrivo = 480; 
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{ 
					JLabel cicogna = new JLabel(); 
					Icon iconaCicogna = new ImageIcon(this.getClass().getResource("/cicognaRosa.png"));
					cicogna.setIcon(iconaCicogna);
					cicogna.setVisible(true);
					cicogna.setBounds((int)xPartenza, (int)yPartenza, 90, 60);

					JLabel animaleNato = new JLabel(); 
					Icon iconaAgnello = new ImageIcon(this.getClass().getResource("/agnelloNato.png")); 
					animaleNato.setIcon(iconaAgnello);
					animaleNato.setVisible(false);
					animaleNato.setBounds((int)xIntermedio, (int)yPartenza +50, 30, 30);

					layeredPane.add(cicogna, Integer.valueOf(1));
					layeredPane.add(animaleNato, Integer.valueOf(1)); 

					double x = xPartenza; 
					double y = yPartenza; 
					final long tempo = (long) (xArrivo - xPartenza)/100;  
					for(x = xPartenza; x < xArrivo; x++) {
						if(x == xIntermedio) {
							animaleNato.setVisible(true);
						}
						cicogna.setBounds((int)x,(int) y, 90, 60);
						Thread.sleep(tempo); 
					}
					cicogna.setVisible(false);
					animaleNato.setVisible(false);
				} catch(Exception e){
					LOGGER.log(Level.SEVERE, "errore nello spostamento dinamico", e);
				}
			}
		}); 
		t.start(); 


	}

	/**
	 * modifica la quantità di tessere in possesso di un giocatore
	 * @param tessereInPossesso: contiene informazioni riguardanti le tessere in possesso del giocatore
	 * @param indiceGiocatore: indice del giocatore di cui si vuole modificare a video il numero di tessere
	 * che possiede
	 */
	@Override
	public void setTessereInPossesso(List<Integer> tessereInPossesso, int indiceGiocatore) {
		for(int i = 0; i < bottoniTessereTerreno.size(); i++) {
			bottoniTessereTerreno.get(i).setText("" + tessereInPossesso.get(i));
		}

	}

	/**
	 * visualizza a video l'informazione che non ci sono più mosse disponibili
	 */
	@Override
	public void avvisoMosseNonDisponibili() {
		panelMosseNonDisponibili.setVisible(true);
		
	}

	/**
	 * disattiva la vista del pannello che segnala che non ci sono più mosse disponibili.
	 */
	@Override
	public void disattivaPannelloMosseNonDisponibili() {
		panelMosseNonDisponibili.setVisible(false);
		
	}

	/**
	 * fa chiudere la schermata di gioco
	 */
	@Override
	public void chiudiSchermata() {
		frame.dispose();
	}
	
	public void aggiornaSchermataPrincipale(EventoGenerico evento, int indice) {
		switch(evento.getTipo()) {
		case ANIMALE_SELEZIONATO:
			break;
		case ATTIVA_ANIMALI_SELEZIONABILI:
			attivaAnimaliSelezionabili(((AttivaAnimaliSelezionabili)evento).getAnimaliSelezionabili());
			break;
		case ATTIVA_BOTTONI_MOSSE_SELEZIONABILI:
			attivaBottoniMosseSelezionabili(((AttivaBottoniMosseSelezionabili)evento).getMosseSelezionabili());
			break;
		case ATTIVA_BOTTONI_REGIONI:
			attivaBottoniRegioni();
			break;
		case ATTIVA_BOTTONI_STRADE_SELEZIONABILI:
			attivaBottoniStradeSelezionabili(((AttivaBottoniStradeSelezionabili)evento).getIdStradeSelezionabili());
			break;
		case ATTIVA_BOTTONI_TESSERE_SELEZIONABILI:
			attivaBottoniTessereSelezionabili(((AttivaBottoniTessereSelezionabili)evento).getNumeriTessereCorrispondenti());
			break;
		case AVVISO_MOSSE_NON_DISPONIBILI: 
			avvisoMosseNonDisponibili(); 
			break; 
		case CHIUSURA_FRAME_GIOCATORE:
			chiudiSchermata();
			break; 
		case DISATTIVA_BOTTONI_MOSSE:
			disattivaBottoniMosse(((DisattivaBottoniMosse)evento).getNumeroMossaDaNonDisattivare());
			break;
		case DISATTIVA_BOTTONI_REGIONI:
			disattivaBottoniRegioni();
			break;
		case DISATTIVA_PANNELLO_MOSSE_NON_DISPONIBILI:
			disattivaPannelloMosseNonDisponibili();
			break; 
		case DISATTIVA_BOTTONI_STRADE_SELEZIONABILI:
			disattivaBottoniStradeSelezionabili(((DisattivaBottoniStradeSelezionabili)evento).getIdStradeSelezionabili());
			break;
		case DISATTIVA_BOTTONI_TESSERE_TERRENO:
			disattivaBottoniTessereTerreno();
			break;
		case DISATTIVA_TUTTI_BOTTONI_MOSSE:
			disattivaBottoniMosse();
			break;
		case DISATTIVA_PANNELLO_SELEZIONE_ANIMALI:
			disattivaPannelloSelezioneAnimali();
			break; 
		case ESEGUI_LANCIO_DADO:
			eseguiLancioDado(((EseguiLancioDado)evento).getValoreLancioDado()); 
			break;
		case EVENTO_GENERICO:
			break;
		case GENERA_RISULTATO: 
			generaRisultato(((GeneraRisultato)evento).getPunteggiGiocatori(), ((GeneraRisultato)evento).getPunteggioMassimo()); 
			break; 
		case INSERIMENTO_GIOCATORE:
			break;
		case INSERIMENTO_NOME_GIOCATORE:
			break;
		case INSERISCI_GIOCATORE:
			inserisciGiocatore(((InserisciGiocatore)evento).getIndice()); 
			break;
		case MORTE_ANIMALE: 
			morteAnimale(((MorteAnimale)evento).getTipoAnimale(), ((MorteAnimale)evento).getIdRegione());
			break; 
		case NASCITA_AGNELLO: 
			nascitaAgnello(((NascitaAgnello)evento).getIdRegione());
			break; 
		case POSIZIONA_CANCELLO:
			posizionaCancello(((PosizionaCancello)evento).getIdStradaPartenza(),((PosizionaCancello)evento).isFaseFinale());
			break;
		case REGIONE_SELEZIONATA:
			break;
		case REMOVE_LANCIO_DADO:
			removeLancioDado();
			break;
		case SET_COSTO_TESSERA_TERRENO:
			setCostoTesseraTerreno(((SetCostoTesseraTerreno)evento).getTipoTerreno(), ((SetCostoTesseraTerreno)evento).getNuovoCosto()); 
			break;
		case SET_DANARI_GIOCATORE:
			setDanariGiocatore(((SetDanariGiocatore)evento).getIndice(), ((SetDanariGiocatore)evento).getDanari());
			break;
		case SET_GIOCATORE_DEL_TURNO:
			setGiocatoreDelTurno(((SetGiocatoreDelTurno)evento).getNomeGiocatore());
			break;
		case SET_ICONA_GIOCATORE_INIZIO:
			setIconaGiocatoreInizio(((SetIconaGiocatoreInizio)evento).getIdStradaArrivo(), ((SetIconaGiocatoreInizio)evento).getPosizioneGiocatoreInArray());
			break;
		case SET_ICONA_GIOCATORE_IN_STRADA:
			setIconaGiocatoreInStrada(((SetIconaGiocatoreInStrada)evento).getIdStradaArrivo(), ((SetIconaGiocatoreInStrada)evento).getCoordinataXPartenza(), ((SetIconaGiocatoreInStrada)evento).getCoordinataYPartenza(), ((SetIconaGiocatoreInStrada)evento).getCoordinataXArrivo(), ((SetIconaGiocatoreInStrada)evento).getCoordinataYArrivo(), ((SetIconaGiocatoreInStrada)evento).getPosizioneGiocatoreInArray());
			break;
		case SET_LABEL_COMUNICAZIONI:
			setLabelComunicazioni(((SetLabelComunicazioni)evento).getString());
			break;
		case SET_POSIZIONE_LUPO:
			setPosizioneLupo(((SetPosizioneLupo)evento).getIdRegionePartenza(), ((SetPosizioneLupo)evento).getIdRegioneArrivo());
			break;
		case SET_POSIZIONE_PECORA_NERA:
			setPosizionePecoraNera(((SetPosizionePecoraNera)evento).getIdRegionePartenza(), ((SetPosizionePecoraNera)evento).getIdRegioneArrivo());
			break;
		case SET_TESSERE_IN_POSSESSO: 
			if(((SetTessereInPossesso)evento).getIndiceGiocatore() == indice) {
				setTessereInPossesso(((SetTessereInPossesso)evento).getTessereInPossesso(), ((SetTessereInPossesso)evento).getIndiceGiocatore());
			}
			break; 
		case SET_TOTALE_AGNELLI_IN_REGIONE:
			setTotaleAgnelliInRegione(((SetTotaleAgnelliInRegione)evento).getIdRegione(), ((SetTotaleAgnelliInRegione)evento).getTotaleAgnelli());
			break;
		case SET_TOTALE_ARIETI_IN_REGIONE:
			setTotaleArietiInRegione(((SetTotaleArietiInRegione)evento).getIdRegione(), ((SetTotaleArietiInRegione)evento).getTotaleArieti());
			break;
		case SET_TOTALE_PECORE_BIANCHE_IN_REGIONE:
			setTotalePecoreBiancheInRegione(((SetTotalePecoreBiancheInRegione)evento).getIdRegione(), ((SetTotalePecoreBiancheInRegione)evento).getTotalePecoreBianche());
			break;
		case SPOSTAMENTO_ANIMALE:
			spostamentoAnimale(((SpostamentoAnimale)evento).getIdRegionePartenza(), ((SpostamentoAnimale)evento).getIdRegioneArrivo(), ((SpostamentoAnimale)evento).getAnimale());
			break;
		case STRADA_SELEZIONATA:
			break;
		case TESSERA_TERRENO_SELEZIONATA:
			break;
		case VISIBILITY_BOTTONI_ANIMALI:
			visibilityBottoniAnimali(((VisibilityBottoniAnimali)evento).getIdRegione(), ((VisibilityBottoniAnimali)evento).isCondizione());
			break;
		default:
			break;
		
		}
	}

}