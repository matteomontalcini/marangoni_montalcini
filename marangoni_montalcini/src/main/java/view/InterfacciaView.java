package view;

import java.util.List;

public interface InterfacciaView {

	/**
	 * Posiziona un cancello su una strada
	 * @param idStradaPartenza: strada in cui posizionare il cancello
	 * @param faseFinale: se true, bisogna posizionare un cancello iniziale, altrimenti uno finale
	 */
	void posizionaCancello(String idStradaPartenza, boolean faseFinale); 
	
	/**
	 * attiva i bottoni delle tessere terreno che sono selezionabili
	 * @param numeriTessereCorrispondenti: sono gli interi che corrispondono alle tessere che possono essere selezionate
	 */
	void attivaBottoniTessereSelezionabili(List<Integer> numeriTessereCorrispondenti); 
	
	/**
	 * disattiva i bottoni delle tessere terreno
	 */
	void disattivaBottoniTessereTerreno(); 
	
	/**
	 * attiva i bottoni delle strade selezionabili
	 * @param idStradeSelezionabili: id delle strade che possono essere selezionate
	 */
	void attivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili);  
	
	/**
	 * disattiva i bottoni delle strade selezionabili
	 * @param idStradeSelezionabili: id delle strade che possono essere selezionate
	 */
	void disattivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili); 
	
	/**
	 * posiziona la pedina del giocatore in una determinata strada
	 * @param idStradaArrivo: id della strada in cui arriva la pedina del giocatore
	 * @param coordinataXPartenza: coordinata x della strada di partenza
	 * @param coordinataYPartenza: coordinata y della strada di partenza
	 * @param coordinataXArrivo: coordinata x della strada di arrivo
	 * @param coordinataYArrivo: coordinata y della strada di arrivo
	 * @param posizioneGiocatoreInArray: indice del giocatore 
	 */
	void setIconaGiocatoreInStrada(String idStradaArrivo, int coordinataXPartenza, int coordinataYPartenza, int coordinataXArrivo, int coordinataYArrivo, int posizioneGiocatoreInArray); 
	
	/**
	 * posiziona la pedina del giocatore in una strada
	 * @param idStradaArrivo: id della strada selezionata
	 * @param posizioneGiocatoreInArray: indice del giocatore
	 */
	void setIconaGiocatoreInizio(String idStradaArrivo, int posizioneGiocatoreInArray); 
	
	/**
	 * visualizza il valore del lancio del dado a video
	 * @param valoreLancioDado: risultato del lancio del dado
	 */
	void eseguiLancioDado(int valoreLancioDado); 
	
	/**
	 * rimuove la visualizzazione del risultato del lancio del dado
	 */
	void removeLancioDado(); 
	
	/**
	 * visualizza a video una comunicazione
	 * @param string: è la comunicazione che si vuole visualizzare a video
	 */
	void setLabelComunicazioni(String string); 
	
	/**
	 * attiva i bottoni degli animali selezionabili
	 * @param animaliSelezionabili: contiene gli integer corrispondenti agli animali selezionabili
	 */
	void attivaAnimaliSelezionabili(List<Integer> animaliSelezionabili);
	
	/**
	 * attiva i bottoni delle regioni
	 */
	void attivaBottoniRegioni(); 
	
	/**
	 * disattiva i bottoni delle regioni 
	 */
	void disattivaBottoniRegioni(); 
	
	/**
	 * attiva i bottoni delle mosse selezionabili
	 * @param mosseSelezionabili: contiene gli integer corrispondenti alle mosse selezionabili
	 */
	void attivaBottoniMosseSelezionabili(List<Integer> mosseSelezionabili); 
	
	/**
	 * disattiva i bottoni delle mosse, eccetto uno (quello della mossa in corso)
	 * @param numero: è l'intero corrispondente alla mossa da non disattivare
	 */
	void disattivaBottoniMosse(int numero); 
	
	/**
	 * visualizza quali sono i danari in possesso del giocatore
	 * @param indice: indice del giocatore di cui bisogna visualizzare i danari in possesso
	 * @param danari: totale danari posseduti dal giocatore
	 */
	void setDanariGiocatore (int indice, int danari); 
	
	/**
	 * visualizza il costo della tessera terreno, settando l'immagine appropriata
	 * @param tipo: tipo di terreno di cui è cambiato il costo
	 * @param nuovoCosto: nuovo costo di quel tipo di terreno
	 */
	void setCostoTesseraTerreno(int tipo, int nuovoCosto); 
	
	/**
	 * visualizza il totale di pecore bianche nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale delle pecore bianche
	 * @param totalePecoreBianche: totale pecore bianche presenti nella regione
	 */
	void setTotalePecoreBiancheInRegione(String idRegione, int totalePecoreBianche); 
	
	/**
	 * visualizza il totale degli agnelli nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale degli agnelli
	 * @param totaleAgnelli: totale agnelli presenti nella regione
	 */
	 void setTotaleAgnelliInRegione(String idRegione, int totaleAgnelli); 
	
	/**
	 * visualizza il totale degli arieti nel popup della regione corrispondente
	 * @param idRegione: regione in cui va modificato il totale degli arieti
	 * @param totaleArieti: totale arieti presenti nella regione
	 */
	void setTotaleArietiInRegione(String idRegione, int totaleArieti); 
	
	/**
	 * cambia la posizione del lupo
	 * @param idRegionePartenza: regione da cui è partito il lupo
	 * @param idRegioneArrivo: regione in cui è arrivato il lupo
	 */
	void setPosizioneLupo(String idRegionePartenza, String idRegioneArrivo);
	
	/**
	 * cambia la posizione della pecora nera
	 * @param idRegionePartenza: regione da cui è partita la pecora nera
	 * @param idRegioneArrivo: regione in cui è arrivata la pecora nera
	 */
	void setPosizionePecoraNera(String idRegionePartenza, String idRegioneArrivo); 
	
	/**
	 * modifica quello che è il giocatore del turno
	 * @param nomeGiocatore: nome del giocatore del turno
	 */
	void setGiocatoreDelTurno(String nomeGiocatore); 
	
	/**
	 * inserisce il giocatore graficamente
	 * @param indice: indice del giocatore.
	 */
	void inserisciGiocatore(int indice); 
	
	
	/**
	 * disattiva tutti i bottoni delle mosse
	 */
	void disattivaBottoniMosse(); 
	
	/**
	 * modifica la visibilità del bottone degli animali della regione
	 * @param idRegione: regione in cui modificare la visibilità
	 * @param condizione: se è true il bottone è settato come visibile, se è false non visibile
	 */
	void visibilityBottoniAnimali(String idRegione, boolean condizione); 
	
	/**
	 * visualizza a video lo spostamento di un animale
	 * @param idRegionePartenza: regione da cui parte l'animale
	 * @param idRegioneArrivo: regione in cui arriva l'animale
	 * @param animale: animale che è stato spostato
	 */
	void spostamentoAnimale(String idRegionePartenza, String idRegioneArrivo, String animale); 
	
	/**
	 * disattiva il pannello di selezione degli animali
	 */
	void disattivaPannelloSelezioneAnimali(); 
	
	/**
	 * visualizza a video la morte di un animale
	 * @param tipoAnimale: animale che è morto
	 * @param idRegione: regione in cui è morto l'animale
	 */
	void morteAnimale(String tipoAnimale, String idRegione); 
	
	/**
	 * visualizza a video la nascita di un agnello
	 * @param idRegione: regione in cui è nato l'agnello
	 */
	void nascitaAgnello(String idRegione); 
	
	/**
	 * modifica la quantità di tessere in possesso di un giocatore
	 * @param tessereInPossesso: contiene informazioni riguardanti le tessere in possesso del giocatore
	 * @param indiceGiocatore: indice del giocatore di cui si vuole modificare a video il numero di tessere
	 * che possiede
	 */
	void setTessereInPossesso(List<Integer> tessereInPossesso, int indiceGiocatore); 
	
	/**
	 * visualizza a video quali sono i punteggi realizzati dai giocatori
	 * @param punteggiGiocatori: contiene l'informazione relativa ai punteggi realizzati dai giocatori
	 * @param punteggioMassimo: è il massimo punteggio realizzato da un giocatore nella partita
	 */
	void generaRisultato(List<Integer> punteggiGiocatori, int punteggioMassimo); 
	
	/**
	 * visualizza a video l'informazione che non ci sono più mosse disponibili
	 */
	void avvisoMosseNonDisponibili(); 
	
	/**
	 * disattiva la vista del pannello che segnala che non ci sono più mosse disponibili.
	 */
	void disattivaPannelloMosseNonDisponibili(); 
	
	/**
	 * fa chiudere la schermata di gioco
	 */
	void chiudiSchermata(); 
	
}

