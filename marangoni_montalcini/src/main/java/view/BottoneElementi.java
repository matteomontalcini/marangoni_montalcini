package view;

import java.awt.Image;

/** 
 * Classe che permette di associare ad un bottone una coordinata x, una coordinata y
 * per rappresentarlo sulla mappa, e l'id della regione di appartenenza corrispondente.
 * Usato per rappresentare pecore bianche, lupo e pecora nera sulla mappa.
 * Dato che estende ButtonWithBackground è possibile associargli una immagine di sfondo.
 */
public class BottoneElementi extends ButtonWithBackground {
	
	private static final long serialVersionUID = 5892975573082743986L;

	/**
	 * Id della regione corrispondente
	 */
	private String idRegione; 
	
	/**
	 * coordinata x delle regione corrispondente
	 */
	private int coordinataX; 
	
	/**
	 * coordinata y della regine corrispondente
	 */
	private int coordinataY; 
	
	/**
	 * Costruttore
	 * @param img: immagine di sfondo che dovrà avere
	 * @param idRegione: regione alla quale è riferito
	 */
	public BottoneElementi(Image img, String idRegione) {
		super(img); 
		this.idRegione = idRegione; 
	}
	
	public String getIdRegione() {
		return this.idRegione;
	}
	
	public void setCoordinataX(int coordinataX) {
		this.coordinataX = coordinataX; 
	}
	
	public int getCoordinataX() {
		return this.coordinataX; 
	}
	
	public void setCoordinataY(int coordinataY) { 
		this.coordinataY = coordinataY;
	}
	
	public int getCoordinataY() {
		return this.coordinataY; 
	}

}
