package view;

import javax.swing.JLabel;

/**
 * Classe che permette di associare ad una label i dati di una regione.
 */
public class LabelAnimali extends JLabel {
	
	private static final long serialVersionUID = -7345283963327702037L;
	
	/**
	 * Id della regione a cui si riferisce
	 */
	private String idRegione;
	
	/**
	 * Costruttore
	 * @param testo: testo da rappresentare
	 * @param idRegione: id della regione a cui si riferisce
	 */
	public LabelAnimali(String testo, String idRegione) {
		super(testo);
		this.idRegione = idRegione; 
	}
	
	public String getIdRegione() {
		return this.idRegione;
	}
}
