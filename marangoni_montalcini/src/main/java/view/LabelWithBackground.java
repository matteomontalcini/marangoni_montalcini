package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JLabel;

/**
 * Classe che permette di avere una immagine come sfondo
 */
public class LabelWithBackground extends JLabel {

	private static final long serialVersionUID = 6166321171181567720L;
	
	/**
	 * Immagine che andrà messa come sfondo
	 */
	private Image imgSfondo;

	public LabelWithBackground(Image imgSfondo) {
		super();
		this.imgSfondo = imgSfondo;
		}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imgSfondo, 0, 0, this);
		}
}
