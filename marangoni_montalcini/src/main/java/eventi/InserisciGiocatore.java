package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method inserisciGiocatore sulla view. 
 */
public class InserisciGiocatore extends EventoGenerico {
	
	private static final long serialVersionUID = -8227817791320032107L;
	/**
	 * indice del giocatore da inserire
	 */
	private int indice; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public InserisciGiocatore() {
		super(TipoEvento.INSERISCI_GIOCATORE); 
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}
	
	

}
