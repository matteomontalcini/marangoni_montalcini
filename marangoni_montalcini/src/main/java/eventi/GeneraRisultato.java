package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method generaRisultato sulla view. 
 */
public class GeneraRisultato extends EventoGenerico {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5813071793239619807L;

	/**
	 * contiene i punteggi realizzati dai vari giocatori
	 */
	private List<Integer> punteggiGiocatori; 
	
	/**
	 * è il punteggio massimo realizzato tra tutti i giocatori
	 */
	private int punteggioMassimo; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public GeneraRisultato () {
		super(TipoEvento.GENERA_RISULTATO); 
	}

	public List<Integer> getPunteggiGiocatori() {
		return punteggiGiocatori;
	}

	public void setPunteggiGiocatori(List<Integer> punteggiGiocatori) {
		this.punteggiGiocatori = punteggiGiocatori;
	}

	public int getPunteggioMassimo() {
		return punteggioMassimo;
	}

	public void setPunteggioMassimo(int punteggioMassimo) {
		this.punteggioMassimo = punteggioMassimo;
	}

	
}
