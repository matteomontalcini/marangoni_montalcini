package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method visibilityBottoniAnimali sulla view. 
 */
public class VisibilityBottoniAnimali extends EventoGenerico {
	
	private static final long serialVersionUID = -2847334587524511578L;
	/**
	 * regione che si sta considerando
	 */
	private String idRegione;
	/**
	 * specifica se deve essere settata a true o a false la visibilità 
	 */
	private boolean condizione;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public VisibilityBottoniAnimali() {
		super(TipoEvento.VISIBILITY_BOTTONI_ANIMALI); 
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}

	public boolean isCondizione() {
		return condizione;
	}

	public void setCondizione(boolean condizione) {
		this.condizione = condizione;
	}
	

}
