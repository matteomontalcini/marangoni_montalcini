package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method attivaBottoniStradeSelezionabili sulla view. 
 */
public class AttivaBottoniStradeSelezionabili extends EventoGenerico {

	private static final long serialVersionUID = -4928565009144479889L;
	/**
	 * Contiene gli id delle strade che possono essere selezionate
	 */
	private List<String> idStradeSelezionabili; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AttivaBottoniStradeSelezionabili () {
		super(TipoEvento.ATTIVA_BOTTONI_STRADE_SELEZIONABILI); 
	}
	
	public List<String> getIdStradeSelezionabili() {
		return idStradeSelezionabili;
	}

	public void setIdStradeSelezionabili(List<String> idStradeSelezionabili) {
		this.idStradeSelezionabili = idStradeSelezionabili;
	}

}
