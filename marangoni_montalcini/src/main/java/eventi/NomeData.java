package eventi;

import java.util.Date;


/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando un client si connette al server e quindi 
 * deve comunicare ad esso il suo nome e la data in cui ha accarezzato per l'ultima volta una pecora.
 */
public class NomeData extends EventoGenerico {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4751818057150344911L;
	/**
	 * contiene il nome del giocatore che si è appena connesso
	 */
	private String nomeGiocatore; 
	
	/**
	 * contiene la data in cui il giocatore ha accarezzato per l'ultima volta una pecora
	 */
	private Date dataUltimaCarezza; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public NomeData () {
		super(TipoEvento.NOME_DATA); 
	}

	public String getNomeGiocatore() {
		return nomeGiocatore;
	}

	public void setNomeGiocatore(String nomeGiocatore) {
		this.nomeGiocatore = nomeGiocatore;
	}

	public Date getDataUltimaCarezza() {
		return dataUltimaCarezza;
	}

	public void setDataUltimaCarezza(Date dataUltimaCarezza) {
		this.dataUltimaCarezza = dataUltimaCarezza;
	}
	
	

}
