package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando viene selezionata, dalla schermata di gioco, 
 * una tessera terreno per effettuarne l'acquisto. 
 */
public class TesseraTerrenoSelezionata extends EventoGenerico { 
	
	private static final long serialVersionUID = -4392277748607768161L;
	/**
	 * integer corrispondente al particolare tipo di terreno che è stato selezionato
	 */
	private Integer tipoDiTerreno; 
	

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public TesseraTerrenoSelezionata() {
		super(TipoEvento.TESSERA_TERRENO_SELEZIONATA); 
	}
	
	public void setTipoDiTerreno(Integer tipoDiTerreno) {
		this.tipoDiTerreno = tipoDiTerreno; 
	}
	
	public Integer getTipoDiTerreno() {
		return this.tipoDiTerreno; 
	}

	

}
