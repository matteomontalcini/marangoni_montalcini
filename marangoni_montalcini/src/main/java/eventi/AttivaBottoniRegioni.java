package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method attivaBottoniRegioni sulla view. 
 */
public class AttivaBottoniRegioni extends EventoGenerico {
	
	private static final long serialVersionUID = 5152380145867880518L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AttivaBottoniRegioni() {
		super(TipoEvento.ATTIVA_BOTTONI_REGIONI); 
	}
	

}
