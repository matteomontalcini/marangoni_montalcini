package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaBottoniMosse sulla view. 
 */
public class DisattivaBottoniMosse extends EventoGenerico {
	
	private static final long serialVersionUID = -8622719703054135879L;
	/**
	 * Questo intero rappresenta la corrispondente mossa da non disattivare: le altre invece vanno tutte disattivate
	 */
	private int numeroMossaDaNonDisattivare; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaBottoniMosse() {
		super(TipoEvento.DISATTIVA_BOTTONI_MOSSE); 
	}

	public int getNumeroMossaDaNonDisattivare() {
		return numeroMossaDaNonDisattivare;
	}

	public void setNumeroMossaDaNonDisattivare(int numeroMossaDaNonDisattivare) {
		this.numeroMossaDaNonDisattivare = numeroMossaDaNonDisattivare;
	}
	
	

}
