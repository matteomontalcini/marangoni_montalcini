package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method avvisoMosseNonDisponibili sulla view. 
 */
public class AvvisoMosseNonDisponibili extends EventoGenerico {
	
	private static final long serialVersionUID = -650057081971850617L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AvvisoMosseNonDisponibili () {
		super(TipoEvento.AVVISO_MOSSE_NON_DISPONIBILI); 
	}
	
	

}
