package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setTotaleArietiInRegione sulla view. 
 */
public class SetTotaleArietiInRegione extends EventoGenerico {
	
	private static final long serialVersionUID = 7037405257325390161L;
	/**
	 * id della regione in cui bisogna settare il numero di arieti
	 */
	private String idRegione; 
	/**
	 * totale arieti nella regione
	 */
	private int totaleArieti; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetTotaleArietiInRegione() {
		super(TipoEvento.SET_TOTALE_ARIETI_IN_REGIONE); 
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}

	public int getTotaleArieti() {
		return totaleArieti;
	}

	public void setTotaleArieti(int totaleArieti) {
		this.totaleArieti = totaleArieti;
	}
	
	

}
