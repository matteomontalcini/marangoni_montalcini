package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setPosizionePecoraNera sulla view. 
 */
public class SetPosizionePecoraNera extends EventoGenerico {
	
	private static final long serialVersionUID = -490376064019468404L;
	/**
	 * id della regione da cui si è spostata la pecora nera
	 */
	private String idRegionePartenza;  
	/**
	 * id della regione in cui è arrivata la pecora nera
	 */
	private String idRegioneArrivo;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetPosizionePecoraNera() {
		super(TipoEvento.SET_POSIZIONE_PECORA_NERA); 
	}

	public String getIdRegionePartenza() {
		return idRegionePartenza;
	}

	public void setIdRegionePartenza(String idRegionePartenza) {
		this.idRegionePartenza = idRegionePartenza;
	}

	public String getIdRegioneArrivo() {
		return idRegioneArrivo;
	}

	public void setIdRegioneArrivo(String idRegioneArrivo) {
		this.idRegioneArrivo = idRegioneArrivo;
	}
	

}
