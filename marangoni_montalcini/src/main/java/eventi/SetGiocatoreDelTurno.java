package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setGiocatoreDelTurno sulla view. 
 */
public class SetGiocatoreDelTurno extends EventoGenerico {
	
	private static final long serialVersionUID = 8091767046291969969L;
	/**
	 * nome del giocatore che sta svolgendo il proprio turno
	 */
	private String nomeGiocatore; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetGiocatoreDelTurno() {
		super(TipoEvento.SET_GIOCATORE_DEL_TURNO); 
	}

	public String getNomeGiocatore() {
		return nomeGiocatore;
	}

	public void setNomeGiocatore(String nomeGiocatore) {
		this.nomeGiocatore = nomeGiocatore;
	}
	
	

}
