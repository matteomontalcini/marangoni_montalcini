package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method attivaAnimaliSelezionabili sulla view. 
 */
public class AttivaAnimaliSelezionabili extends EventoGenerico {
	
	private static final long serialVersionUID = -5795643268174816719L;
	
	/**
	 * Questa lista contiene gli integer corrispondenti agli animali che possono essere selezionati
	 */
	private List<Integer> animaliSelezionabili; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AttivaAnimaliSelezionabili () {
		super(TipoEvento.ATTIVA_ANIMALI_SELEZIONABILI); 
	}

	public List<Integer> getAnimaliSelezionabili() {
		return animaliSelezionabili;
	}

	public void setAnimaliSelezionabili(List<Integer> animaliSelezionabili) {
		this.animaliSelezionabili = animaliSelezionabili;
	}
	
	

}
