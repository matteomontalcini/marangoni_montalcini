package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaBottoniTessereTerreno sulla view. 
 */
public class DisattivaBottoniTessereTerreno extends EventoGenerico {
	
	private static final long serialVersionUID = 3243000426459284026L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaBottoniTessereTerreno () {
		super(TipoEvento.DISATTIVA_BOTTONI_TESSERE_TERRENO); 
	}

}
