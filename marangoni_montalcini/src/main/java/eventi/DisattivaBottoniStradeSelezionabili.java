package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaBottoniStradeSelezionabili sulla view. 
 */
public class DisattivaBottoniStradeSelezionabili extends EventoGenerico {
	
	private static final long serialVersionUID = 1310206095495029050L;
	/**
	 * Contiene gli id di tutte le strade selezionabili
	 */
	private List<String> idStradeSelezionabili; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaBottoniStradeSelezionabili () {
		super(TipoEvento.DISATTIVA_BOTTONI_STRADE_SELEZIONABILI); 
	}

	public List<String> getIdStradeSelezionabili() {
		return idStradeSelezionabili;
	}

	public void setIdStradeSelezionabili(List<String> idStradeSelezionabili) {
		this.idStradeSelezionabili = idStradeSelezionabili;
	}


}
