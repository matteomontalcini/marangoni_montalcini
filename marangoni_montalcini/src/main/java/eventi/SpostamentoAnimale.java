package eventi;


/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method spostamentoAnimale sulla view. 
 */
public class SpostamentoAnimale extends EventoGenerico {
	
	private static final long serialVersionUID = -6169448765447449108L;
	/**
	 * id della regione da cui è stato spostato l'animale
	 */
	private String idRegionePartenza; 
	/**
	 * id della regione in cui è stato spostato l'animale
	 */
	private String idRegioneArrivo; 
	/**
	 * tipo di animale che è stato spostato
	 */
	private String animale; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SpostamentoAnimale() {
		super(TipoEvento.SPOSTAMENTO_ANIMALE); 
	}

	public String getIdRegionePartenza() {
		return idRegionePartenza;
	}

	public void setIdRegionePartenza(String idRegionePartenza) {
		this.idRegionePartenza = idRegionePartenza;
	}

	public String getIdRegioneArrivo() {
		return idRegioneArrivo;
	}

	public void setIdRegioneArrivo(String idRegioneArrivo) {
		this.idRegioneArrivo = idRegioneArrivo;
	}

	public String getAnimale() {
		return animale;
	}

	public void setAnimale(String animale) {
		this.animale = animale;
	}
	
	

}
