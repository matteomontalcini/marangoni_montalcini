package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setIconaGiocatoreInStrada sulla view. 
 */
public class SetIconaGiocatoreInStrada extends EventoGenerico {
	
	private static final long serialVersionUID = -280526468156785652L;
	/**
	 * id della strada in cui posizionare la pedina
	 */
	private String idStradaArrivo;
	/**
	 * coordinata X della strada di partenza
	 */
	private int coordinataXPartenza; 
	/**
	 * coordinata Y della strada di partenza
	 */
	private int coordinataYPartenza; 
	/**
	 * coordinata X della strada di arrivo
	 */
	private int coordinataXArrivo; 
	/**
	 * coordinata Y della strada di arrivo
	 */
	private int coordinataYArrivo; 
	/**
	 * indice del giocatore che sta effettuando il movimento (serve per associargli la giusta pedina)
	 */
	private int posizioneGiocatoreInArray; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetIconaGiocatoreInStrada () {
		super(TipoEvento.SET_ICONA_GIOCATORE_IN_STRADA);
	}

	public String getIdStradaArrivo() {
		return idStradaArrivo;
	}

	public void setIdStradaArrivo(String idStradaArrivo) {
		this.idStradaArrivo = idStradaArrivo;
	}

	public int getCoordinataXPartenza() {
		return coordinataXPartenza;
	}

	public void setCoordinataXPartenza(int coordinataXPartenza) {
		this.coordinataXPartenza = coordinataXPartenza;
	}

	public int getCoordinataYPartenza() {
		return coordinataYPartenza;
	}

	public void setCoordinataYPartenza(int coordinataYPartenza) {
		this.coordinataYPartenza = coordinataYPartenza;
	}

	public int getCoordinataXArrivo() {
		return coordinataXArrivo;
	}

	public void setCoordinataXArrivo(int coordinataXArrivo) {
		this.coordinataXArrivo = coordinataXArrivo;
	}

	public int getCoordinataYArrivo() {
		return coordinataYArrivo;
	}

	public void setCoordinataYArrivo(int coordinataYArrivo) {
		this.coordinataYArrivo = coordinataYArrivo;
	}

	public int getPosizioneGiocatoreInArray() {
		return posizioneGiocatoreInArray;
	}

	public void setPosizioneGiocatoreInArray(int posizioneGiocatoreInArray) {
		this.posizioneGiocatoreInArray = posizioneGiocatoreInArray;
	}
	
	
}
