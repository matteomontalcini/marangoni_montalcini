package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method attivaBottoniMosseSelezionabili sulla view. 
 */
public class AttivaBottoniMosseSelezionabili extends EventoGenerico {
	
	private static final long serialVersionUID = 4374019947132062517L;
	/**
	 * Contiene gli integer corrispondenti alle mosse che possono essere selezionate
	 */
	private List<Integer> mosseSelezionabili; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AttivaBottoniMosseSelezionabili() {
		super(TipoEvento.ATTIVA_BOTTONI_MOSSE_SELEZIONABILI); 
	}

	public List<Integer> getMosseSelezionabili() {
		return mosseSelezionabili;
	}

	public void setMosseSelezionabili(List<Integer> mosseSelezionabili) {
		this.mosseSelezionabili = mosseSelezionabili;
	}
	

}
