package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setTessereInPossesso sulla view. 
 */
public class SetTessereInPossesso extends EventoGenerico {
	
	private static final long serialVersionUID = -7867351927563488774L;
	/**
	 * tipo di terreno corrispondente alla tessera di cui bisogna incrementare il costo
	 */
	private List<Integer> tessereInPossesso; 
	/**
	 * indice del giocatore di cui bisogna visualizzare le tessere in possesso
	 */
	private int indiceGiocatore;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetTessereInPossesso() {
		super(TipoEvento.SET_TESSERE_IN_POSSESSO); 
	}

	public List<Integer> getTessereInPossesso() {
		return tessereInPossesso;
	}

	public void setTessereInPossesso(List<Integer> tessereInPossesso) {
		this.tessereInPossesso = tessereInPossesso;
	}

	public int getIndiceGiocatore() {
		return indiceGiocatore;
	}

	public void setIndiceGiocatore(int indiceGiocatore) {
		this.indiceGiocatore = indiceGiocatore;
	}
	
	
	

}
