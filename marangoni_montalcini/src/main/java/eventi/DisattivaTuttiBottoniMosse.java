package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaBottoniMosse() sulla view. 
 */
public class DisattivaTuttiBottoniMosse extends EventoGenerico {
	
	private static final long serialVersionUID = 4189804370015356301L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaTuttiBottoniMosse() {
		super(TipoEvento.DISATTIVA_TUTTI_BOTTONI_MOSSE); 
	}
	

}
