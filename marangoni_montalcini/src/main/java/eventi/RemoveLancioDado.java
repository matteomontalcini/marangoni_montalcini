package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method removeLancioDado sulla view. 
 */
public class RemoveLancioDado extends EventoGenerico {
	
	private static final long serialVersionUID = 6509869608230368914L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public RemoveLancioDado() {
		super(TipoEvento.REMOVE_LANCIO_DADO); 
	}

}
