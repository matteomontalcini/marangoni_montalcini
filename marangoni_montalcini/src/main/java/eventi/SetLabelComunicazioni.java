package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setLabelComunicazioni sulla view. 
 */
public class SetLabelComunicazioni extends EventoGenerico {
	
	private static final long serialVersionUID = 1688447930494937375L;
	/**
	 * stringa da visualizzare a video
	 */
	private String string; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetLabelComunicazioni () {
		super(TipoEvento.SET_LABEL_COMUNICAZIONI); 
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	
	
	

}
