package eventi;


/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando viene selezionata, dalla schermata di gioco, 
 * una strada. 
 */
public class StradaSelezionata extends EventoGenerico {
	
	private static final long serialVersionUID = 4127621720233574677L;
	/**
	 * id della strada che è stata selezionata
	 */
	private String idStrada; 
	
	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public StradaSelezionata() {
		super(TipoEvento.STRADA_SELEZIONATA); 
	}
	
	public void setIdStrada(String idStrada) {
		this.idStrada = idStrada; 
	}
	
	public String getIdStrada() {
		return this.idStrada; 
	}

}
