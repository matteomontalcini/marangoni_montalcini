package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene generato dall'ImplementazioneServerSocket quando i giocatori
 * sono tutti connessi e quindi è necessario avviare la partita. 
 */
public class AvvioSchermataPrincipale extends EventoGenerico {
	
	private static final long serialVersionUID = -3188824852035883778L;
	/**
	 * contiene i nomi dei giocatori
	 */
	private List<String> nomiGiocatori; 
	/**
	 * E' il numero di giocatori della partita
	 */
	private int numeroGiocatori; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AvvioSchermataPrincipale() {
		super(TipoEvento.AVVIO_SCHERMATA_PRINCIPALE); 
	}

	public List<String> getNomiGiocatori() {
		return nomiGiocatori;
	}

	public void setNomiGiocatori(List<String> nomiGiocatori) {
		this.nomiGiocatori = nomiGiocatori;
	}

	public int getNumeroGiocatori() {
		return numeroGiocatori;
	}

	public void setNumeroGiocatori(int numeroGiocatori) {
		this.numeroGiocatori = numeroGiocatori;
	}
	
	

}
