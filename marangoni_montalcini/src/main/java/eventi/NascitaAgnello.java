package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il nascitaAgnello sulla view. 
 */
public class NascitaAgnello extends EventoGenerico {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4196481171333486722L;
	/**
	 * id della regione in cui è nato un agnello
	 */
	private String idRegione; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public NascitaAgnello() {
		super(TipoEvento.NASCITA_AGNELLO); 
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}
	
	

}
