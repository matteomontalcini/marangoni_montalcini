package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando viene selezionata, dalla schermata di gioco, 
 * una delle mosse disponibili. 
 */
public class MossaSelezionata extends EventoGenerico {
	
	private static final long serialVersionUID = 2671331468565774942L;
	
	/**
	 * contiene il tipo di mossa selezionata
	 */
	private String mossaSelezionata; 
	
	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public MossaSelezionata() {
		super(TipoEvento.MOSSA_SELEZIONATA); 
	}
	
	public void setMossaSelezionata(String mossaSelezionata) {
		this.mossaSelezionata = mossaSelezionata; 
	}
	
	public String getMossaSelezionata() {
		return this.mossaSelezionata; 
	}
	

}
