package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaPannelloMosseNonDisponibili sulla view. 
 */
public class DisattivaPannelloMosseNonDisponibili extends EventoGenerico {
	
	private static final long serialVersionUID = 543731790558929601L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaPannelloMosseNonDisponibili() {
		super(TipoEvento.DISATTIVA_PANNELLO_MOSSE_NON_DISPONIBILI); 
	}

}
