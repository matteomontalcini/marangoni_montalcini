package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando un giocatore chiude la sua schermata: di 
 * conseguenza, non essendoci più tutti i giocatori, non è possibile continuare la partita e quindi devono 
 * essere chiuse le schermate di tutti: si comunica il tutto tramite questo evento
 */
public class ChiusuraFrameGiocatore extends EventoGenerico {
	
	private static final long serialVersionUID = -9094560109020398446L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public ChiusuraFrameGiocatore() {
		super(TipoEvento.CHIUSURA_FRAME_GIOCATORE); 
	}

}
