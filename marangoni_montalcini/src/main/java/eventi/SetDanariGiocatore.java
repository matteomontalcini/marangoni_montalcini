package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setDanariGiocatore sulla view. 
 */
public class SetDanariGiocatore extends EventoGenerico {
	
	private static final long serialVersionUID = -1380478693577664542L;
	/**
	 * indice del giocatore di cui bisogna modificare i danari in possesso
	 */
	private int indice; 
	
	/**
	 * danari che il giocatore possiede
	 */
	private int danari; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetDanariGiocatore() {
		super(TipoEvento.SET_DANARI_GIOCATORE); 
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getDanari() {
		return danari;
	}

	public void setDanari(int danari) {
		this.danari = danari;
	}
	
	

}
