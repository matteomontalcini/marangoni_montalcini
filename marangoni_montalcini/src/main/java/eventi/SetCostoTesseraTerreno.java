package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setCostoTesseraTerreno sulla view. 
 */
public class SetCostoTesseraTerreno extends EventoGenerico {
	
	private static final long serialVersionUID = 5137878699501294129L;
	/**
	 * tipo di terreno corrispondente alla tessera di cui bisogna incrementare il costo
	 */
	private int tipoTerreno; 
	
	/**
	 * nuovo costo della tessera
	 */
	private int nuovoCosto; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetCostoTesseraTerreno() {
		super(TipoEvento.SET_COSTO_TESSERA_TERRENO); 
	}

	public int getTipoTerreno() {
		return tipoTerreno;
	}


	public void setTipoTerreno(int tipoTerreno) {
		this.tipoTerreno = tipoTerreno;
	}


	public int getNuovoCosto() {
		return nuovoCosto;
	}

	public void setNuovoCosto(int nuovoCosto) {
		this.nuovoCosto = nuovoCosto;
	}


}
