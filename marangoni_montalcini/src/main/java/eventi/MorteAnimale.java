package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method morteAnimale sulla view. 
 */
public class MorteAnimale extends EventoGenerico {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6212808902770001060L;
	/**
	 * contiene l'informazione relativa al tipo di animale che è stato ucciso
	 */
	private String tipoAnimale; 
	/**
	 * id della regione in cui l'animale è stato ucciso
	 */
	private String idRegione; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public MorteAnimale () {
		super(TipoEvento.MORTE_ANIMALE); 
	}

	public String getTipoAnimale() {
		return tipoAnimale;
	}

	public void setTipoAnimale(String tipoAnimale) {
		this.tipoAnimale = tipoAnimale;
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}
	
	

}
