package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaBottoniRegioni sulla view. 
 */
public class DisattivaBottoniRegioni extends EventoGenerico {
	
	private static final long serialVersionUID = -5723515459081774899L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaBottoniRegioni()  {
		super(TipoEvento.DISATTIVA_BOTTONI_REGIONI); 
	}

}
