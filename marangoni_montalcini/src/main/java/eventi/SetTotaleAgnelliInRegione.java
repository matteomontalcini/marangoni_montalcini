package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setTotaleAgnelliInRegione sulla view. 
 */
public class SetTotaleAgnelliInRegione extends EventoGenerico {
	
	private static final long serialVersionUID = 4112847074777497758L;
	/**
	 * id della regione in cui bisogna settare il numero di agnelli
	 */
	private String idRegione; 
	/**
	 * totale agnelli presenti nella regione
	 */
	private int totaleAgnelli; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetTotaleAgnelliInRegione() {
		super(TipoEvento.SET_TOTALE_AGNELLI_IN_REGIONE); 
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}

	public int getTotaleAgnelli() {
		return totaleAgnelli;
	}

	public void setTotaleAgnelli(int totaleAgnelli) {
		this.totaleAgnelli = totaleAgnelli;
	}
	
	

}
