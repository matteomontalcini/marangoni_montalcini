package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito quando viene selezionata, dalla mappa, una 
 * regione. 
 */
public class RegioneSelezionata extends EventoGenerico{ 
	
	private static final long serialVersionUID = -2059022431931533870L;
	
	/**
	 * Id della regione selezionata
	 */
	private String idRegione; 
	

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public RegioneSelezionata() {
		super(TipoEvento.REGIONE_SELEZIONATA);	
	}
	
	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione; 
	}
	
	public String getIdRegione() {
		return this.idRegione; 
	}

}
