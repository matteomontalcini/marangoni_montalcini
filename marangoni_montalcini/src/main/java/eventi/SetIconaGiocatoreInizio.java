package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setIconaGiocatoreInizio sulla view. 
 */
public class SetIconaGiocatoreInizio extends EventoGenerico {
	
	private static final long serialVersionUID = -241488729593225957L;
	/**
	 * id della strada in cui posizionare la pedina
	 */
	private String idStradaArrivo; 
	/**
	 * indice del giocatore di cui va posizionata la pedina (fondamentale per scegliere il colore giusto)
	 */
	private int posizioneGiocatoreInArray; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetIconaGiocatoreInizio () {
		super(TipoEvento.SET_ICONA_GIOCATORE_INIZIO); 
	}

	public String getIdStradaArrivo() {
		return idStradaArrivo;
	}

	public void setIdStradaArrivo(String idStradaArrivo) {
		this.idStradaArrivo = idStradaArrivo;
	}

	public int getPosizioneGiocatoreInArray() {
		return posizioneGiocatoreInArray;
	}

	public void setPosizioneGiocatoreInArray(int posizioneGiocatoreInArray) {
		this.posizioneGiocatoreInArray = posizioneGiocatoreInArray;
	}
	
	

}
