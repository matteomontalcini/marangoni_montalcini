package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito quando viene selezionato un animale da un panel
 * che mostra le scelte possibili
 */
public class AnimaleSelezionato extends EventoGenerico {
	
	private static final long serialVersionUID = 6219346280848771243L;
	
	/**
	 * Contiene l'informazione relativa all'animale selezionato
	 */
	private String animaleSelezionato; 
	
	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AnimaleSelezionato() {
		super(TipoEvento.ANIMALE_SELEZIONATO); 
	}

	public void setAnimaleSelezionato(String animaleScelto) {
		this.animaleSelezionato = animaleScelto; 
	}
	
	public String getAnimaleSelezionato() {
		return this.animaleSelezionato; 
	}

	
}
