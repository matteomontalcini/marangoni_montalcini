package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method eseguiLancioDado sulla view. 
 */
public class EseguiLancioDado extends EventoGenerico {
	
	private static final long serialVersionUID = 8145952273767955170L;
	/**
	 * contiene il valore del risultato del lancio del dado
	 */
	private int valoreLancioDado; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public EseguiLancioDado() {
		super(TipoEvento.ESEGUI_LANCIO_DADO); 
	}

	public int getValoreLancioDado() {
		return valoreLancioDado;
	}

	public void setValoreLancioDado(int valoreLancioDado) {
		this.valoreLancioDado = valoreLancioDado;
	}

	
}
