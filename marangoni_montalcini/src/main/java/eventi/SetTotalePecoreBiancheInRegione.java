package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setTotalePecoreBiancheInRegione sulla view. 
 */
public class SetTotalePecoreBiancheInRegione extends EventoGenerico {
	
	private static final long serialVersionUID = 4939661772802655445L;
	/**
	 * id della regione in cui bisogna settare il numero di pecore bianche
	 */
	private String idRegione; 
	
	/**
	 * totale pecore bianche nella regione
	 */
	private int totalePecoreBianche; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetTotalePecoreBiancheInRegione() {
		super(TipoEvento.SET_TOTALE_PECORE_BIANCHE_IN_REGIONE); 
	}

	public String getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(String idRegione) {
		this.idRegione = idRegione;
	}

	public int getTotalePecoreBianche() {
		return totalePecoreBianche;
	}

	public void setTotalePecoreBianche(int totalePecoreBianche) {
		this.totalePecoreBianche = totalePecoreBianche;
	}
	
	

}
