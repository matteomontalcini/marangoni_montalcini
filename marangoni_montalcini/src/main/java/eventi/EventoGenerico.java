package eventi;

import java.io.Serializable;

/**
 * Questo evento è alla base di tutti gli altri eventi, che lo estendono. E' serializzabile e ha come attributi solo
 * il tipo di evento (che fa riferimento a una enumerazione di tutti i possibili tipi di evento) e l'indice del giocatore
 * che ha prodotto l'evento (se è un evento della view, altrimenti non viene utilizzato questo campo). 
 * E' fondamentale per lo scambio di messaggi in rete.
 */
public abstract class EventoGenerico implements Serializable {
	
	private static final long serialVersionUID = 3818549962675663269L;
	
	private TipoEvento tipo; 
	
	private int indiceGiocatore = 0; 

	/**
	 * costruttore: crea l'EventoGenerico, passandogli come parametro il particolare tipo di evento che si deve costruire. 
	 */
	public EventoGenerico(TipoEvento tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * @return è il particolare tipo di evento
	 */
	public TipoEvento getTipo() {
		return this.tipo; 
	}
	
	/**
	 * @return è l'indice del giocatore che ha generato l'evento: si usa solo nel caso in cui sia un evento della view
	 * (quindi generato tramite un click sulla schermata di gioco) e solo per particolari eventi del controller
	 */
	public int getIndiceGiocatore() {
		return indiceGiocatore;
	}
	/**
	 * @param indiceGiocatore è l'indice del giocatore che ha generato l'evento o, eventualmente (per particolari eventi
	 * del controller), l'indice del giocatore che deve gestire l'evento
	 */
	public void setIndiceGiocatore(int indiceGiocatore) {
		this.indiceGiocatore = indiceGiocatore;
	}
	
	
}
