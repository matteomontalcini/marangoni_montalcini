package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene generato quando viene premuto il tasto di conferma dal 
 * giocatore quando egli ha appurato che non ha più mosse disponibili e quindi il suo turno è concluso
 */
public class ConfermaRicezioneAvvisoNoMosse extends EventoGenerico {
	
	private static final long serialVersionUID = -91530030894567453L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public ConfermaRicezioneAvvisoNoMosse() {
		super(TipoEvento.CONFERMA_RICEZIONE_AVVISO_NO_MOSSE); 
	}

}
