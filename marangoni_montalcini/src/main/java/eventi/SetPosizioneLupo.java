package eventi;


/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method setPosizioneLupo sulla view. 
 */
public class SetPosizioneLupo extends EventoGenerico {
	
	private static final long serialVersionUID = 8009063032912056959L;
	/**
	 * id della regione da cui si è spostato il lupo
	 */
	private String idRegionePartenza; 
	/**
	 * id della regione in cui è arrrivato il lupo
	 */
	private String idRegioneArrivo; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public SetPosizioneLupo() {
		super(TipoEvento.SET_POSIZIONE_LUPO); 
	}

	public String getIdRegionePartenza() {
		return idRegionePartenza;
	}

	public void setIdRegionePartenza(String idRegionePartenza) {
		this.idRegionePartenza = idRegionePartenza;
	}

	public String getIdRegioneArrivo() {
		return idRegioneArrivo;
	}

	public void setIdRegioneArrivo(String idRegioneArrivo) {
		this.idRegioneArrivo = idRegioneArrivo;
	}
	
	

}
