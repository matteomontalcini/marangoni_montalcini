package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method posizionaCancello sulla view. 
 */
public class PosizionaCancello extends EventoGenerico {
	
	private static final long serialVersionUID = 1018736842329700006L;
	/**
	 * è l'id della strada in cui va posizionato il cancello
	 */
	private String idStradaPartenza; 
	
	/**
	 * permette di sapere se si è nella fase finale della partita o meno: è fondamentale nella scelta di quale
	 * recinto posizionare nella strada sopra indicata
	 */
	private boolean faseFinale; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public PosizionaCancello() {
		super(TipoEvento.POSIZIONA_CANCELLO); 
	}

	public String getIdStradaPartenza() {
		return idStradaPartenza;
	}

	public void setIdStradaPartenza(String idStradaPartenza) {
		this.idStradaPartenza = idStradaPartenza;
	}

	public boolean isFaseFinale() {
		return faseFinale;
	}

	public void setFaseFinale(boolean faseFinale) {
		this.faseFinale = faseFinale;
	}
	
	

}
