package eventi;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method disattivaPannelloSelezioneAnimali sulla view. 
 */
public class DisattivaPannelloSelezioneAnimali extends EventoGenerico {
	
	private static final long serialVersionUID = -1877386653837668494L;

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public DisattivaPannelloSelezioneAnimali() {
		super(TipoEvento.DISATTIVA_PANNELLO_SELEZIONE_ANIMALI); 
	}

}
