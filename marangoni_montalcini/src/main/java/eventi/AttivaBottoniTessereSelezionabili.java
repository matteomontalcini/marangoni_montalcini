package eventi;

import java.util.List;

/**
 * Questo evento estende la classe EventoGenerico. Viene costruito dalle implementazioni della view presenti per la 
 * rete quando viene chiamato dal controller il method attivaBottoniTessereSelezionabili sulla view. 
 */
public class AttivaBottoniTessereSelezionabili extends EventoGenerico {

	private static final long serialVersionUID = 9094830291210213323L;
	/**
	 * Contiene gli integer corrispondneti alle tessere che possono essere selezionate
	 */
	private List<Integer> numeriTessereCorrispondenti; 

	/**
	 * costruttore: crea il nuovo evento, chiamando il costruttore di EventoGenerico e passandogli come parametro
	 * il particolare tipo di evento che si deve costruire. 
	 */
	public AttivaBottoniTessereSelezionabili () {
		super(TipoEvento.ATTIVA_BOTTONI_TESSERE_SELEZIONABILI); 
	}

	public List<Integer> getNumeriTessereCorrispondenti() {
		return numeriTessereCorrispondenti;
	}

	public void setNumeriTessereCorrispondenti(List<Integer> numeriTessereCorrispondenti) {
		this.numeriTessereCorrispondenti = numeriTessereCorrispondenti;
	}
	
	

}
