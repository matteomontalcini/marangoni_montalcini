package it.polimi.marangoni_montalcini;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.StatoPartita;
import model.Turno;
import view.SchermataIniziale;
import view.SchermataPrincipale;
import controller.ControllerPartita;

/**
 * Classe contenente il main
 */
public class Gioco {

	private StatoPartita statoPartita; 

	private SchermataPrincipale schermataPrincipale;

	private ControllerPartita controllerPartita; 

	private SchermataIniziale schermataIniziale; 

	private static final Logger LOGGER = Logger.getLogger("it.polimimarangoni_montalcini"); 

	/**
	 * Method che fa partire la schermata iniziale di inserimento dati.
	 */
	public Gioco(){
		schermataIniziale = new SchermataIniziale(this); 
	}

	/**
	 * Method usato per inizializzare il gioco locale
	 */
	public void giocoLocale() {
		//si prende il numero di giocatori dalla schermata iniziale
		int numeroGiocatori = schermataIniziale.getNomiGiocatori().size();
		//si settano nomi e date dei giocatori
		List<String> nomiGiocatori = schermataIniziale.getNomiGiocatori();
		List<Date> dateCarezze = schermataIniziale.getDate();
		//si istanzia lo stato della partita passandogli il numero dei giocatori e le date
		statoPartita = new StatoPartita(numeroGiocatori, dateCarezze); 
		//si crea una nuova schermata di gioco
		schermataPrincipale = new SchermataPrincipale(statoPartita.getNumeroGiocatori(), nomiGiocatori);
		//si aggiungono i nomi al model
		statoPartita.aggiungiGiocatori(nomiGiocatori);
		//si inizializzano i costi delle tessere terreno
		statoPartita.inizializzaCostiTessereTerreno();
		//creo il controller partita
		controllerPartita = new ControllerPartita(statoPartita, schermataPrincipale);
		//Viene fatto per la seconda volta il controllo di correttezza dei nomi e delle date inserite (la prima volta lo si fa dalla schermata iniziale quando si inseriscono i dati)
		if(!controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			//i dati inseriti non sono corretti, non si avvia il gioco
			LOGGER.log(Level.SEVERE,  "Impossibile avviare il gioco: inserimenti nomi/date errati", new IllegalArgumentException());
		} else if(controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			//i dati inseriti sono corretti
			//stabilisco quale sarà il primo giocatore
			controllerPartita.stabilisciPrimoGiocatore();
			//istanziamo un nuovo turno
			statoPartita.setTurno(new Turno(statoPartita.getGiocatori().get(0)));
			//rendiamo il controller partita osservatore nei confronti della schermata di gioco
			schermataPrincipale.addObserver(controllerPartita);
			schermataPrincipale.run();
			//distribuiamo le terrere terreno iniziali, le si visualizzano e si comincia con la scelta delle strade di partenza
			controllerPartita.distribuisciTessereIniziali();
			controllerPartita.visualizzaTessereInPossesso();
			controllerPartita.getControllerTurno().scegliStradePartenza();
		}
	} 

	public static void main( String[] args ) { 
		new Gioco(); 
	}

}