package rete;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.InterfacciaView;
import eventi.*;

public class ImplementazioneServerSocket extends Observable implements InterfacciaView  {
	
	/**
	 * è l'hashMap che contiene le corrispondenza tra l'indice del giocatore (integer) è la sua socket
	 */
	Map<Integer, Socket> clientsSocket;
	
	private static final Logger LOGGER = Logger.getLogger("implementazioneClientSocket"); 
	
	/**
	 * costruttore: genera una nuova implementazioneServerSocket. Ha una classe interna RicettoreEventi, che effettua l'attesa dei
	 * messaggi dei vari clientSocket. Per ogni socket dei client viene creato un nuovo thread di attesa di messaggi. 
	 * @param clientsSocket: è l'hashMap che contiene le corrispondenza tra l'indice del giocatore (integer) è la sua socket
	 * @param dateCarezze: contiene tutte le date in cui i giocatori hanno accarezzato per l'ultima volta una pecora. 
	 */
	public ImplementazioneServerSocket(Map<Integer, Socket> clientsSocket) {
		this.clientsSocket = clientsSocket; 
		
		class RicettoreEventi implements Runnable {
			Socket s; 
			public RicettoreEventi(Socket s) {
				this.s = s; 
			}
			@Override
			public void run() {
				try {
					attesaMessaggio(s);
				} catch (ClassNotFoundException | IOException e) {
					LOGGER.log(Level.SEVERE, "errore nell'avvio dell'attesa dei messaggi", e); 
				}
				
			}
		}
		
		for(Socket socket : clientsSocket.values()) {
			(new Thread(new RicettoreEventi(socket))).start();
		}
	
	}
	/**
	 * Genera l'evento AvvioSchermataPrincipale e, tramite la funzione inviaEventoController(evento), 
	 * lo invia a tutti i client. 
	 * @param nomiGiocatori: contiene tutti i nomi dei giocatori che partecipano alla partita
	 * @param numeroGiocatori: è il numero di giocatori che partecipano alla partita
	 */
	public void avviaSchermate(List<String> nomiGiocatori, int numeroGiocatori) {
		AvvioSchermataPrincipale evento = new AvvioSchermataPrincipale(); 
		evento.setNomiGiocatori(nomiGiocatori);
		evento.setNumeroGiocatori(numeroGiocatori);
		inviaEventoController(evento);
	}
	
	/**
	 * Quando invocato, fa in modo che si resti in attesa di messaggi in ingresso. Quando arriva un messaggio, dopo
	 * averlo "interpretato", viene creato l'evento contenente ciò che si è ricevuto in input e questo evento viene 
	 * notificato a tutti gli observer. 
	 */
	private void attesaMessaggio(Socket s) throws IOException, ClassNotFoundException {
		EventoGenerico evento = null; 
		ObjectInputStream in; 
		while(true) {
			in = (new ObjectInputStream(s.getInputStream())); 
			evento = (EventoGenerico)in.readObject(); 
			if(evento!=null) {
				setChanged(); 
				notifyObservers(evento); 
			}
		}
	}
	
	/**
	 * Invia l'evento ricevuto come parametro a tutti i client
	 * @param evento: è di tipo EventoGenerico e viene poi inviato a tutti i client in output
	 */
	public void inviaEventoController(EventoGenerico evento) {
		for(Socket s: clientsSocket.values()) {
			ObjectOutputStream objectOutputStream;
			try {
				objectOutputStream = new ObjectOutputStream(s.getOutputStream());
				objectOutputStream.flush();
				objectOutputStream.writeObject(evento);
				
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "errore nell'invio di un evento del controller", e); 
			} 
			
		}
	}

	/**
	 * Genera l'evento PosizionaCancello, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void posizionaCancello(String idStradaPartenza, boolean faseFinale) {
		PosizionaCancello evento = new PosizionaCancello(); 
		evento.setIdStradaPartenza(idStradaPartenza);
		evento.setFaseFinale(faseFinale);
		inviaEventoController(evento); 
	}
	
	/**
	 * Genera l'evento AttivaBottoniTessereSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void attivaBottoniTessereSelezionabili(List<Integer> numeriTessereCorrispondenti) {
		AttivaBottoniTessereSelezionabili evento = new AttivaBottoniTessereSelezionabili(); 
		evento.setNumeriTessereCorrispondenti(numeriTessereCorrispondenti);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaBottoniTessereTerreno. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaBottoniTessereTerreno() {
		DisattivaBottoniTessereTerreno evento = new DisattivaBottoniTessereTerreno(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento AttivaBottoniStradeSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void attivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		AttivaBottoniStradeSelezionabili evento = new AttivaBottoniStradeSelezionabili(); 
		evento.setIdStradeSelezionabili(idStradeSelezionabili);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaBottoniStradeSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		DisattivaBottoniStradeSelezionabili evento = new DisattivaBottoniStradeSelezionabili(); 
		evento.setIdStradeSelezionabili(idStradeSelezionabili);
		inviaEventoController(evento); 
		
	}

	/**
	 * Genera l'evento SetIconaGiocatoreInStrada, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setIconaGiocatoreInStrada(String idStradaArrivo,int coordinataXPartenza, int coordinataYPartenza,int coordinataXArrivo, int coordinataYArrivo,int posizioneGiocatoreInArray) {
		SetIconaGiocatoreInStrada evento = new SetIconaGiocatoreInStrada(); 
		evento.setIdStradaArrivo(idStradaArrivo);
		evento.setCoordinataXPartenza(coordinataXPartenza);
		evento.setCoordinataYPartenza(coordinataYPartenza);
		evento.setCoordinataXArrivo(coordinataXArrivo);
		evento.setCoordinataYArrivo(coordinataYArrivo);
		evento.setPosizioneGiocatoreInArray(posizioneGiocatoreInArray);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetIconaGiocatoreInizio, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setIconaGiocatoreInizio(String idStradaArrivo,int posizioneGiocatoreInArray) {
		SetIconaGiocatoreInizio evento = new SetIconaGiocatoreInizio(); 
		evento.setIdStradaArrivo(idStradaArrivo);
		evento.setPosizioneGiocatoreInArray(posizioneGiocatoreInArray);
		inviaEventoController(evento); 
		
	}
	
	/**
	 * Genera l'evento EseguiLancioDado, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void eseguiLancioDado(int valoreLancioDado) {
		EseguiLancioDado evento = new EseguiLancioDado(); 
		evento.setValoreLancioDado(valoreLancioDado);
		inviaEventoController(evento); 
		
	}

	/**
	 * Genera l'evento RemoveLancioDado. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void removeLancioDado() {
		RemoveLancioDado evento = new RemoveLancioDado(); 
		inviaEventoController(evento); 
		
	}

	/**
	 * Genera l'evento SetLabelComunicazioni, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setLabelComunicazioni(String string) {
		SetLabelComunicazioni evento = new SetLabelComunicazioni(); 
		evento.setString(string);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento AttivaAnimaliSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void attivaAnimaliSelezionabili(List<Integer> animaliSelezionabili) {
		AttivaAnimaliSelezionabili evento = new AttivaAnimaliSelezionabili(); 
		evento.setAnimaliSelezionabili(animaliSelezionabili);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento AttivaBottoniRegioni. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void attivaBottoniRegioni() {
		AttivaBottoniRegioni evento = new AttivaBottoniRegioni(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaBottoniRegioni. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaBottoniRegioni() {
		DisattivaBottoniRegioni evento = new DisattivaBottoniRegioni(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento AttivaBottoniMosseSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void attivaBottoniMosseSelezionabili(List<Integer> mosseSelezionabili) {
		AttivaBottoniMosseSelezionabili evento = new AttivaBottoniMosseSelezionabili(); 
		evento.setMosseSelezionabili(mosseSelezionabili);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaBottoniMosse, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaBottoniMosse(int numero) {
		DisattivaBottoniMosse evento = new DisattivaBottoniMosse(); 
		evento.setNumeroMossaDaNonDisattivare(numero);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetDanariGiocatore, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setDanariGiocatore(int indice, int danari) {
		SetDanariGiocatore evento = new SetDanariGiocatore(); 
		evento.setIndice(indice);
		evento.setDanari(danari);
		inviaEventoController(evento); 	
	}

	/**
	 * Genera l'evento SetCostoTesseraTerreno, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setCostoTesseraTerreno(int tipo, int nuovoCosto) {
		SetCostoTesseraTerreno evento = new SetCostoTesseraTerreno(); 
		evento.setTipoTerreno(tipo);
		evento.setNuovoCosto(nuovoCosto);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetTotalePecoreBiancheInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setTotalePecoreBiancheInRegione(String idRegione, int totalePecoreBianche) {
		SetTotalePecoreBiancheInRegione evento = new SetTotalePecoreBiancheInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotalePecoreBianche(totalePecoreBianche);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetTotaleAgnelliInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setTotaleAgnelliInRegione(String idRegione, int totaleAgnelli) {
		SetTotaleAgnelliInRegione evento = new SetTotaleAgnelliInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotaleAgnelli(totaleAgnelli);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetTotaleArietiInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setTotaleArietiInRegione(String idRegione, int totaleArieti) {
		SetTotaleArietiInRegione evento = new SetTotaleArietiInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotaleArieti(totaleArieti);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetPosizioneLupo, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setPosizioneLupo(String idRegionePartenza,String idRegioneArrivo) {
		SetPosizioneLupo evento = new SetPosizioneLupo(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetPosizionePecoraNera, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setPosizionePecoraNera(String idRegionePartenza,String idRegioneArrivo) {
		SetPosizionePecoraNera evento = new SetPosizionePecoraNera(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento SetGiocatoreDelTurno, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setGiocatoreDelTurno(String nomeGiocatore) {
		SetGiocatoreDelTurno evento = new SetGiocatoreDelTurno(); 
		evento.setNomeGiocatore(nomeGiocatore);
		inviaEventoController(evento); 	
	}

	/**
	 * Genera l'evento InserisciGiocatore, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void inserisciGiocatore(int indice) {
		InserisciGiocatore evento = new InserisciGiocatore(); 
		evento.setIndice(indice);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaBottoniMosse. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaBottoniMosse() {
		DisattivaTuttiBottoniMosse evento = new DisattivaTuttiBottoniMosse(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento VisibilityBottoniAnimali, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void visibilityBottoniAnimali(String idRegione, boolean condizione) {
		VisibilityBottoniAnimali evento = new VisibilityBottoniAnimali(); 
		evento.setIdRegione(idRegione);
		evento.setCondizione(condizione);
		inviaEventoController(evento); 
	}
	
	/**
	 * Genera l'evento SpostamentoAnimale, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void spostamentoAnimale(String idRegionePartenza,String idRegioneArrivo, String animale) {
		SpostamentoAnimale evento = new SpostamentoAnimale(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		evento.setAnimale(animale);
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaPannelloSelezioneAnimali. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaPannelloSelezioneAnimali() {
		DisattivaPannelloSelezioneAnimali evento = new DisattivaPannelloSelezioneAnimali(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento MorteAnimale, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void morteAnimale(String tipoAnimale, String idRegione) {
		MorteAnimale evento = new MorteAnimale(); 
		evento.setTipoAnimale(tipoAnimale);
		evento.setIdRegione(idRegione);
		inviaEventoController(evento);
	}
	
	/**
	 * Genera l'evento NascitaAgnello, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void nascitaAgnello(String idRegione) {
		NascitaAgnello evento = new NascitaAgnello(); 
		evento.setIdRegione(idRegione);
		inviaEventoController(evento);
	}

	/**
	 * Genera l'evento SetTessereTerrenoInPossesso, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void setTessereInPossesso(List<Integer> tessereInPossesso, int indiceGiocatore) {
		SetTessereInPossesso evento = new SetTessereInPossesso(); 
		evento.setTessereInPossesso(tessereInPossesso);
		evento.setIndiceGiocatore(indiceGiocatore);
		inviaEventoController(evento);
	}
	
	/**
	 * Genera l'evento GeneraRisultato, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void generaRisultato(List<Integer> punteggiGiocatori, int punteggioMassimo) {
		GeneraRisultato evento = new GeneraRisultato(); 
		evento.setPunteggiGiocatori(punteggiGiocatori);
		evento.setPunteggioMassimo(punteggioMassimo);
		inviaEventoController(evento);
	}

	/**
	 * Genera l'evento AvvisoMosseNonDisponibili. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void avvisoMosseNonDisponibili() {
		AvvisoMosseNonDisponibili evento = new AvvisoMosseNonDisponibili(); 
		inviaEventoController(evento); 
	}

	/**
	 * Genera l'evento DisattivaPannelloMosseNonDisponibili. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void disattivaPannelloMosseNonDisponibili() {
		DisattivaPannelloMosseNonDisponibili evento = new DisattivaPannelloMosseNonDisponibili(); 
		inviaEventoController(evento);	
	}

	/**
	 * Genera l'evento ChiudiSchermata. Invia poi l'evento a tutti i client tramite inviaEventoController(evento) 
	 */
	@Override
	public void chiudiSchermata() {
		ChiusuraFrameGiocatore evento = new ChiusuraFrameGiocatore(); 
		inviaEventoController(evento);
	}

}
