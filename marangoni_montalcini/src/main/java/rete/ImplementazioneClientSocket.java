package rete;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.SchermataPrincipale;
import eventi.*;

public class ImplementazioneClientSocket implements Observer {
	
	private SchermataPrincipale schermataPrincipale; 
	
	private Integer indice; 
	
	private Socket socket; 
	
	private static final Logger LOGGER = Logger.getLogger("implementazioneClientSocket"); 
	
	/**
	 * Costruttore
	 * @param indirizzoIPServer: l'indirizzo ip del server a cui connettersi
	 * @param porta: la porta su cui è resa disponibile la connessione
	 * @param nome: il nome del giocatore
	 * @param dataCarezza: la data dell'ultima carezza
	 */
	public ImplementazioneClientSocket(String indirizzoIPServer, int porta, String nome, Date dataCarezza) {
		
		indice = null;
		try {
			//creo una nuova socket sull'indirizzo ip e la porta passati come parametro, ed invio l'evento di inserimento del giocatore in output
			socket = new Socket(indirizzoIPServer, porta);
			NomeData evento = new NomeData(); 
			evento.setNomeGiocatore(nome);
			evento.setDataUltimaCarezza(dataCarezza);
			(new ObjectOutputStream(socket.getOutputStream())).writeObject(evento);
			while(true) {
				indice = Integer.valueOf((new BufferedReader(new InputStreamReader(socket.getInputStream()))).readLine()); 
				if(indice!=null) {
					break; 
				}
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "errore nell'invio dell'evento NomeData o nella ricezione del proprio indice", e);
		}
		(new Thread (new Runnable() {
			@Override
			public void run() {
				try{
					attesaMessaggio(); 
				} catch(IOException | ClassNotFoundException e) {
					LOGGER.log(Level.SEVERE, "errore nell'avvio del thread che attende i messaggi", e);
				}
			}
		})).start(); 
	}
	
	/**
	 * Method che si mette in ascolto
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void attesaMessaggio() throws IOException, ClassNotFoundException{
		EventoGenerico evento = null; 
		while(true) {
			//deserializzo
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream()); 
			evento = (EventoGenerico)in.readObject(); 
			if(evento!=null) {
				ricezioneEvento(evento); 
			}
		}
	}
	
	/**
	 * Method che riceve un evento in ingresso e, se l'evento è relativo all'avvio della schermata principale, la avvia e rende
	 * l'implementazioneClientSocket Observer nei suoi confronti, altrimenti, chiama il method aggiornaSchermataPrincipale presente 
	 * nella schermata principale passandogli l'evento ricevuto e l'indice
	 * @param evento
	 */
	public void ricezioneEvento(EventoGenerico evento) {
		if (TipoEvento.AVVIO_SCHERMATA_PRINCIPALE.equals(evento.getTipo())) {
				schermataPrincipale = new SchermataPrincipale(((AvvioSchermataPrincipale)evento).getNumeroGiocatori(), ((AvvioSchermataPrincipale)evento).getNomiGiocatori()); 
				schermataPrincipale.run();
				schermataPrincipale.addObserver(this);
		} else if(schermataPrincipale != null) {
			schermataPrincipale.aggiornaSchermataPrincipale(evento, indice);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		EventoGenerico evento = (EventoGenerico) arg; 
		evento.setIndiceGiocatore(indice);
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(evento);
			out.flush();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Errore nell'invio in rete dell'evento", e);
		} 
	}
	
}

	