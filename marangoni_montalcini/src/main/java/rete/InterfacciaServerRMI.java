package rete;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import eventi.EventoGenerico;

public interface InterfacciaServerRMI extends Remote {
	
	int getIndex() throws RemoteException;
	
	void aggiungiClient(InterfacciaClientRMI client, String nomeGiocatore, Date data) throws RemoteException;
	
	void inserisciGiocatore(int index, String nomeGiocatore, Date dataUltimaCarezza) throws RemoteException;
	
	int getNumeroGiocatori() throws RemoteException;
	
	List<String> getNomiGiocatori() throws RemoteException;
	
	void riceviEvento(EventoGenerico evento, int index) throws RemoteException; 
	
}