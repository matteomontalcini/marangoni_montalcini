package rete;

import java.rmi.RemoteException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.SchermataPrincipale;
import eventi.*;

public class ImplementazioneClientRMI implements InterfacciaClientRMI, Observer {
	
	private SchermataPrincipale schermataPrincipale;
	
	private InterfacciaServerRMI server;
	
	private int mioIndex;
	
	private static final Logger LOGGER = Logger.getLogger("implementazioneClient"); 
	
	/**
	 * è il costruttore: crea una nuova ImplementazioneClient, salvandosi il riferimento all'interfacciaServer
	 * che gli viene passato come parametro
	 * @param server: è il riferimento all'interfacciaServer, di cui potrà chiamare dei metodi. 
	 */
	public ImplementazioneClientRMI(InterfacciaServerRMI server){
		this.server = server;
	}
	
	/**
	 * setta l'indice del giocatore nella partita
	 */
	public void setMioIndex(int mioIndex) {
		this.mioIndex = mioIndex;
	}
	
	/**
	 * restituisce il valore dell'attributo mioIndex; 
	 */
	public int getMioIndex() {
		return this.mioIndex;
	}
	
	/**
	 * Crea una nuova schermataPrincipale (cioè la schermata di gioco) e la avvia. 
	 */
	@Override
	public void mostraSchermata(int index) throws RemoteException {
		schermataPrincipale = new SchermataPrincipale(server.getNumeroGiocatori(), server.getNomiGiocatori());
		schermataPrincipale.run();
		schermataPrincipale.addObserver(this);
	}

	/**
	 * L'implementazione client riceve degli eventi: questo method permette di interpretare il particolare
	 * evento che viene ricevuto e, in base alla tipologia, vengono chiamati i corrispondenti metodi sulla schermata
	 * di gioco. 
	 */
	@Override
	public void interpretaEvento(EventoGenerico evento) {
		schermataPrincipale.aggiornaSchermataPrincipale(evento, mioIndex);
	}
	
	/**
	 * L'implementazioneClient è observer nei confronti della schermataPrincipale: quando questa genera un evento, lo
	 * notifica all'implementazioneClient. Tramite questo method, viene chiamato il method "riceviEvento" dell'interfacciaServer, 
	 * a cui viene passato come parametro il particolare evento ricevuto e l'indice del giocatore. 
	 */
	@Override
	public void update(Observable o, Object arg) {
		if(!(o.equals( schermataPrincipale)) || !(arg instanceof EventoGenerico)) {
			throw new IllegalArgumentException();
			}	
		try {
			server.riceviEvento((EventoGenerico)arg, mioIndex);
		} catch (RemoteException e) {
			LOGGER.log(Level.SEVERE, "Ricezione evento fallita!", e); 
		}
	}

}