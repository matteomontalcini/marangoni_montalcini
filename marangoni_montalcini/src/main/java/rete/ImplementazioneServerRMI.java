package rete;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.StatoPartita;
import model.Turno;
import view.InterfacciaView;
import controller.ControllerPartita;
import eventi.EventoGenerico;

public class ImplementazioneServerRMI extends UnicastRemoteObject implements InterfacciaServerRMI   {
	
	private static final long serialVersionUID = -7098548671967083832L;
	
	/**
	 * è una mappa che memorizza le corrispondenze tra interfacciaClient e indice del giocatore corrispondente
	 */
	private Map<Integer, InterfacciaClientRMI> clients = new HashMap<Integer, InterfacciaClientRMI>();
	
	/**
	 * è il controller di gioco
	 */
	private ControllerPartita controllerPartita;
	
	private StatoPartita statoPartita;
	
	private InterfacciaView view;
	
	private int index;
	
	/**
	 * è il numero di giocatori che partecipa alla partita, nonchè il numero di giocatori di cui si attenderà
	 * la connessione
	 */
	private int numeroGiocatori;
	
	/**
	 * è il numero di giocatori che si sono già connessi
	 */
	private int giocatoriConnessi;
	
	private static final Logger LOGGER = Logger.getLogger("implementazioneServer"); 

	/**
	 * è il costruttore di ImplementazioneServer. Chiama i costruttori di statoPartita, di ImplementazioneView, 
	 * di controllerPartita e setta l'indice = 1: sarà l'indice del giocatore che farà da server. 
	 * @param numeroGiocatori:è il numero di giocatori che parteciperà alla partita. 
	 * @throws RemoteException
	 */
	public ImplementazioneServerRMI(int numeroGiocatori) throws RemoteException {
		super(0);	
		this.statoPartita = new StatoPartita(numeroGiocatori);
		view = new ImplementazioneViewRMI(clients); 
		this.controllerPartita = new ControllerPartita(statoPartita, view);
		this.index = 1;
		this.numeroGiocatori = numeroGiocatori;
		this.giocatoriConnessi = 0;
	}
	
	/**
	 * restituisce l'indice, cioè il valore attuale di index e che deve essere assegnato nella corrispondenza
	 * tra interfacciaClient del nuovo giocatore che si connette e il suo indice. 
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * restituisce una lista contenente i nomi dei giocatori che vi partecipano
	 */
	public List<String> getNomiGiocatori() throws RemoteException {
		return statoPartita.getNomiGiocatori();
	}

	/**
	 * restituisce il numero di giocatori che partecipano alla partita.
	 */
	public int getNumeroGiocatori() throws RemoteException {
		return numeroGiocatori;
	}
	
	/**
	 * Chiama il method che genera la nuova schermata su tutti i riferimenti alle interfacceClient presenti nella 
	 * hashMap, poi chiama i metodi di statoPartita, controller e view che sono necessari per poter avviare la partita.
	 * @throws RemoteException
	 */
	private void startPartita() throws RemoteException {
		List<String> nomiGiocatori = new ArrayList<String>(); 
		List<Date> dateCarezze = new ArrayList<Date>(); 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			nomiGiocatori.add(statoPartita.getGiocatori().get(i).getNomeGiocatore()); 
			dateCarezze.add(statoPartita.getGiocatori().get(i).getUltimaCarezza());
		}
		if(!controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			LOGGER.log(Level.SEVERE,  "Impossibile avviare il gioco", new IllegalArgumentException());
		} else if(controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			for(Entry<Integer, InterfacciaClientRMI> client : clients.entrySet()){
				client.getValue().mostraSchermata(client.getKey());
			}
			statoPartita.inizializzaCostiTessereTerreno();
			controllerPartita.stabilisciPrimoGiocatore();
			statoPartita.setTurno(new Turno(statoPartita.getPrimoGiocatore()));
			controllerPartita.distribuisciTessereIniziali();
			for(int i = 1; i <= numeroGiocatori; i++) {
				view.setTessereInPossesso(statoPartita.getGiocatori().get(i-1).getTessereTerrenoInPossesso(), i);
			}
			controllerPartita.getControllerTurno().scegliStradePartenza();
		}
	}
	
	/**
	 * Questo method viene invocato ogni volta che si aggiunge un nuovo client: aggiunge la corrispondenza 
	 * interfacciaClient-indice nella hashMap, assegna al client il suo indice, inserisce il giocatore tra quelli
	 * partecipanti alla partita e incrementa di una unità l'indice che sarà assegnato al client successivo
	 */
	@Override
	public void aggiungiClient(InterfacciaClientRMI client, String nomeGiocatore, Date data) throws RemoteException {
		clients.put(index, client);
		client.setMioIndex(index);
		inserisciGiocatore(index, nomeGiocatore, data); 
		index++;
	}
	
	/**
	 * Chiama il method "inserisciGiocatore" del controller e poi effettua un controllo sul numero di giocatori connessi: 
	 * se questo numero è uguale a quello dei giocatori che parteciperanno alla partita, viene chiamato il method "startPartita()"
	 */
	@Override
	public void inserisciGiocatore(int index, String nomeGiocatore, Date dataUltimaCarezza) throws RemoteException {
		controllerPartita.inserisciGiocatore(index, nomeGiocatore, dataUltimaCarezza, numeroGiocatori);
		giocatoriConnessi++;
		if(giocatoriConnessi == numeroGiocatori) {
			try {
				startPartita();
				
			} catch (RemoteException e) {
				LOGGER.log(Level.SEVERE, "Inizio partita fallito!", e); 
			}
		}
	}
	
	/**
	 * Questo method, che riceve in ingresso un evento generico, effettua un controllo sull'indice del giocatore
	 * che ha inviato l'evento: se è l'indice corrispondente al giocatore del turno, all'ora chiama la funzione 
	 * "update" del controller, altrimenti scarta l'evento. 
	 */
	@Override 
	public void riceviEvento(EventoGenerico evento, int index) {
		if(statoPartita.getTurno().getGiocatore() == statoPartita.getGiocatori().get(index - 1)) {
			controllerPartita.update(null, evento);
		}
	}
}
	