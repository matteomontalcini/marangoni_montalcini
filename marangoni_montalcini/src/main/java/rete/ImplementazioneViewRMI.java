package rete;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.InterfacciaView;
import eventi.*; 


public class ImplementazioneViewRMI implements InterfacciaView {
	
	/**
	 * l'hashMap che contiene le associazioni tra InterfacciaClient e il corrispondente indice del giocatore
	 */
	Map<Integer, InterfacciaClientRMI> clients; 
	
	
	private static final Logger LOGGER = Logger.getLogger("implementazioneView"); 
	
	/**
	 * costruttore: crea una nuova ImplementazioneView, settando anche l'attributo clients con il parametro che viene 
	 * passato in ingresso
	 * @param clients: è l'hashMap
	 */
	public ImplementazioneViewRMI(Map<Integer, InterfacciaClientRMI> clients) {
		this.clients = clients; 
	}
	
	/**
	 * questo method chiama la funzione intepretaEvento su ogni client, passando come parametro l'evento che 
	 * ha generato e che deve essere interpretato dai client
	 * @param evento
	 */
	public void inviaEvento(EventoGenerico evento) {
		for(Entry<Integer, InterfacciaClientRMI> client : clients.entrySet()){
			try {
				client.getValue().interpretaEvento(evento);
			} catch (RemoteException e) {
				LOGGER.log(Level.SEVERE, "invio evento fallito!", e);
			}
		}
	}

	/**
	 * Genera l'evento PosizionaCancello, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void posizionaCancello(String idStradaPartenza, boolean faseFinale) {
		PosizionaCancello evento = new PosizionaCancello(); 
		evento.setIdStradaPartenza(idStradaPartenza);
		evento.setFaseFinale(faseFinale);
		inviaEvento(evento);
	}
	
	/**
	 * Genera l'evento AttivaBottoniTessereSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void attivaBottoniTessereSelezionabili(List<Integer> numeriTessereCorrispondenti) {
		AttivaBottoniTessereSelezionabili evento = new AttivaBottoniTessereSelezionabili(); 
		evento.setNumeriTessereCorrispondenti(numeriTessereCorrispondenti);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento DisattivaBottoniTessereTerreno. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaBottoniTessereTerreno() {
		DisattivaBottoniTessereTerreno evento = new DisattivaBottoniTessereTerreno(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento AttivaBottoniStradeSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void attivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		AttivaBottoniStradeSelezionabili evento = new AttivaBottoniStradeSelezionabili(); 
		evento.setIdStradeSelezionabili(idStradeSelezionabili);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento DisattivaBottoniStradeSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaBottoniStradeSelezionabili(List<String> idStradeSelezionabili) {
		DisattivaBottoniStradeSelezionabili evento = new DisattivaBottoniStradeSelezionabili(); 
		evento.setIdStradeSelezionabili(idStradeSelezionabili);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetIconaGiocatoreInStrada, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setIconaGiocatoreInStrada(String idStradaArrivo, int coordinataXPartenza, int coordinataYPartenza,int coordinataXArrivo, int coordinataYArrivo,int posizioneGiocatoreInArray) {
		SetIconaGiocatoreInStrada evento = new SetIconaGiocatoreInStrada(); 
		evento.setIdStradaArrivo(idStradaArrivo);
		evento.setCoordinataXPartenza(coordinataXPartenza);
		evento.setCoordinataYPartenza(coordinataYPartenza);
		evento.setCoordinataXArrivo(coordinataXArrivo);
		evento.setCoordinataYArrivo(coordinataYArrivo);
		evento.setPosizioneGiocatoreInArray(posizioneGiocatoreInArray);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetIconaGiocatoreInizio, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setIconaGiocatoreInizio(String idStradaArrivo,int posizioneGiocatoreInArray) {
		SetIconaGiocatoreInizio evento = new SetIconaGiocatoreInizio(); 
		evento.setIdStradaArrivo(idStradaArrivo);
		evento.setPosizioneGiocatoreInArray(posizioneGiocatoreInArray);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento EseguiLancioDado, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void eseguiLancioDado(int valoreLancioDado) {
		EseguiLancioDado evento = new EseguiLancioDado(); 
		evento.setValoreLancioDado(valoreLancioDado);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento RemoveLancioDado. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void removeLancioDado() {
		RemoveLancioDado evento = new RemoveLancioDado(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetLabelComunicazioni, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setLabelComunicazioni(String string) {
		SetLabelComunicazioni evento = new SetLabelComunicazioni(); 
		evento.setString(string);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento AttivaAnimaliSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void attivaAnimaliSelezionabili(List<Integer> animaliSelezionabili) {
		AttivaAnimaliSelezionabili evento = new AttivaAnimaliSelezionabili(); 
		evento.setAnimaliSelezionabili(animaliSelezionabili);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento AttivaBottoniRegioni. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void attivaBottoniRegioni() {
		AttivaBottoniRegioni evento = new AttivaBottoniRegioni(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento DisattivaBottoniRegioni. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaBottoniRegioni() {
		DisattivaBottoniRegioni evento = new DisattivaBottoniRegioni(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento AttivaBottoniMosseSelezionabili, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void attivaBottoniMosseSelezionabili(List<Integer> mosseSelezionabili) {
		AttivaBottoniMosseSelezionabili evento = new AttivaBottoniMosseSelezionabili(); 
		evento.setMosseSelezionabili(mosseSelezionabili);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento DisattivaBottoniMosse, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaBottoniMosse(int numero) {
		DisattivaBottoniMosse evento = new DisattivaBottoniMosse(); 
		evento.setNumeroMossaDaNonDisattivare(numero);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetDanariGiocatore, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setDanariGiocatore(int indice, int danari) {
		SetDanariGiocatore evento = new SetDanariGiocatore(); 
		evento.setIndice(indice);
		evento.setDanari(danari);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetTotalePecoreBiancheInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setTotalePecoreBiancheInRegione(String idRegione,int totalePecoreBianche) {
		SetTotalePecoreBiancheInRegione evento = new SetTotalePecoreBiancheInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotalePecoreBianche(totalePecoreBianche);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetTotaleAgnelliInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setTotaleAgnelliInRegione(String idRegione, int totaleAgnelli) {
		SetTotaleAgnelliInRegione evento = new SetTotaleAgnelliInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotaleAgnelli(totaleAgnelli);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetTotaleArietiInRegione, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setTotaleArietiInRegione(String idRegione, int totaleArieti) {
		SetTotaleArietiInRegione evento = new SetTotaleArietiInRegione(); 
		evento.setIdRegione(idRegione);
		evento.setTotaleArieti(totaleArieti);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetPosizioneLupo, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setPosizioneLupo(String idRegionePartenza,String idRegioneArrivo) {
		SetPosizioneLupo evento = new SetPosizioneLupo(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetPosizionePecoraNera, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setPosizionePecoraNera(String idRegionePartenza,String idRegioneArrivo) {
		SetPosizionePecoraNera evento = new SetPosizionePecoraNera(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		inviaEvento(evento);	
	}

	/**
	 * Genera l'evento SetGiocatoreDelTurno, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setGiocatoreDelTurno(String nomeGiocatore) {
		SetGiocatoreDelTurno evento = new SetGiocatoreDelTurno(); 
		evento.setNomeGiocatore(nomeGiocatore);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento InserisciGiocatore, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void inserisciGiocatore(int indice) {
		InserisciGiocatore evento = new InserisciGiocatore(); 
		evento.setIndice(indice);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento DisattivaBottoniMosse. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaBottoniMosse() {
		DisattivaTuttiBottoniMosse evento = new DisattivaTuttiBottoniMosse(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento VisibilityBottoniAnimali, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void visibilityBottoniAnimali(String idRegione, boolean condizione) {
		VisibilityBottoniAnimali evento = new VisibilityBottoniAnimali(); 
		evento.setIdRegione(idRegione);
		evento.setCondizione(condizione);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SpostamentoAnimale, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void spostamentoAnimale(String idRegionePartenza, String idRegioneArrivo, String animale) {
		SpostamentoAnimale evento = new SpostamentoAnimale(); 
		evento.setIdRegionePartenza(idRegionePartenza);
		evento.setIdRegioneArrivo(idRegioneArrivo);
		evento.setAnimale(animale);
		inviaEvento(evento);	
	}

	/**
	 * Genera l'evento DisattivaPannelloSelezioneAnimali. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaPannelloSelezioneAnimali() {
		DisattivaPannelloSelezioneAnimali evento = new DisattivaPannelloSelezioneAnimali(); 
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento SetCostoTesseraTerreno, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setCostoTesseraTerreno(int tipo, int nuovoCosto) {
		SetCostoTesseraTerreno evento = new SetCostoTesseraTerreno(); 
		evento.setTipoTerreno(tipo);
		evento.setNuovoCosto(nuovoCosto);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento MorteAnimale, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void morteAnimale(String tipoAnimale, String idRegione) {
		MorteAnimale evento = new MorteAnimale(); 
		evento.setTipoAnimale(tipoAnimale);
		evento.setIdRegione(idRegione);
		inviaEvento(evento);	
	}

	/**
	 * Genera l'evento NascitaAgnello, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void nascitaAgnello(String idRegione) {
		NascitaAgnello evento = new NascitaAgnello(); 
		evento.setIdRegione(idRegione);
		inviaEvento(evento);	
	}

	/**
	 * Genera l'evento SetTessereTerrenoInPossesso, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void setTessereInPossesso(List<Integer> tessereInPossesso, int indiceGiocatore) {
		SetTessereInPossesso evento = new SetTessereInPossesso(); 
		evento.setTessereInPossesso(tessereInPossesso);
		evento.setIndiceGiocatore(indiceGiocatore);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento GeneraRisultato, settando i vari attributi dell'evento con i parametri che vengono
	 * passati in ingresso. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void generaRisultato(List<Integer> punteggiGiocatori,int punteggioMassimo) {
		GeneraRisultato evento = new GeneraRisultato(); 
		evento.setPunteggiGiocatori(punteggiGiocatori);
		evento.setPunteggioMassimo(punteggioMassimo);
		inviaEvento(evento);
	}

	/**
	 * Genera l'evento AvvisoMosseNonDisponibili. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void avvisoMosseNonDisponibili() {
		AvvisoMosseNonDisponibili evento = new AvvisoMosseNonDisponibili(); 
		inviaEvento(evento); 
	}

	/**
	 * Genera l'evento DisattivaPannelloMosseNonDisponibili. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void disattivaPannelloMosseNonDisponibili() {
		DisattivaPannelloMosseNonDisponibili evento = new DisattivaPannelloMosseNonDisponibili(); 
		inviaEvento(evento);	
	}

	/**
	 * Genera l'evento ChiudiSchermata. Invia poi l'evento a tutti i client tramite inviaEvento(evento) 
	 */
	@Override
	public void chiudiSchermata() {
		ChiusuraFrameGiocatore evento = new ChiusuraFrameGiocatore(); 
		inviaEvento(evento);	
	}
	
}
