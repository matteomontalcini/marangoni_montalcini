package rete;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Giocatore;
import model.StatoPartita;
import model.Turno;
import controller.ControllerPartita;
import eventi.NomeData;

/**
 * Raccoglie le connessioni provenienti dai client sulla server socket, una volta ricevute tutte le connessioni
 * fa partire il gioco in rete.
 */
public class RaccoltaSocket {
	
	private int porta; 
	
	private int numeroGiocatori; 
	
	private StatoPartita statoPartita; 
	
	private ControllerPartita controllerPartita; 
	
	Map<Integer, Socket> clientsSocket = new HashMap<Integer, Socket>(); 
	
	private int index; 
	
	private List<String> nomiGiocatori = new ArrayList<String>(); 
	
	private List<Date> dateCarezze = new ArrayList<Date>(); 
	
	private static final Logger LOGGER = Logger.getLogger("raccoltaSocket"); 
	
	/**
	 * Costruttore
	 * @param numeroGiocatori: numero di giocatori che parteciperanno alla partita
	 * @param porta: numero di porta
	 */
	public RaccoltaSocket(int numeroGiocatori, int porta) {
			this.porta = porta; 
			this.numeroGiocatori = numeroGiocatori; 
			this.statoPartita = new StatoPartita(numeroGiocatori);
			this.index = 1;
			this.numeroGiocatori = numeroGiocatori;
	}
	
	/**
	 * Avvia la server socket ed aspetta le connessioni su questa
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void avviaServerSocket() throws IOException, ClassNotFoundException {
		ServerSocket server = new ServerSocket(porta); 
		Socket s; 
		NomeData nomeData; 

		for(int i = 0; i < numeroGiocatori; i++) {
			s = server.accept(); 
			clientsSocket.put(index, s); 
			ObjectInputStream in;
			nomeData = null;
			while(true) {
				in = (new ObjectInputStream(s.getInputStream()));
				//il primo evento è di questo tipo, una volta ricevuto si può aggiungere il giocatore allo stato partita
				nomeData = ((NomeData)in.readObject()); 
				if(nomeData!=null) {
					nomiGiocatori.add(nomeData.getNomeGiocatore()); 
					dateCarezze.add(nomeData.getDataUltimaCarezza()); 
					statoPartita.addGiocatore(new Giocatore(nomeData.getNomeGiocatore(), nomeData.getDataUltimaCarezza(), numeroGiocatori));
					(new PrintWriter(s.getOutputStream(), true)).println(index); 
					index ++; 
					break; 
				}
			}	
		}
		//sono state ricevute tutte le connessioni che si aspettavano
		ImplementazioneServerSocket view = new ImplementazioneServerSocket(clientsSocket); 
		this.controllerPartita = new ControllerPartita(statoPartita, view);
		//si effettua un controllo sui nomi inseriti e, se i nomi inseriti sono corretti, chiama la funzione avvia schermata
		//che genera l'evento di avvio schermata, altrimenti il gioco non parte ed appare un messaggio di errore.
		if(!controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			LOGGER.log(Level.SEVERE,  "Impossibile avviare il gioco", new IllegalArgumentException());
		} else if(controllerPartita.controlloNomiEDate(nomiGiocatori, dateCarezze)) {
			view.avviaSchermate(nomiGiocatori, numeroGiocatori);
			view.addObserver(controllerPartita);
			statoPartita.inizializzaCostiTessereTerreno();
			controllerPartita.stabilisciPrimoGiocatore();
			statoPartita.setTurno(new Turno(statoPartita.getPrimoGiocatore()));
			controllerPartita.distribuisciTessereIniziali();
			for(int i = 1; i <= numeroGiocatori; i++) {
				view.setTessereInPossesso(statoPartita.getGiocatori().get(i-1).getTessereTerrenoInPossesso(), i);
			}
			controllerPartita.getControllerTurno().scegliStradePartenza();
		}
	}
	
}
