package rete;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class ClientRMI {
	
	private static final Logger LOGGER = Logger.getLogger("clientRMI"); 
	
	private String indirizzoIPACuiConnettersi;
	
	/**
	 * è il costruttore: setta l'attributo "indirizzoIPACuiConnettersi" tramite il paramentro che gli viene passato in ingresso
	 * @param indirizzoIPACuiConnettersi: è l'indirizzoIP del giocatore server
	 */
	public ClientRMI(String indirizzoIPACuiConnettersi) {
		this.indirizzoIPACuiConnettersi = indirizzoIPACuiConnettersi;
	}
	
	/**
	 * crea una nuova interfacciaServer e una implementazioneClient; inoltre crea anche una InterfacciaClient, che passa
	 * come attributo al method "aggiungiClient" dell'interfacciaServer, che permetterà così di aggiungere l'interfacciaClient
	 * alla HashMap in cui sono presenti le corrispondenze indice-interfacciaClient
	 * @param nomeGiocatore: è il nome del giocatore che parteciperà alla partita come Client
	 * @param data: è la data in cui il giocatore ha accarezzato per l'ultima volta una pecora. 
	 */
	public void avviaClient(String nomeGiocatore, Date data) {
		
		InterfacciaServerRMI interfacciaServer;
		
		try {
			interfacciaServer = (InterfacciaServerRMI)Naming.lookup("//" + indirizzoIPACuiConnettersi + "/Server");	
			
			if(interfacciaServer.getIndex() <= interfacciaServer.getNumeroGiocatori()){
				ImplementazioneClientRMI implementazioneClient = new ImplementazioneClientRMI(interfacciaServer);
				InterfacciaClientRMI remoteRef = (InterfacciaClientRMI) UnicastRemoteObject.exportObject(implementazioneClient, 0);
				interfacciaServer.aggiungiClient(remoteRef, nomeGiocatore, data);
			}
		} catch (MalformedURLException e) {
			LOGGER.log(Level.SEVERE, "URL non valido", e);
		} catch (RemoteException e) {
			LOGGER.log(Level.SEVERE, "connessione fallita!", e); 
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JOptionPane.showMessageDialog(frame,"Il Server non è stato trovato","Errore",JOptionPane.ERROR_MESSAGE);
			frame.dispose();
			
		} catch (NotBoundException e) {
			LOGGER.log(Level.SEVERE, "Mancanza associazione!", e);
		}
	}
}
