package rete;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerRMI {
	
	private int numeroGiocatori;

	private static final Logger LOGGER = Logger.getLogger("serverRMI"); 
	
	private String indirizzoIPServer;
	
	/**
	 * è il costruttore: si limita a settare l'attributo "numeroGiocatori" con il parametro che gli viene passato.
	 * @param numeroGiocatori: è il numero di giocatori che parteciperà alla partita.
	 */
	public ServerRMI(int numeroGiocatori){
		this.numeroGiocatori = numeroGiocatori;
	}
	
	/**
	 * crea un nuovo registry e una implementazione server
	 * @param nomeGiocatore: è il nome del giocatore che farà da server nella partita
	 * @param data: è la data in cui il giocatore ha accarezzato per l'ultima volta una pecora. 
	 */
	public void avviaServer(String nomeGiocatore, Date data){
		try {
			LocateRegistry.createRegistry(1099);
		} catch (RemoteException e) {
			LOGGER.log(Level.SEVERE, "Registry già presente!", e);
		}		
		
		try {
			ImplementazioneServerRMI implementazioneServer = new ImplementazioneServerRMI(numeroGiocatori);		
			Naming.rebind("//localhost/Server", implementazioneServer);
			
			try {
				indirizzoIPServer = Inet4Address.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				LOGGER.log(Level.SEVERE, "Errore nel trovare il proprio indirizzo IP", e);
			}
			ClientRMI clientRMI = new ClientRMI(indirizzoIPServer);
			clientRMI.avviaClient(nomeGiocatore, data);
			
		} catch (MalformedURLException e) {
			LOGGER.log(Level.SEVERE, "Impossibile registrare l'oggetto indicato!", e);
		} catch (RemoteException e) {
			LOGGER.log(Level.SEVERE, "Errore di connessione!", e); 
		}
	}

}