package rete;

import java.rmi.Remote;
import java.rmi.RemoteException;

import eventi.EventoGenerico;

public interface InterfacciaClientRMI extends Remote {
	
	void setMioIndex(int mioIndex) throws RemoteException;
	
	int getMioIndex() throws RemoteException;
	
	void mostraSchermata(int index) throws RemoteException;
	
	void interpretaEvento(EventoGenerico evento) throws RemoteException; 
	
}
