package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contiene tutte le variabili che servono per l'astrazione del giocatore fisico 
 * ed i metodi get e set relativi a queste.
 */
public class Giocatore {
	/**
	 * Identifica il nickname che il giocatore deciderà di inserire.
	 */
	
	private String nomeGiocatore; 
	/**
	 * Viene salvata la data in cui il giocatore ha accarezzato per l'ultima volta una pecora. 
	 * Il confronto delle date in cui i giocatori hanno accarezzato per l'ultima volta una 
	 * pecora ci serve per decretare quale sarà il primo giocatore.
	 */
	
	private Date ultimaCarezza; 
	
	/**
	 * Indica il punteggio totalizzato dal giocatore che viene settato al termine della partita 
	 * quando verranno calcolati i punteggi. Servirà per decretare il vincitore della partita.
	 */
	private int punteggioTotalizzato = 0; 
	
	/**
	 * Indica i soldi che il giocatore possiede.
	 */
	private int danari; 
	
	/**
	 * Identifica il posizionamento della pedina del giocatore sulla mappa.
	 */
	private Strada posizioneGiocatore;
	
	/**
	 * Viene settata solo nel caso in cui la partita si svolge tra due 
	 * giocatori. In questo caso ogni giocatore posiziona sulla mappa esattamente due pedine, questa variabile
	 * indica la posizione della seconda pedina.
	 */
	private Strada posizione2Giocatore; 
	
	/**
	 * Rappresenta il numero di tessere, divise per tipo, che il giocatore possiede.
	 * In particolare, l'indice della lista corrisponde ad un tipo di terreno individuato dall'HashMap tipiDiTerreno, 
	 * ed il numero associato all'indice rappresenta quindi il numero di tessere terreno di quel tipo.
	 */
	private List<Integer> tessereTerrenoInPossesso = new ArrayList<Integer>(); 
	
	/**
	 * Costruttore della classe Giocatore.
	 * @param nickname che avrà il giocatore.
	 * @param data in cui ha accerezzato l'ultima volta una pecora.
	 * @param soldi che il giocatore possiede, settati a 20 se i giocatori sono più di due e 30 se i giocatori sono esattamente 2.
	 */
	public Giocatore (String nomeGiocatore, Date ultimaCarezza, int numeroGiocatori) {
		this.nomeGiocatore=nomeGiocatore; 
		this.ultimaCarezza=ultimaCarezza; 
		for(int i = 0; i < 6; i++) {
			tessereTerrenoInPossesso.add(0); 
		}
		if (numeroGiocatori ==2 ) {
			this.danari = 30;  
		} else {
			this.danari = 20; 
		}
	}
	
	/**
	 * @return nomeGiocatore: il nome del giocatore.
	 */
	public String getNomeGiocatore() {
		return this.nomeGiocatore; 
	}
	
	/**
	 * @return ultimaCarezza: la data in cui ha accerezzato l'ultima volta una pecora.
	 */
	public Date getUltimaCarezza() {
		return this.ultimaCarezza; 
	}
	
	/**
	 * Setta l'attributo punteggioTotalizzato.
	 * @param punteggioTotalizzato: il punteggio totalizzato dal giocatore.
	 */
	public void setPunteggioTotalizzato(int punteggio) {
		this.punteggioTotalizzato=punteggio; 
	}
	
	/**
	 * @return punteggioTotalizzato: il punteggio totalizzato dal giocatore.
	 */
	public int getPunteggioTotalizzato() {
		return this.punteggioTotalizzato; 
	}
	
	/**
	 * Setta il valore dei danari, per incrementare o decrementare i danari del gicatore.
	 * @param danari: soldi che possiede il giocatore.
	 */
	public void setDanari(int danari) {
		this.danari=danari;
	}
	
	/**
	 * @return danari: la quantità di danari posseduti dal giocatore.
	 */
	public int getDanari() {
		return this.danari; 
	}
	
	/**
	 * Setta la posizione della pedina del giocatore.
	 * @param posizioneGiocatore: strada in cui viene posizionata la pedina.
	 */
	public void setPosizioneGiocatore(Strada posizione) {
		this.posizioneGiocatore = posizione; 
	}
	
	/**
	 * @return posizioneGiocatore: la strada in cui è posizionata la pedina.
	 */
	public Strada getPosizioneGiocatore() {
		return this.posizioneGiocatore; 
	}
	
	/**
	 * Setta la posizione della seconda pedina del giocatore nel caso di partita a due giocatori.
	 * @param posizione2Giocatore: strada in cui viene posizionata la seconda pedina.
	 */
	public void setPosizione2Giocatore(Strada posizione) {
		this.posizione2Giocatore = posizione; 
	}
	
	/**
	 * @return posizione2Giocatore: la strada in cui è posizionata la seconda pedina nel caso di partita a due giocatori.
	 */
	public Strada getPosizione2Giocatore() {
		return this.posizione2Giocatore; 
	}
	
	/**
	 * @return tessereTerrenoInPossesso: la lista contenente il numero di tessere terreno per ogni tipo di terreno.
	 */
	public List<Integer> getTessereTerrenoInPossesso() {
		return this.tessereTerrenoInPossesso; 
	}
	
	/**
	 * Aggiunge una tessera terreno a quelle già possedute.
	 * @param tipo di terreno acquistato.
	 */
	public void addTesseraTerrenoInPossesso(int tipologia) {
		this.tessereTerrenoInPossesso.set(tipologia, this.tessereTerrenoInPossesso.get(tipologia) + 1); 
	}

}
