package model;
/**
 * questa classe contiene tutte le informazioni riguardanti il turno
 */
public class Turno {
	
	/**
	 * giocatore che sta eseguendo il proprio turno
	 */
	private Giocatore giocatore; 
	
	/**
	 * posizione del giocatore nel turno
	 */
	private Strada posizioneGiocatore;
	
	/**
	 * informazione relativa alla necessità o meno di selezionare una posizione per il turno
	 */
	private boolean selezioneStradaNecessaria = false; 
	
	/**
	 * risultato del lancio del dado effettuato a inizio turno
	 */
	private int valoreLancioDado; 
	
	/**
	 * quando viene eseguita MUOVI_PEDINA viene settato a true
	 */
	private boolean pastoreMosso = false; 
	
	/**
	 * quando viene eseguita MUOVI_ANIMALE viene settato a true; se è true, non può essere effettuata
	 * la mossa MUOVI_ANIMALE; viene settato a false quando viene poi spostato il pastore o quando viene resettato il turno
	 */
	private boolean animaleMosso = false; 
	
	/**
	 * quando viene eseguita ACQUISTA_TESSERA_TERRENO viene settato a true; se è true, non può essere effettuata
	 * la mossa ACQUISTA_TESSERA_TERRENO; viene settato a false quando viene poi spostato il pastore o quando viene resettato il turno
	 */
	private boolean terrenoAcquistato = false; 

	/**
	 * quando viene eseguita EFFETTUA_ACCOPPIAMENTO viene settato a true; se è true, non può essere effettuata
	 * la mossa EFFETTUA_ACCOPPIAMENTO; viene settato a false quando viene poi spostato il pastore o quando viene resettato il turno
	 */
	private boolean accoppiamentoEffettuato = false; 
	
	/**
	 * quando viene eseguita EFFETTUA_SPARATORIA viene settato a true; se è true, non può essere effettuata
	 * la mossa EFFETTUA_SPARATORIA; viene settato a false quando viene poi spostato il pastore o quando viene resettato il turno
	 */
	private boolean sparatoriaEffettuata = false; 
	
	/**
	 * indica il numero di mosse ancora disponibili nel turno
	 */
	private int numeroMosseRimanenti = 3;
	
	/**
	 * costruttore: crea un nuovo turno
	 * @param giocatore: è il giocatore che deve eseguire il proprio turno
	 */
	public Turno(Giocatore giocatore) {
		this.giocatore=giocatore; 
	}
	
	/**
	 * Setta il giocatore del turno
	 * @param giocatore
	 */
	public void setGiocatore(Giocatore giocatore) {
		this.giocatore = giocatore; 
	}
	
	/**
	 * @return il giocatore del turno
	 */
	public Giocatore getGiocatore() {
		return this.giocatore; 
	}
	
	/**
	 * @return la posizione del giocatore del turno
	 */
	public Strada getPosizioneGiocatore() {
		return this.posizioneGiocatore; 
	}
	
	/**
	 * Setta la posizione del giocatore nel turno
	 * @param posizione del giocatore
	 */
	public void setPosizioneGiocatore(Strada posizione) {
		this.posizioneGiocatore = posizione; 
	}
	
	/**
	 * @return il valore del lancio del dado
	 */
	public int getValoreLancioDado() {
		return this.valoreLancioDado; 
	}
	
	/**
	 * Setta il valore del lancio del dado
	 * @param valore
	 */
	public void setValoreLancioDado(int valore) {
		this.valoreLancioDado = valore; 
	}
	
	public void setPastoreMosso(boolean valore) {
		this.pastoreMosso = valore; 
	}
		
	public boolean getPastoreMosso() {
		return this.pastoreMosso; 
	}
	
	public boolean getAnimaleMosso() {
		return this.animaleMosso; 
	} 
	
	public void setAnimaleMosso(boolean valore) {
		this.animaleMosso = valore; 
	}	

	public boolean getTerrenoAcquistato() {
		return this.terrenoAcquistato;  
	}
		
	public void setTerrenoAcquistato(boolean valore) {
		this.terrenoAcquistato = valore; 
	}
	
	public boolean getSparatoriaEffettuata() {
		return this.sparatoriaEffettuata; 
	}
	
	public void setSparatoriaEffettuata(boolean valore) {
		this.sparatoriaEffettuata = valore; 
	}
	
	public boolean getAccoppiamentoEffettuato() {
		return this.accoppiamentoEffettuato; 
	}
	
	public void setAccoppiamentoEffettuato(boolean valore) {
		this.accoppiamentoEffettuato = valore; 
	}

	public int getNumeroMosseRimanenti() {
		return this.numeroMosseRimanenti; 
	} 
	
	public void setNumeroMosseRimanenti(int numero) {
		this.numeroMosseRimanenti=numero; 
	}
	
	public void setSelezioneSceltaStradaNecessaria(boolean valore) {
		this.selezioneStradaNecessaria = valore;
	}
	
	public boolean getSelezioneStradaNecessaria() {
		return this.selezioneStradaNecessaria;
	}
	
	/**
	 * riporta i valori degli attributi di turno a quelli di partenza, eccezion fatta per il giocatore e per la posizione
	 * del giocatore nel turno
	 * @param giocatore: è il giocatore che deve eseguire il turno
	 */
	public void resettaValoriTurno(Giocatore giocatore) {
		this.giocatore = giocatore; 
		this.posizioneGiocatore = giocatore.getPosizioneGiocatore(); 
		this.selezioneStradaNecessaria = false; 
		this.valoreLancioDado = 0; 
		this.pastoreMosso = false; 
		this.animaleMosso = false; 
		this.terrenoAcquistato = false; 
		this.accoppiamentoEffettuato = false; 
		this.sparatoriaEffettuata = false; 
		this.numeroMosseRimanenti = 3;
		
	}
}

