package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * All'interno viene creato il grafo rappresentante la mappa di gioco partendo da un file xml nel quale
 * vengono specificati gli attributi che avrà ogni nodo del grafo ed i collegamenti tra i vari nodi.
 * I nodi sono di due tipi, Strada e Regione. I collegamenti sono creati tra Strada e Strada e tra Regione e Strada
 * E' possibile in questo modo, dato un oggetto di tipo Strada, trovare tutte le strade ad essa adiacenti e le 
 * regioni ad essa adiacenti. Data una regione è possibile trovare tutte le strade ad essa adiacenti. 
 */
public class Mappa {
	//Creazione grafo SimpleGraph usando interfaccia UndirectedGraph
	/**
	 * Grafo che contiene tutti i vertici di tipo Regione e Strada e tutti i collegamenti tra essi.
	 */
	UndirectedGraph<VerticeGrafo, DefaultEdge> grafo = new SimpleGraph<VerticeGrafo, DefaultEdge>(DefaultEdge.class); 
	
	//Creazione HashMap in cui tengo i riferimenti tra l'identificatore delle regioni e delle strade dell'xml e le istanze regioni e le istanze strade
	/**
	 * HashMap che contiene tutte le corrispondenze tra le stringhe usate nell'xml per descrivere le strade e le regioni ed i loro collegamenti.
	 * Viene usata per la creazione del grafo a partire dai file .xml
	 */
	Map<String, VerticeGrafo> mappaVertici = new HashMap<String, VerticeGrafo>();
	
	Map<VerticeGrafo, String> mappaVerticiInversa = new HashMap<VerticeGrafo, String>(); 
	
	private static final Logger LOGGER = Logger.getLogger("model"); 
	
	/**
	 * Costruttore della classe Mappa, non riceve parametri ed inizializza il grafo prendendo informazioni da quattro file xml.
	 * Due relativi ad i vertici, rispettivamente uno per i vertici di tipo Regione ed uno per i vertici di tipo Strada.
	 * Uno relativo ai collegamenti tra vertici di tipo Strada con vertici di tipo Strada.
	 * Uno relativo ai collegamenti tra i vertici di tipo Regione ed i vertici di tipo Strada.
	 */
	public Mappa() {	
		
		// Creo e inserisco vertici di tipo Regione
		try {
			//accedo al file Regione.xml per farne il parsing.
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/Regioni.xml")); 
			document.getDocumentElement().normalize(); 
			//accedo a tag "Regione"
			NodeList regioni = document.getElementsByTagName("Regione"); 
			/*ciclo sulla NodeList: per ogni tag Regione, prendo l'attributo "tipoDiTerreno" e lo passo al costruttore delle regioni per istanziare 
			 * l'oggetto dato il tipoDiTerreno. In seguito lo aggiungo al grafo come vertice. Prelevo il campo id (univoco) di ogni regione e lo 
			 * metto in una HashMap che permette di risalire all'istanza creata della regione con quell'id. Mi servirà per creare i collegamenti. 
			 * tra i vertici del grafo.*/
			for (int i=0; i<regioni.getLength(); i++) {
				Node nodo = regioni.item(i);
				Element element = (Element) nodo; 
				int tipoDiTerreno = Integer.parseInt(element.getElementsByTagName("tipoDiTerreno").item(0).getTextContent());
				//creazione istanze regione da lettura xml
				Regione regione = new Regione(tipoDiTerreno); 
				if(tipoDiTerreno == 6) {
					regione.setTotalePecoreBianche(0);
					regione.setTotaleArieti(0);
				}
				//aggiunta regioni ai vertici del grafo
				grafo.addVertex(regione);  
				String id = element.getAttribute("id");
				mappaVertici.put(id, regione); 
				mappaVerticiInversa.put(regione, id); 
				}
			} catch(Exception e) {
				LOGGER.log(Level.SEVERE, "errore nel caricamento da xml", e);
			}
			
		// Creo e inserisco i vertici di tipo Strada
		try {
			//accedo al file Strada.xml per farne il parsing
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/Strade.xml")); 
			document.getDocumentElement().normalize();
			//accedo a tag "Strada"
			NodeList strade = document.getElementsByTagName("Strada"); 
			/*ciclo sulla NodeList: per ogni tag Strada, prendo l'attributo "numero" e lo passo al costruttore delle strade per istanziare 
			 * l'oggetto dato il numero della strada. In seguito lo aggiungo al grafo come vertice. Prelevo il campo id (univoco) di ogni strada e lo 
			 * metto in una HashMap che permette di risalire all'istanza creata della strada con quell'id. Mi servirà per creare i collegamenti 
			 * tra i vertici del grafo*/
			for(int i=0; i<strade.getLength(); i++) {
				Node nodo = strade.item(i); 
				Element element = (Element) nodo; 
				int numero = Integer.parseInt(element.getElementsByTagName("numero").item(0).getTextContent());
				int coordinataX = Integer.parseInt(element.getElementsByTagName("coordinatax").item(0).getTextContent()); 
				int coordinataY = Integer.parseInt(element.getElementsByTagName("coordinatay").item(0).getTextContent()); 
				//creazione istanze strada da lettura xml
				Strada strada = new Strada(numero, coordinataX, coordinataY); 
				//aggiunta strade ai vertici del grafo
				grafo.addVertex(strada); 
				String id =  element.getAttribute("id"); 
				mappaVertici.put(id, strada);
				mappaVerticiInversa.put(strada, id); 
				}
			} catch(Exception e) {
				LOGGER.log(Level.SEVERE, "errore nel caricamento da xml", e);
			}
		
		// creo e inserisco le connessioni tra strade
		try {
			//accedo al file ConnessioniTraStrade.xml per farne il parsing
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder= documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/ConnessioniTraStrade.xml")); 
			document.getDocumentElement().normalize(); 
			//accedo a tag "connessione"
			NodeList connessioniTraStrade = document.getElementsByTagName("connessione"); 
			/*ciclo la NodeList ed assegno le due stringhe contenute all'interno dei tag "el", ovvero i due id, a due variabili.
			 * cerco le due stringhe nella HashMap e prelevo le istanze relative al loro id. In seguito inserisco un collegamento tra
			 * le due istanze al grafo*/
			for (int i=0; i<connessioniTraStrade.getLength(); i++) {
				Node edge = connessioniTraStrade.item(i);
				Element element = (Element) edge;  
				String el0 = element.getElementsByTagName("el").item(0).getFirstChild().getTextContent();  
				String el1 = element.getElementsByTagName("el").item(1).getFirstChild().getTextContent();
				//Ricerca istanze strada all'interno della HashMap
				Strada strada0 = (Strada) mappaVertici.get(el0); 
				Strada strada1 = (Strada) mappaVertici.get(el1); 
				//aggiunta collegamento
				grafo.addEdge(strada0, strada1); 
				}
			} catch(Exception e) {
				LOGGER.log(Level.SEVERE, "errore nel caricamento da xml", e);
			}
		
		// creo e inserisco le connessioni tra regione e strada come ConnessioniTraStrade
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = documentFactory.newDocumentBuilder(); 
			Document document = builder.parse(this.getClass().getResourceAsStream("/ConnessioniRegioniStrade.xml")); 
			document.getDocumentElement().normalize(); 
			NodeList connessioniRegioniStrade = document.getElementsByTagName("connessione"); 	
			for (int i=0; i<connessioniRegioniStrade.getLength(); i++) {
				Node edge = connessioniRegioniStrade.item(i); 
				Element element = (Element) edge; 
				String el0 = element.getElementsByTagName("el").item(0).getFirstChild().getTextContent();
				String el1 = element.getElementsByTagName("el").item(1).getFirstChild().getTextContent();
				//Ricerca istanza regione e strada all'interno della HashMap
				Regione regione = (Regione) mappaVertici.get(el0); 
				Strada strada = (Strada) mappaVertici.get(el1);
				//Aggiunta collegamento
				grafo.addEdge(regione, strada);
				}
			} catch(Exception e) {
				LOGGER.log(Level.SEVERE, "errore nel caricamento da xml", e);
			}
		}
		
		/**
		 * Permette di trovare le strade adiacenti ad una strada
		 * @param la strada della quale si vogliono sapere quali sono le strade adiacenti
		 * @return stradeAdiacenti: una lista contenente le strade adiacenti alla strada passata
		 */
		public List<Strada> getStradeAdiacentiAStrada(Strada strada) {
			List<Strada> stradeAdiacenti = new ArrayList<Strada>(); 
			Set<DefaultEdge> edges = grafo.edgesOf(strada);
			Iterator<DefaultEdge> iterator = edges.iterator();
			while(iterator.hasNext()) {
				DefaultEdge edge = iterator.next();
				VerticeGrafo elemento1 = grafo.getEdgeSource(edge); 
				Strada elemento2 = (Strada) grafo.getEdgeTarget(edge); 
				if(Strada.class.isInstance(elemento1) && !elemento1.equals(strada)) {
					stradeAdiacenti.add((Strada) elemento1); 
					}
				if(!elemento2.equals(strada)) {
					stradeAdiacenti.add(elemento2); 
				}
			}
			return stradeAdiacenti; 
		}
		
		/**
		 * Permette di trovare le regioni adiacenti ad una strada
		 * @param la strada della quale si vogliono sapere quali sono le regioni adiacenti
		 * @return regioniAdiacenti: una lista contenente le regioni adiacenti alla strada passata
		 */
		public List<Regione> getRegioniAdiacentiAStrada (Strada strada) {
			List<Regione> regioniAdiacenti = new ArrayList<Regione>(); 
			Set<DefaultEdge> edges = grafo.edgesOf(strada); 
			Iterator<DefaultEdge> iterator = edges.iterator(); 
			while(iterator.hasNext()) {
				DefaultEdge edge = iterator.next(); 
				VerticeGrafo elemento1 = grafo.getEdgeSource(edge);  
				if(Regione.class.isInstance(elemento1)) {
					regioniAdiacenti.add((Regione)elemento1); 
				}
			}
			return regioniAdiacenti; 
		}
		
		/**
		 * Permette di trovare le strade adiacenti ad una data regione
		 * @param la regione della quale si vogliono sapere le strade adiacenti
		 * @return stradeAdiacentiARegione: una lista contenente le strade adiacenti alla regione passata
		 */
		public List<Strada> getStradeAdiacentiARegione (Regione regione) {
			List<Strada> stradeAdiacentiARegione = new ArrayList<Strada>(); 
			Set<DefaultEdge> edges = grafo.edgesOf(regione); 
			Iterator<DefaultEdge> iterator = edges.iterator(); 
			while(iterator.hasNext()) {
				DefaultEdge edge = iterator.next(); 
				Strada strada = (Strada) grafo.getEdgeTarget(edge);
				stradeAdiacentiARegione.add(strada); 
			}
			return stradeAdiacentiARegione; 
		}
		
		/**
		 * @return regioni: tutti i vertici di tipo Regione
		 */
		public List<Regione> getRegioniMappa() {
			List<Regione> regioni = new ArrayList<Regione>(); 
			Set<VerticeGrafo> vertici = grafo.vertexSet();
			Iterator<VerticeGrafo> iterator = vertici.iterator(); 
			while (iterator.hasNext()) {
				VerticeGrafo vertice = iterator.next(); 
				if(vertice instanceof Regione) {
					regioni.add((Regione) vertice); 
				}
			}
			return regioni; 
		}
		
		/**
		 * Restituisce tutti i vertici di tipo Strada
		 */
		public List<Strada> getStradeMappa() {
			List<Strada> strade = new ArrayList<Strada>(); 
			Set<VerticeGrafo> vertici = grafo.vertexSet();
			Iterator<VerticeGrafo> iterator = vertici.iterator(); 
			while (iterator.hasNext()) {
				VerticeGrafo vertice = iterator.next(); 
				if(vertice instanceof Strada) {
					strade.add((Strada) vertice); 
				}
			}
			return strade; 
		}
		
		/**
		 * @return grafo: il grafo completo
		 */
		public UndirectedGraph<VerticeGrafo, DefaultEdge> getGrafo() {
			return this.grafo; 
		}
		
		/**
		 * @return mappaVertici: la mappatura dei vertici con id-istanza
		 */
		public Map<String, VerticeGrafo> getMappaVertici() {
			return this.mappaVertici; 
		}
		
		/**
		 * @return mappaVerticiInversa: la mappatura dei vertici con istanza-id
		 */
		public Map<VerticeGrafo, String> getMappaVerticiInversa() {
			return this.mappaVerticiInversa; 
		}
		
}
