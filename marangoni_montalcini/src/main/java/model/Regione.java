package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe che rappresenta una Regione della plancia di gioco
 */
public class Regione implements VerticeGrafo{
	
	/**
	 * Indica il di che tipo è la regione. Il tipo è un intero poichè si usa una HashMap che permette di associare la stringa relativa
	 * al tipo di terreno ad un numero.
	 */
	private final int tipoDiTerreno; 
	
	/**
	 * Indica il numero delle  pecore presenti nella regione.
	 */
	private int totalePecoreBianche = 1;  
	
	/**
	 * Indica il numero degli arieti presenti nella regione.
	 */
	private int totaleArieti = 1; 
	
	/**
	 * Indica il numero di agnelli presenti nella regione.
	 */
	private int totaleAgnelli = 0;
	
	/**
	 * è composto da integer che dicono quanti turni mancano prima che l'agnello si trasformi
	 */
	private List<Integer> contatoriVitaAgnello = new ArrayList<Integer>();
	
	/**
	 * Costruttore della classe Regione.
	 * @param il tipo relativo alla regione da creare.
	 */
	public Regione(int tipoDiTerreno) {
		this.tipoDiTerreno=tipoDiTerreno; 
	}
	
	/**
	 * @return tipoDiTerreno: il tipo di terreno relativo ad una istanza della classe Regione.
	 */
	public int getTipoDiTerreno() {
		return this.tipoDiTerreno; 
	}
	
	/**
	 * @return totalePecoreBianche: il numero totale di pecore presenti nella regione esclusa la pecora nera.
	 */
	public int getTotalePecoreBianche() {
		return this.totalePecoreBianche; 
	}
	
	/**
	 * Setta il numero di pecore nella regione. Serve per incrementarle o decrementarle.
	 * @param totalePecoreBianche: il numero che di pecore che alla fine dell'operazione la regione dovrà
	 * avere.
	 */
	public void setTotalePecoreBianche(int totalePecoreBianche) {
		this.totalePecoreBianche=totalePecoreBianche; 
	}
	
	/**
	 * @return totaleArieti: il numero totale degli arieti presenti nella regione.
	 */
	public int getTotaleArieti() {
		return this.totaleArieti; 
	}
	
	/**
	 * Setta il numero degli arieti nella regione. Serve per incrementarli o decrementarli.
	 * @param totaleArieti: il numero di arieti che alla fine dell'operazione la regione dovrà avere.
	 */
	public void setTotaleArieti(int totaleArieti) {
		this.totaleArieti = totaleArieti; 
	}
	
	/**
	 * @return totaleAgnelli: il numero totale degli agnelli presenti nella regione.
	 */
	public int getTotaleAgnelli() {
		return this.totaleAgnelli; 
	}
	
	/**
	 * Setta il numero degli agnelli nella regione. Serve per incrementarli o decrementarli.
	 * @param totaleAgnelli: il numero totale degli agnelli presenti nella regione.
	 */
	public void setTotaleAgnelli(int totaleAgnelli) {
		this.totaleAgnelli = totaleAgnelli; 
	}
	
	/**
	 * @return contatoriVitaAgnello: la lista di contatori che contiene l'indicazione sui turni che mancano 
	 * affinchè un agnello si trasformi
	 */
	public List<Integer> getContatoriVitaAgnello() {
		return this.contatoriVitaAgnello; 
	}

	@Override
	public String toString() {
		return "Regione [tipoDiTerreno=" + tipoDiTerreno + ", totalePecore="
				+ totalePecoreBianche + ", totaleArieti=" + totaleArieti
				+ ", totaleAgnelli=" + totaleAgnelli + "]\n";
	}

}
