package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

/**
 * Classe che gestisce lo stato della partita
 */
public class StatoPartita extends Observable{
	
	/**
	 * Costante che rappresenta il massimo numero di giocatori che possono giocare ad una stessa partita.
	 */
	private static final int MAX_NUM_GIOCATORI=4;
	
	/**
	 * Indica il costo massimo delle tessere terreno. Il costo va da 0 a 4.
	 */
	private static final int MAX_COSTO_TESSERA=4; 
	
	/**
	 * Costante che rappresenta il numero dei recinti iniziali, dopo i quali bisogna cominciare ad usare i recinti finali.
	 */
	private static final int NUMERO_RECINTI_INIZIALI=20; 
	
	/**
	 * Oggetto di tipo Mappa che quindi contiene l'intero grafo del gioco.
	 */
	private Mappa mappa;
	
	/**
	 * HashMap che tiene l'associazione tra tipo di terreno (in formato stringa) ed un numero.
	 */
	Map<String, Integer> tipiDiTerreno = new HashMap<String, Integer>();
	
	/**
	 * ArrayList che contiene il riferimento alle istanze Giocatore che partecipano alla partita
	 */
	private List<Giocatore> listaGiocatori = new ArrayList<Giocatore>();
	
	/**
	 * Identifica il primo giocatore
	 */
	private Giocatore primoGiocatore;
	
	/**
	 * Numero giocatori che partecipano alla partita.
	 */
	private int numeroGiocatori;
	
	/**
	 * Variabile di tipo Turno che tiene conto di tutto ciò che avviene durante il turno di un giocatore.
	 */
	private Turno turno; 
	
	/**
	 * Lista di interi che tiene in memoria il costo delle tessere terreno in base al tipo. Si utilizza l'indice 
	 * grazie all'associazione data dall'HashMap tipiDiTerreno.
	 */
	private List<Integer> costoTessereTerreno;
	
	/**
	 * Variabile che tiene conto dei recinti utilizzati. Arrivati a 20 recinti,
	 * comincia il giro finale in cui si utilizzano i recinti finali fino al primo giocatore
	 * con cui termina la partita. 
	 */
	private int recintiUtilizzati = 0;
	
	/**
	 * Contiene il riferimento alla regione in cui si trova la pecora nera.
	 */
	private Regione posizionePecoraNera; 
	
	/**
	 * Contiene il riferimento alla regione in cui si trova il lupo.
	 */
	private Regione posizioneLupo; 
	
	/**
	 * Lista contenente le regioni in cui si trovano gli agnelli.
	 */
	private List<Regione> posizioniAgnelli = new ArrayList<Regione>(); 
	
	
	/**
	 * Lista contenente le date relative ai giocatori della partita nello stesso ordine dell'array di nomi.
	 */
	private List<Date> dateCarezze = new ArrayList<Date>(); 
	
	/**
	 * HashMap contenente la corrispondenza tra indice giocatore e giocatore. Viene usato nella modalità di gioco remota.
	 */
	Map<Integer, Giocatore> giocatoriOnline = new HashMap<Integer, Giocatore>(); 
	
	/**
	 * Stringa identificativa della regione Sheepsburg.
	 */
	private static final String SHEEPSBURG = "Sheepsburg"; 
	
	/**
	 * Costruttore di StatoPartita, all'interno viene creata la mappa viene inizializzata la posizione della pecora nera e 
	 * del lupo a Sheepsburg, viene inizializzato la lista del costo delle tessere terreno a tutti 0, vengono inizializzati i 
	 * tipi di terreno.
	 * Viene utilizzato per il gioco in locale, in cui sappiamo sia il numero dei giocatori che le date di tutti i giocatori.
	 * @param il numero di giocatori che partecipa alla partita
	 * @param la lista delle date relative ai giocatori
	 */
	public StatoPartita(int numeroGiocatori, List<Date> dateCarezze) {
		this.dateCarezze = dateCarezze; 
		this.mappa = new Mappa(); 
		this.numeroGiocatori = numeroGiocatori; 
		this.costoTessereTerreno = new ArrayList<Integer>(); 
		this.posizionePecoraNera = (Regione) mappa.mappaVertici.get(SHEEPSBURG); 
		this.posizioneLupo = (Regione) mappa.mappaVertici.get(SHEEPSBURG);
		// inizializzo i tipi di terreno
		tipiDiTerreno.put("Pianura", 0);
        tipiDiTerreno.put("Montagna", 1);
        tipiDiTerreno.put("Campi", 2);
        tipiDiTerreno.put("Palude", 3);
        tipiDiTerreno.put("Steppa", 4);
        tipiDiTerreno.put("Bosco", 5);
        tipiDiTerreno.put(SHEEPSBURG, 6);
	}
	
	/**
	 * Costruttore di StatoPartita usato per il gioco online.
	 * @param numero dei giocatori che parteciperanno alla partita
	 */
	public StatoPartita(int numeroGiocatori) {
		this.mappa = new Mappa(); 
		this.numeroGiocatori = numeroGiocatori; 
		this.costoTessereTerreno = new ArrayList<Integer>(); 
		this.posizionePecoraNera = (Regione) mappa.mappaVertici.get(SHEEPSBURG); 
		this.posizioneLupo = (Regione) mappa.mappaVertici.get(SHEEPSBURG);
		// inizializzo i tipi di terreno
		tipiDiTerreno.put("Pianura", 0);
        tipiDiTerreno.put("Montagna", 1);
        tipiDiTerreno.put("Campi", 2);
        tipiDiTerreno.put("Palude", 3);
        tipiDiTerreno.put("Steppa", 4);
        tipiDiTerreno.put("Bosco", 5);
        tipiDiTerreno.put(SHEEPSBURG, 6);
	}
	
	/**
	 * Aggiunge i giocatori alla lista di giocatori
	 * @param lista dei nomi dei giocatori
	 */
	public void aggiungiGiocatori(List<String> nomiGiocatori) {
		for (int i = 0; i < nomiGiocatori.size(); i++) {
			listaGiocatori.add(new Giocatore(nomiGiocatori.get(i), dateCarezze.get(i), numeroGiocatori)); 
		}
		
	}
	
	/**
	 * Inizializza i costi delle tessere terreno a 0
	 */
	public void inizializzaCostiTessereTerreno() {
		for (int i = 0; i<6; i++) {
			costoTessereTerreno.add(0); 
		}
	}
	

	/**
	 * @return la mappa dello stato partita attuale.
	 */
	public  Mappa getMappa() {
		return this.mappa; 
	}

    /**
     * @return HashMap con associazione tra tipo di terreno in formato stringa e un numero.
     */
    public Map<String, Integer> getTipiDiTerreno() {
    	return tipiDiTerreno; 
    }
    
	/**
	 * Aggiunge un nuovo giocatore alla lista dei giocatori.
	 * @param giocatore
	 */
	public void addGiocatore(Giocatore giocatore) {
		this.listaGiocatori.add(giocatore); 
	}
	
	/**
	 * @return la lista dei giocatori partecipanti alla partita.
	 */
	public List<Giocatore> getGiocatori() {
		return this.listaGiocatori; 
	}
	
	/**
	 * Setta il giocatore che per primo eseguirà il turno.
	 * @param primoGiocatore: primoGiocatore
	 */
	public void setPrimoGiocatore(Giocatore primoGiocatore) {
		this.primoGiocatore=primoGiocatore; 
	}
	
	/**
	 * @return il giocatore che ha effettuato per primo il turno.
	 */
	public Giocatore getPrimoGiocatore(){
		return this.primoGiocatore; 
	}
	
	/**
	 * @return il numero di giocatori partecipanti alla partita.
	 */
	public int getNumeroGiocatori() {
		return this.numeroGiocatori; 
	}
	
	/**
	 * Setta il costo attuale delle tessere terreno di un certo tipo.
	 * @param tipologia delle tessere terreno al quale si vuole incrementare il costo.
	 * @param costo nuovo costo da assegnare.
	 */
	public void setCostoTesseraTerreno(int tipologia, int costo) {
		this.costoTessereTerreno.set(tipologia, costo);
	}
	
	/**
	 * @param tipologia: tipologia del terreno di cui si vuol sapere il costo.
	 * @return costo relativo al tipo di terreno passato.
	 */
	public int getCostoTesseraTerreno(int tipologia) {
		return this.costoTessereTerreno.get(tipologia);
	}
	
	/**
	 * @return il turno in corso
	 */
	public Turno getTurno() {
		return this.turno; 
	}
	
	/**
	 * Setta il nuovo turno nello statoPartita
	 * @param turno: è il nuovo turno in corso
	 */
	public void setTurno(Turno turno) {
		this.turno = turno; 
	}
	
	/**
	 * Setta il numero di recinti utilizzati. Permette di incrementare il numero di recinti utilizzati.
	 * @param recintiUtilizzati numero di recinti fino ad ora utilizzati.
	 */
	public void setNumeroRecintiUtilizzati(int recintiUtilizzati) {
		this.recintiUtilizzati=recintiUtilizzati; 
	}
	
	/**
	 * @return numero di recinti utilizzati.
	 */
	public int getNumeroRecintiUtilizzati() {
		return this.recintiUtilizzati; 
	}
	
	/**
	 * @return il numero massimo di giocatori che possono partecipare ad una partita.
	 */
	public int getMaxNumGiocatori() {
		return MAX_NUM_GIOCATORI; 
	}
	
	/**
	 * @return il numero relativo alla quantità di recinti iniziali disponibili nel gioco.
	 */
	public int getNumRecintiIniziali() {
		return NUMERO_RECINTI_INIZIALI; 
	}
	
	/**
	 * @return il costo massimo che possono assumere le tessere terreno.
	 */
	public int getMaxCostoTessera() {
		return MAX_COSTO_TESSERA; 
	}
	
	/**
	 * Settare la posizione della pecora nera in una regione.
	 * @param posizionePecoraNera: regione la regione in cui la si deve spostare.
	 */
	public void setPosizionePecoraNera(Regione regione) {
		this.posizionePecoraNera=regione; 
	}
	
	/**
	 * @return la regione in cui si trova la pecora nera.
	 */
	public Regione getPosizionePecoraNera(){
		return this.posizionePecoraNera;
	}
	
	/**
	 * Settare la posizione dal lupo in una regione.
	 * @param posizioneLupo: la regione in cui lo si vuole spostare.
	 */
	public void setPosizioneLupo(Regione regione) {
		this.posizioneLupo = regione; 
	}
	
	/**
	 * @return la posizione in cui si trova il lupo.
	 */
	public Regione getPosizioneLupo() {
		return this.posizioneLupo; 
	}
	
	/**
	 * @return una lista contenente le regioni in cui sono presenti gli agnelli
	 */
	public List<Regione> getPosizioniAgnelli() {
		return this.posizioniAgnelli; 
	}
	
	/**
	 * @return HashMap contenente la corrispondenza tra l'indice del giocatore e l'istanza del giocatore
	 */
	public Map<Integer, Giocatore> getGiocatoriOnline() {
		return giocatoriOnline;
	}
	
	/**
	 * @return la lista dei nomi dei giocatori della partita
	 */
	public List<String> getNomiGiocatori() {
		List<String> nomi = new ArrayList<String>(); 
		for (Giocatore giocatore: listaGiocatori) {
			nomi.add(giocatore.getNomeGiocatore()); 
		}
		return nomi;
	}

	/**
	 * Setta l'HashMap dei giocatori online
	 * @param HashMap relativa ai giocatori online
	 */
	public void setGiocatoriOnline(Map<Integer, Giocatore> giocatoriOnline) {
		this.giocatoriOnline = giocatoriOnline;
	}
	
}