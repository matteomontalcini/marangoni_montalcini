package model; 

/**
 * Classe che rappresenta una Strada della plancia di gioco
 */
public class Strada implements VerticeGrafo {
	
	/**
	 * Indica qual è il numero della strada
	 */
	private int numero;
	
	/**
	 * Indica se la strada è libera
	 */
	private boolean libera=true;
	
	/**
	 * Indica la coordinata x della strada rispetto all'immagine della mappa.
	 */
	private int coordinataX;
	
	/**
	 * Indica la coordinata y della strada rispetto all'immagine della mappa.
	 */
	private int coordinataY; 
	
	/**
	 * costruttore
	 * @param numero relativo alla strada
	 * @param coordinata x della strada rispetto all'immagine della mappa.
	 * @param coordinata y della strada rispetto all'immagine della mappa.
	 */
	public Strada(int numero, int coordinataX, int coordinataY) {
		this.numero=numero; 
		this.coordinataX = coordinataX - 13; 
		this.coordinataY = coordinataY - 13; 
	}

	/**
	 * @return numero: il numero relativo alla strada
	 */
	public int getNumeroStrada() {
		return this.numero; 
	}
	
	/**
	 * setta la variabile booleana 'libera' della strada a false
	 */
	public void setLiberaFalse() {
		this.libera=false; 
	}
	
	/**
	 * @return libera: true se la strada è libera, false se è occupata
	 */
	public boolean getLibera(){
		return this.libera; 
	}
	
	/**
	 * @return coordinataX: coordinata x della strada
	 */
	public int getCoordinataX() {
		return this.coordinataX; 
	}
	
	/**
	 * @return coordinataY: coordinata y della strada
	 */
	public int getCoordinataY() {
		return this.coordinataY; 
	}
	
	@Override
	public String toString() {
		return "Strada [numero=" + numero + ", libera=" + libera
				+ ", coordinataX=" + coordinataX + ", coordinataY="
				+ coordinataY + "]";
	}
	
	
}
