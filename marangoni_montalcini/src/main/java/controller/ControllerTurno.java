package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import model.Giocatore;
import model.Regione;
import model.StatoPartita;
import model.Strada;
import view.InterfacciaView;

public class ControllerTurno {
	
	private StatoPartita statoPartita; 
	
	private InterfacciaView view; 
	
	private Strada stradaScelta = null; 
	
	private boolean assegnata = false; 
	
	private boolean faseFinale = false; 
	
	private boolean sceltaPosizioneEffettuata = false; 
	
	private Regione regioneScelta; 
	
	private Integer tipoTerrenoScelto = null;  
	
	private String mossaScelta = null;  
	
	private String animaleScelto = null; 
	
	private Giocatore giocatoreCheScegliePosizione = null; 
	
	private Integer valoreLancioDado; 
	
	private int numeroStradeAncoraDaSelezionare = 0; 
	
	private static final String PECORA_BIANCA = "PECORA_BIANCA", ARIETE = "ARIETE", PECORA_NERA = "PECORA_NERA"; 
	
	/**
	 * costruttore
	 */
	public ControllerTurno(StatoPartita statoPartita, InterfacciaView view) {
		this.view = view; 
		this.statoPartita=statoPartita; 
	}

	/**
	 * esegue le operazioni necessarie per chiamare i giusti metodi della view e sistemare tutto per l'inizio
	 * del nuovo turno. Se necessario, fa scegliere al giocatore la sua posizione nel turno, altrimenti la setta
	 * automaticamente e invoca svolgimentoTurno()
	 */
	public void eseguiTurno() {
		view.setGiocatoreDelTurno(statoPartita.getTurno().getGiocatore().getNomeGiocatore());
		disattivaStradeSelezionabiliInizio(); 
		decrementaContatoriAgnelli();
		trasformazioniAgnelli();
		//se i giocatori sono due, all'inizio del turno, il giocatore deve selezionare una pedina
		if(statoPartita.getNumeroGiocatori() == 2) {
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					sceltaPosizioneTurno(); 
					
				}
			}, 500);
		} else {
			statoPartita.getTurno().setPosizioneGiocatore(statoPartita.getTurno().getGiocatore().getPosizioneGiocatore());
			svolgimentoTurno();
		}
	}
	
	/**
	 * contiene i metodi che devono essere eseguiti dopo che è stato generato il turno ed è stata scelta, 
	 * nel caso di due giocatori, la posizione del turno. 
	 * Viene effettuato il lancio del dado, vengono eseguiti gli spostamenti automatici di lupo e pecora nera
	 * e vengono mostrate le mosse consentite. 
	 */
	public void svolgimentoTurno() {
		//si resettano i valori del turno precedente e viene effettuato il lancio del dado
		resettaValori();
		effettuaLancioDado();
		statoPartita.getTurno().setValoreLancioDado(valoreLancioDado); 
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				//vengono gestiti gli spostamenti automatici del lupo e della pecora nera e vengono mostrate le mosse consentite
				spostamentoAutomaticoPecoraNera(statoPartita.getTurno().getValoreLancioDado()); 
				spostamentoAutomaticoLupo(); 
				view.setLabelComunicazioni("Seleziona una mossa da effettuare tra quelle consentite");
				mostraMosseConsentite(); 
			}
		}, 2500);
	}
	
	/**
	 * decrementa di una unità ciascun contatore di vita di un agnello
	 */
	public void decrementaContatoriAgnelli() {
		for(int i = 0; i < statoPartita.getPosizioniAgnelli().size(); i++) {
			//viene prelevato il numero di agnelli nella regione e messo in una lista
			List<Integer> contatoriAgnelli = statoPartita.getPosizioniAgnelli().get(i).getContatoriVitaAgnello(); 
			for (int j = 0; j < contatoriAgnelli.size(); j++) {
				//si decrementa il numero degli agnelli
				contatoriAgnelli.set(j, contatoriAgnelli.get(j) - 1); 
			}
		}
	}
	
	/**
	 * chiama il method mutazioneAgnello() sulle regioni in cui è necessario. Decrementa quindi il numero di agnelli
	 * presenti, aumenta il numero di pecore o arieti. 
	 */
	public void trasformazioniAgnelli() {
		List<Regione> regioniConAgnelli = statoPartita.getPosizioniAgnelli(); 
		for (int i = 0; i < regioniConAgnelli.size(); i++) {
			List<Integer> contatori = regioniConAgnelli.get(i).getContatoriVitaAgnello(); 
			for (int j = 0; j < contatori.size(); j++) {
				if(contatori.get(j) == 0) {
					mutazioneAgnello(regioniConAgnelli.get(i)); 
					contatori.remove(j);  
					statoPartita.getPosizioniAgnelli().remove(i); 
				}
			}
		}
	}
	
	/**
	 * effettua la mutazione da agnello ad ariete/pecora in maniera casuale
	 */
	public void mutazioneAgnello(Regione regione) {
		decrementaTotaleAgnelliInRegione(regione); 
		Random random = new Random();
		int valore = random.nextInt(2); 
		//se 0, agnello --> ariete; se 1, agnello --> pecora 
		if (valore == 0) {
			incrementaTotaleArietiInRegione(regione); 
			view.setLabelComunicazioni("L'agnello si è trasformato in un ariete!");
		} else {
			incrementaTotalePecoreBiancheInRegione(regione); 
			view.setLabelComunicazioni("L'agnello si è trasformato in una pecora bianca"); 
		} 
		final Regione regioneConsiderata = regione; 
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				view.setTotaleAgnelliInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regioneConsiderata), regioneConsiderata.getTotaleAgnelli());
				if(regioneConsiderata.getTotaleAgnelli()==0 && regioneConsiderata.getTotaleArieti()==0 && regioneConsiderata.getTotalePecoreBianche() == 0) {
					view.visibilityBottoniAnimali(statoPartita.getMappa().getMappaVerticiInversa().get(regioneConsiderata), false);
				}
			}
		}, 1000);
	
	}
	
	
	/**
	 * il method lanciaDado simula il lancio di un dado, restituendo un valore compreso tra 1 e 6
	 */
	public int lancioDado() { 
		Random random = new Random(); 
		return random.nextInt(6) + 1; 
	}
	
	/**
	 * chiama il method lancioDado, in modo da generare un valore compreso tra 1 e 6; poi chiama una funzione della view
	 * in modo da visualizzare a video il risultato del lancio
	 */
	public void effettuaLancioDado() {
		valoreLancioDado = lancioDado(); 
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				
				view.eseguiLancioDado(valoreLancioDado);
				
			}
		}, 500);
		
	}
	
	
	/**
	 * viene eseguito solo se il valore del lancio del dado è uguale al numero di una strada adiacente alla regione; 
	 * effettua lo spostamento della pecora nera
	 */
	public void spostamentoAutomaticoPecoraNera(int valoreLancioDado) {
		String idRegionePartenza = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizionePecoraNera()); 
		String idRegioneArrivo = null; 
		List<Strada> stradeAdiacenti = statoPartita.getMappa().getStradeAdiacentiARegione(statoPartita.getPosizionePecoraNera()); 
		for (Strada stradaAdiacente : stradeAdiacenti) {
			// al massimo solo una strada avrà come numero il valore del lancio del dado
			if(stradaAdiacente.getNumeroStrada() == valoreLancioDado && stradaAdiacente.getLibera()) {
				List<Regione> regioniAdiacentiAllaStrada = statoPartita.getMappa().getRegioniAdiacentiAStrada(stradaAdiacente); 
				// una strada ha solo due regioni adiacenti
				if(regioniAdiacentiAllaStrada.get(0) == statoPartita.getPosizionePecoraNera()) {
					statoPartita.setPosizionePecoraNera(regioniAdiacentiAllaStrada.get(1));
					idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizionePecoraNera());	
				} else {
					statoPartita.setPosizionePecoraNera(regioniAdiacentiAllaStrada.get(0));
					idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizionePecoraNera());
				}
			}
		}
		if(idRegioneArrivo != null) {
			view.setPosizionePecoraNera(idRegionePartenza, idRegioneArrivo);
		}
	}
	
	
	/** 
	 * Questo method serve per effettuare lo spostamento automatico del lupo. Se non ci sono strade adiacenti libere
	 * attraverso cui il lupo può passare, si effettua lo spostamento usando il primo valore ottenuto dal lancio; 
	 * altrimenti si continua a lanciare il dado fino ad ottenere il numero di una strada libera. 
	 * Quando il lupo arriva nella nuova regione, uccide una pecora o un ariete.
	 */
	public void spostamentoAutomaticoLupo() {
		String idRegionePartenza = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizioneLupo()); 
		String idRegioneArrivo = null; 
		valoreLancioDado = lancioDado(); 
		//si trovano le strade adiacenti alla regione
		List<Strada> stradeAdiacenti = statoPartita.getMappa().getStradeAdiacentiARegione(statoPartita.getPosizioneLupo()); 
		boolean presenzaStradaLibera = false; 
		boolean spostamentoEffettuato = false; 
		//si controlla la presenza di una strada adiacente alla regione libera
		for (int i = 0; i < stradeAdiacenti.size(); i++) {
			if(stradeAdiacenti.get(i).getLibera()) {
				presenzaStradaLibera = true; 
			}
		}
		//se c'è una strada libera
		if(presenzaStradaLibera) {
			do {
				for(Strada stradaAdiacente : stradeAdiacenti) {	
					//caso valore lancio dado == numero di una strada libera
					if(stradaAdiacente.getNumeroStrada() == valoreLancioDado && stradaAdiacente.getLibera()) {
						// lista di due elementi
						List<Regione> regioniCollegateDallaStrada = statoPartita.getMappa().getRegioniAdiacentiAStrada(stradaAdiacente); 
						//controllo qual è la posizione del lupo, lo sposto nella regione in cui non si trova
						if (regioniCollegateDallaStrada.get(0) == statoPartita.getPosizioneLupo()) {
							statoPartita.setPosizioneLupo(regioniCollegateDallaStrada.get(1));
							idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizioneLupo());
							spostamentoEffettuato = true; 
						} else {
							statoPartita.setPosizioneLupo(regioniCollegateDallaStrada.get(0));
							idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizioneLupo());
							spostamentoEffettuato = true; 
						}
					}
				}
				if (!spostamentoEffettuato) {
					valoreLancioDado = lancioDado(); 
				}
			} while (!spostamentoEffettuato);
		} else { 
			//siamo nel caso in cui non ci sono strade libere adiacenti alla regione in cui si trova il lupo, il lupo salta su una strada occupata
			do {
				for(Strada stradaAdiacente : stradeAdiacenti) {	
				if(stradaAdiacente.getNumeroStrada() == valoreLancioDado) {
					List<Regione> regioniCollegateDallaStrada = statoPartita.getMappa().getRegioniAdiacentiAStrada(stradaAdiacente); 
					if (regioniCollegateDallaStrada.get(0) == statoPartita.getPosizioneLupo()) {
						statoPartita.setPosizioneLupo(regioniCollegateDallaStrada.get(1));
						idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizioneLupo());
						spostamentoEffettuato = true; 
					} else {
						statoPartita.setPosizioneLupo(regioniCollegateDallaStrada.get(0));
						idRegioneArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getPosizioneLupo());
						spostamentoEffettuato = true; 
					}
				}
			}
		} while (!spostamentoEffettuato);
		}
		view.setPosizioneLupo(idRegionePartenza, idRegioneArrivo);
		valoreLancioDado = 0; 
		final String id = idRegioneArrivo; 
		//caso in cui ci sono sia pecore bianche che arieti il lupo mangia ariete o pecora bianca della regione in cui arriva
		if(statoPartita.getPosizioneLupo().getTotalePecoreBianche() > 0 && statoPartita.getPosizioneLupo().getTotaleArieti() > 0) {
			Random random = new Random();
			int valore = random.nextInt(2); 
			//se 0, lupo mangia ariete; se 1, lupo mangia pecora bianca
			if (valore == 0) {
				decrementaTotaleArietiInRegione(statoPartita.getPosizioneLupo());
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						view.morteAnimale(ARIETE, id);
					}
				}, 1200);
				
				} else {
					decrementaTotalePecoreBiancheInRegione(statoPartita.getPosizioneLupo());
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						
						@Override
						public void run() {
							view.morteAnimale(PECORA_BIANCA, id);
						}
					}, 1200);
					
				}
		} else if (statoPartita.getPosizioneLupo().getTotalePecoreBianche() > 0 && statoPartita.getPosizioneLupo().getTotaleArieti() == 0) {
			//caso in cui non ci sono arieti, mangia una pecora bianca
			decrementaTotalePecoreBiancheInRegione(statoPartita.getPosizioneLupo()); 
			
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					view.morteAnimale(PECORA_BIANCA, id);
				}
			}, 1200);
			
		} else if (statoPartita.getPosizioneLupo().getTotalePecoreBianche() == 0 && statoPartita.getPosizioneLupo().getTotaleArieti() > 0) {
			//caso in cui non ci sono pecore bianche, mangia un ariete
			decrementaTotaleArietiInRegione(statoPartita.getPosizioneLupo()); 
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					view.morteAnimale(ARIETE, id);
				}
			}, 1200);
		}

	}
	
	/**
	 * Dopo aver trovato quale mosse sono possibili tramite trovaMosseConsentite, chiama una funzione della view 
	 * in modo che queste mosse vengano visualizzate a video. Se non ci sono invece mosse disponibili, fa visualizzare 
	 * a video una comunicazione tramite la chiamata sempre di un method della view
	 */
	public void mostraMosseConsentite() {
		if(trovaMosseConsentite().size() > 0) {
			view.attivaBottoniMosseSelezionabili(trovaMosseConsentite());
		} else if (trovaMosseConsentite().size() == 0){
			view.setLabelComunicazioni("Fine del Turno!");
			view.avvisoMosseNonDisponibili();
			}
	}
	
	
	/**
	 * restituisce una lista di interi corrispondenti alle mosse che sono consentite
	 */
	public List<Integer> trovaMosseConsentite() {
		List<Integer> mosseConsentite = new ArrayList<Integer>(); 
		if(statoPartita.getTurno().getNumeroMosseRimanenti() == 1 && !statoPartita.getTurno().getPastoreMosso()) {
			if(checkSpostamentoPedinaConsentito()) {
				mosseConsentite.add(0); 
			}
		} else {	
			if(checkSpostamentoPedinaConsentito()) {
				mosseConsentite.add(0); 
			}
			if((!statoPartita.getTurno().getAnimaleMosso()) && (checkSpostamentoAnimaleConsentito())) {
				mosseConsentite.add(1); 
			}
			if((!statoPartita.getTurno().getTerrenoAcquistato()) && (checkAcquistoTesseraTerrenoConsentito())) {
				mosseConsentite.add(2); 
			}
			if((!statoPartita.getTurno().getSparatoriaEffettuata()) && (checkSparatoriaConsentita())) {
				mosseConsentite.add(3); 
			}
			if((!statoPartita.getTurno().getAccoppiamentoEffettuato()) && (checkAccoppiamentoConsentito())) {
				mosseConsentite.add(4); 
			} 
		}
		return mosseConsentite; 
	}	
	
	/**
	 * restituisce una lista contente gli id delle strade che sono selezionabili, cioè di quelle strade
	 * che risultano ancora libere.
	 * @return: lista contente gli id delle strade che sono selezionabili
	 */
	public List<String> mostraStradeSelezionabili() {
		List<String> idStradeSelezionabili = new ArrayList<String>(); 
		List<Strada> stradeSelezionabili = new ArrayList<Strada>(); 
		List<Strada> stradeMappa = statoPartita.getMappa().getStradeMappa(); 
		List<Strada> stradeAdiacenti = statoPartita.getMappa().getStradeAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore());
		for (Strada strada: stradeMappa) {
			//aggiunge tutte le strade libere se il giocatore ha più di 0 danari
			if(statoPartita.getTurno().getGiocatore().getDanari() > 0 && strada.getLibera()) {
				stradeSelezionabili.add(strada); 
			} else if (statoPartita.getTurno().getGiocatore().getDanari() == 0 && stradeAdiacenti.contains(strada) && strada.getLibera()) {
				//siamo nel caso in cui il giocatore non ha danari, in questo caso si può spostare solo nelle strade adiacenti alla sua
				stradeSelezionabili.add(strada); 
			}
		}
		for(Strada strada: stradeSelezionabili) {
			idStradeSelezionabili.add(statoPartita.getMappa().getMappaVerticiInversa().get(strada)); 
		}
		//restituisce un array contenente gli id delle strade selezionabili
		return idStradeSelezionabili; 
	}
	
	/**
	 * Dopo aver trovato quali strade sono ancora selezionabili, invoca un method della view in modo che 
	 * non siano più visualizzati a video i bottoni di queste strade
	 */
	public void disattivaStradeSelezionabiliInizio() {
		List<String> idStradeSelezionabili = new ArrayList<String>(); 
		List<Strada> stradeSelezionabili = new ArrayList<Strada>(); 
		List<Strada> stradeMappa = statoPartita.getMappa().getStradeMappa(); 
		for (Strada strada: stradeMappa) {
			if(strada.getLibera()) {
				stradeSelezionabili.add(strada); 
			}
		}
		for(Strada strada: stradeSelezionabili) {
			idStradeSelezionabili.add(statoPartita.getMappa().getMappaVerticiInversa().get(strada)); 
		}
		view.disattivaBottoniStradeSelezionabili(idStradeSelezionabili); 
	}
		
	/**
	 * Dopo aver trovato quali strade sono ancora selezionabili, invoca un method della view in modo che 
	 * siano più visualizzati a video i bottoni di queste strade. 
	 */
	public void mostraStradeSelezionabiliInizio() {
		List<String> idStradeSelezionabili = new ArrayList<String>(); 
		List<Strada> stradeSelezionabili = new ArrayList<Strada>(); 
		List<Strada> stradeMappa = statoPartita.getMappa().getStradeMappa(); 
		for (Strada strada: stradeMappa) {
			if(strada.getLibera()) {
				stradeSelezionabili.add(strada); 
			}
		}
		for(Strada strada: stradeSelezionabili) {
			idStradeSelezionabili.add(statoPartita.getMappa().getMappaVerticiInversa().get(strada)); 
		}
		view.attivaBottoniStradeSelezionabili(idStradeSelezionabili); 
	}
	
	/**
	 * effettua lo spostamento del pastore
	 * @param strada in cui si vuole arrivare
	 */
	public void spostaPedina(Strada strada) {
		List<Strada> stradeAdiacentiAPartenza = statoPartita.getMappa().getStradeAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore());
		Strada posizionePartenza = statoPartita.getTurno().getPosizioneGiocatore(); 
		Strada posizioneArrivo = strada; 
		boolean adiacente = false; 
		for(Strada street: stradeAdiacentiAPartenza) {
			//se la strada selezionata è adiacente alla strada del giocatore, setto a true la variabile adiacente
			if(posizioneArrivo == street) {
				adiacente = true; 
			}
		}
		//se non è adiacente decremento i danari del giocatore di 1
		if (!adiacente) {
			decrementaDanari(1, statoPartita.getTurno().getGiocatore());
		}
		//controllo se la strada di partenza è quella della prima pedina o della seconda (per gestire il caso dei due giocatori)
		if(posizionePartenza == statoPartita.getTurno().getGiocatore().getPosizioneGiocatore()) {
			statoPartita.getTurno().getGiocatore().setPosizioneGiocatore(posizioneArrivo);
		} else {
			statoPartita.getTurno().getGiocatore().setPosizione2Giocatore(posizioneArrivo);
		}
		//setta la nuova posizione del giocatore nel turno
		statoPartita.getTurno().setPosizioneGiocatore(posizioneArrivo);
		//la strada di arrivo è ora occupata, viene posizionato il recinto
		posizioneArrivo.setLiberaFalse();
		String idStradaPartenza = statoPartita.getMappa().getMappaVerticiInversa().get(posizionePartenza); 
		String idStradaArrivo = statoPartita.getMappa().getMappaVerticiInversa().get(posizioneArrivo); 
		view.posizionaCancello(idStradaPartenza, faseFinale); 
		int coordinataXPartenza = posizionePartenza.getCoordinataX(); 
		int coordinataYPartenza = posizionePartenza.getCoordinataY(); 
		int coordinataXArrivo = posizioneArrivo.getCoordinataX(); 
		int coordinataYArrivo = posizioneArrivo.getCoordinataY();
		for(int i = 0; i<statoPartita.getNumeroGiocatori(); i++) {
			if(statoPartita.getTurno().getGiocatore().equals(statoPartita.getGiocatori().get(i))) { 
				view.setIconaGiocatoreInStrada(idStradaArrivo, coordinataXPartenza, coordinataYPartenza, coordinataXArrivo, coordinataYArrivo, i);
			}
		}
		//aumento il numero di recinti utilizzati e se il numero di recinti utilizzati è pari a 20, comincia la fase finale in cui verranno utilizzati i recinti finali
		incrementaNumeroRecintiUtilizzati(); 
		if(statoPartita.getNumeroRecintiUtilizzati() == 20) {
			faseFinale = true; 
		}
		//decremento le mosse rimanenti del giocatore nel turno
		decrementaNumeroMosseRimanenti(); 
		//setto l'ultima mossa fatta a true
		statoPartita.getTurno().setPastoreMosso(true);
		statoPartita.getTurno().setAnimaleMosso(false);
		statoPartita.getTurno().setTerrenoAcquistato(false);
		statoPartita.getTurno().setSparatoriaEffettuata(false);
		statoPartita.getTurno().setAccoppiamentoEffettuato(false);
		resettaValori();
	}
	
	/**
	 * trova quali regioni sono selezionabili per effettuare lo spostamento di un animale
	 * @return: una lista contenente gli id delle regioni selezionabili
	 */
	public List<Regione> trovaRegioniSelezionabiliPerSpostamentoAnimale() {
		List<Regione> regioniAdiacentiAlPastore = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore()); 
		List<Regione> regioniSelezionabili = new ArrayList<Regione>(); 
		for(Regione regione : regioniAdiacentiAlPastore) {
			if(regione.getTotaleArieti() > 0 || regione.getTotalePecoreBianche() > 0 || regione.equals(statoPartita.getPosizionePecoraNera())) {
				regioniSelezionabili.add(regione);
			}
		}
		return regioniSelezionabili; 
	}
	
	/**
	 * dice se la regione che è stata selezionata è valida o no
	 * @param idRegioneSelezionata: l'id della regione che è stata selezionata
	 * @param regioniSelezionabili: una lista contenente gli id delle regioni che sono selezionabili
	 * @return: true o false a seconda che la regione scelta fosse tra quelle selezionabili o meno
	 */
	public boolean checkRegioneSelezionataValida(String idRegioneSelezionata, List<Regione> regioniSelezionabili) {
		for(Regione regione: regioniSelezionabili) {
			if(idRegioneSelezionata.equals(statoPartita.getMappa().getMappaVerticiInversa().get(regione))) {
				return true; 
			}
		}
		return false; 
	}
	
	/**
	 * Trova quali sono gli animali selezionabili per lo spostamento
	 * @param regione: la regione da cui si vuole effettuare lo spostamento
	 * @return: una lista di integer, dove ciascun integer corrisponde a un particolare animale: 
	 * 0-pecora bianca, 1-ariete, 2-pecora nera
	 */
	public List<Integer> mostraAnimaliSelezionabiliPerSpostamento(Regione regione) {
		List<Integer> animaliSelezionabili = new ArrayList<Integer>(); 
		if(regione.getTotalePecoreBianche() > 0) {
			animaliSelezionabili.add(0); 
		}
		if(regione.getTotaleArieti() > 0) {
			animaliSelezionabili.add(1); 
		}
		if(regione.equals(statoPartita.getPosizionePecoraNera())) {
			animaliSelezionabili.add(2); 
		}
		return animaliSelezionabili; 
	}
	
	/**
	 * esegue lo spostamento dell'animale
	 * @param regione: la regione da cui si vuole effettuare lo spostamento
	 */
	public void effettuaSpostamentoAnimale(Regione regione) {
		List<Regione> regioniAdiacentiAlGiocatore = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore()); 
		Regione regionePartenza, regioneArrivo; 
		regionePartenza = regione; 
		if(regione == regioniAdiacentiAlGiocatore.get(0)) {
			regioneArrivo= regioniAdiacentiAlGiocatore.get(1); 
		} else {
			regioneArrivo = regioniAdiacentiAlGiocatore.get(0); 
		}
		switch (animaleScelto) {
			case PECORA_BIANCA:
				decrementaTotalePecoreBiancheInRegione(regionePartenza); 
				incrementaTotalePecoreBiancheInRegione(regioneArrivo); 
				view.spostamentoAnimale(statoPartita.getMappa().getMappaVerticiInversa().get(regionePartenza), statoPartita.getMappa().getMappaVerticiInversa().get(regioneArrivo), PECORA_BIANCA); 
				view.setLabelComunicazioni("Spostamento della pecora bianca effettuato!");
				break; 
			case PECORA_NERA:
				view.setPosizionePecoraNera(statoPartita.getMappa().getMappaVerticiInversa().get(regionePartenza), statoPartita.getMappa().getMappaVerticiInversa().get(regioneArrivo));
				statoPartita.setPosizionePecoraNera(regioneArrivo); 
				view.setLabelComunicazioni("Spostamento della pecora nera effettuato!");
				break; 
			case ARIETE : 
				decrementaTotaleArietiInRegione(regionePartenza);
				incrementaTotaleArietiInRegione(regioneArrivo);
				view.spostamentoAnimale(statoPartita.getMappa().getMappaVerticiInversa().get(regionePartenza), statoPartita.getMappa().getMappaVerticiInversa().get(regioneArrivo), ARIETE); 
				view.setLabelComunicazioni("Spostamento dell'ariete effettuato!");
				break; 
			default: 
				break; 
		}
		//decrementa il numero di mosse che il giocatore può ancora effettuare nel turno
		decrementaNumeroMosseRimanenti();
		//setta la mossa effettuata a true
		statoPartita.getTurno().setAnimaleMosso(true);
		resettaValori();
	}
	
	/**
	 * consente al giocatore di acquistare una tessera terreno
	 */
	public void eseguiAcquistoTesseraTerreno(){
		//prende il costo della tipologia di terreno scelto
		int costo = statoPartita.getCostoTesseraTerreno(tipoTerrenoScelto);
		//decrementa i danari del giocatore
		decrementaDanari(costo, statoPartita.getTurno().getGiocatore()); 
		//aggiunge la tessera terreno comprata dal giocatore alle sue tessere in possesso
		statoPartita.getTurno().getGiocatore().addTesseraTerrenoInPossesso(tipoTerrenoScelto);
		incrementaCostoTesseraTerreno(tipoTerrenoScelto); 
		decrementaNumeroMosseRimanenti(); 
		//setta la mossa effettuata a true
		statoPartita.getTurno().setTerrenoAcquistato(true);
		view.setLabelComunicazioni("Tessera terreno acquistata!");
		resettaValori();
	}
	
	/** 
	 * chiama un method per mostrare a video le tessere terreno che il giocatore può acquistare, dopo averle trovate
	 * tramite la funzione cercaTessereTerrenoAcquistabiliDalGiocatore
	 */
	public void mostraTessereTerrenoAcquistabili() {
		List<Integer> tessereAcquistabili = cercaTessereTerrenoAcquistabiliDalGiocatore(); 
		view.attivaBottoniTessereSelezionabili(tessereAcquistabili);
	}		

	/**
	 * trova quali tessere terreno il giocatore può acquistare
	 * @return: una lista di integer, dove ciascun integer corrisponde a una particolare tessera terreno
	 */
	public List<Integer> cercaTessereTerrenoAcquistabiliDalGiocatore() {
		List<Integer> tessereAcquistabili = new ArrayList<Integer>();
		// regioniAdiacenti contiene solo due elementi
		List<Regione> regioniAdiacenti = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore()); 
		for(Regione regione: regioniAdiacenti) {
			int tipoTerreno = regione.getTipoDiTerreno(); 	
			int costo;
			if(tipoTerreno != 6) {
				costo = statoPartita.getCostoTesseraTerreno(tipoTerreno);
			} else {
				costo = 10; 
			}
			//controllo che il costo non abbia superato il costo massimo delle tessere terreno e che il giocatore abbia abbastanza danari per comprarle
			if (costo <= statoPartita.getMaxCostoTessera() && costo <= statoPartita.getTurno().getGiocatore().getDanari()) {
				tessereAcquistabili.add(tipoTerreno); 
			}
		}
		return tessereAcquistabili; 
	}
	
	/**
	 * il method restituisce una lista delle regioni su cui il pastore può effettuare la sparatoria: tale regione deve
	 * essere adiacente alla sua posizione e il pastore deve poter comprare il silenzio di tutti con i suoi danari. 
	 */
	public List<Regione> trovaRegioniSelezionabiliPerSparatoria () {
		List<Regione> regioniCandidate = new ArrayList<Regione>(); 
		List<Regione> regioniAdiacenti = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore()); 
		for (Regione regioneAdiacente: regioniAdiacenti) {
			if (regioneAdiacente.getTotalePecoreBianche() > 0 || regioneAdiacente.getTotaleArieti() > 0) {
				int numeroGiocatoriVicini = 0; 
				List<Strada> stradeAdiacentiAllaRegione = statoPartita.getMappa().getStradeAdiacentiARegione(regioneAdiacente); 
				for (Strada stradaAdiacenteAllaRegione: stradeAdiacentiAllaRegione) {
					for (int k = 0; k < statoPartita.getNumeroGiocatori(); k++) {
						//controllo quanti giocatori ci sono nelle strade adiacenti alla regione
						if((statoPartita.getGiocatori().get(k) != statoPartita.getTurno().getGiocatore()) && (statoPartita.getGiocatori().get(k).getPosizioneGiocatore() == stradaAdiacenteAllaRegione || statoPartita.getGiocatori().get(k).getPosizione2Giocatore() == stradaAdiacenteAllaRegione)) {
								numeroGiocatoriVicini ++; 
						}
						
					}
				}
				//controllo che il giocatore possa pagare tutti i giocatori presenti nelle strade adiacenti alla regione
				if(statoPartita.getTurno().getGiocatore().getDanari() >= numeroGiocatoriVicini*2) {
					regioniCandidate.add(regioneAdiacente); 
				}
			}
		}
		return regioniCandidate; 
	}
	
	/**
	 * Trova quali sono gli animali selezionabili per la sparatoria
	 * @param regione: la regione su cui si vuole effettuare la sparatoria
	 * @return: una lista di integer, dove ciascun integer corrisponde a un particolare animale: 
	 * 0-pecora bianca, 1-ariete
	 */
	public List<Integer> mostraAnimaliSelezionabiliPerSparatoria(Regione regione) {
		List<Integer> animaliSelezionabili = new ArrayList<Integer>(); 
		if(regione.getTotalePecoreBianche() > 0) {
				animaliSelezionabili.add(0); 
			}
			if(regione.getTotaleArieti() > 0) {
				animaliSelezionabili.add(1); 
			}
			return animaliSelezionabili; 
		}
	
	/**
	 * effettua la sparatoria: questa va a buon fine se il valore ottenuto dal dado è uguale al numero della strada su
	 * cui si trova il pastore. In tal caso, gli altri giocatori lanciano un dado, se ottengono 5 o 6 devono essere pagati
	 */	
	public void eseguiSparatoria(Regione regione, String animale, int valoreLancioDado) {
		if (valoreLancioDado == statoPartita.getTurno().getPosizioneGiocatore().getNumeroStrada()) {
			switch (animaleScelto) {
				case PECORA_BIANCA:  
					decrementaTotalePecoreBiancheInRegione(regione);
					view.morteAnimale(PECORA_BIANCA, statoPartita.getMappa().getMappaVerticiInversa().get(regione));
					view.setTotalePecoreBiancheInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotalePecoreBianche());
					if(regione.getTotaleAgnelli() == 0 && regione.getTotaleArieti() == 0 && regione.getTotalePecoreBianche() == 0) {
						view.visibilityBottoniAnimali(statoPartita.getMappa().getMappaVerticiInversa().get(regione), false);
					}
					break; 
				case ARIETE:
					decrementaTotaleArietiInRegione(regione);
					view.morteAnimale(ARIETE, statoPartita.getMappa().getMappaVerticiInversa().get(regione));
					view.setTotaleArietiInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotaleArieti());
					if(regione.getTotaleAgnelli() == 0 && regione.getTotaleArieti() == 0 && regione.getTotalePecoreBianche() == 0) {
						view.visibilityBottoniAnimali(statoPartita.getMappa().getMappaVerticiInversa().get(regione), false);
					}
					break; 
				default: 
					break; 
			}
			//in giocatoriDaPagare metto i giocatori da pagare per ottenere il loro silenzio
			List<Giocatore> giocatoriDaPagare = new ArrayList<Giocatore>();  
			//in stradeAdiacentiAllaRegione metto le strade adiacenti alla regione: mi servono per 
			//verificare quali giocatori possono assistere alla sparatoria. 
			List<Strada> stradeAdiacentiAllaRegione = statoPartita.getMappa().getStradeAdiacentiARegione(regione); 
			//controllo se ci sono giocatori nelle strade adiacenti alla regione. 
			for (Strada stradaAdiacentiAllaRegione : stradeAdiacentiAllaRegione) { 
				// controllo se ci sono giocatori nelle strade adiacenti alla regione. 
				for (int j = 0; j < statoPartita.getNumeroGiocatori(); j++) {
					if((statoPartita.getGiocatori().get(j) != statoPartita.getTurno().getGiocatore()) && (statoPartita.getGiocatori().get(j).getPosizioneGiocatore() == stradaAdiacentiAllaRegione) || (statoPartita.getGiocatori().get(j).getPosizione2Giocatore() == stradaAdiacentiAllaRegione)) {
						valoreLancioDado = lancioDado(); 
						//lancia il dado il giocatore adiacente alla regione: se fa 5 0 6, lo aggiungo alla lista dei giocatori da pagare
						// mi serve anche il secondo controllo perchè, in caso di due giocatori con due posizioni, potrei aggiungerli più volte
						
						if((valoreLancioDado >= 5) && (!giocatoriDaPagare.contains(statoPartita.getGiocatori().get(j)))) {								
							//se non è già tra i giocatori da pagare, lo aggiungo		
							giocatoriDaPagare.add(statoPartita.getGiocatori().get(j)); 
						}
					}
				}
			}
			 
			//ho a questo punto trovato i giocatori da pagare: effettuo il pagamento
			for (int i = 0; i < giocatoriDaPagare.size(); i++) {
				incrementaDanari(2, giocatoriDaPagare.get(i)); 
				decrementaDanari(2, statoPartita.getTurno().getGiocatore()); 
			}
			view.setLabelComunicazioni("Sparatoria riuscita!");
		} else {
			view.setLabelComunicazioni("Sparatoria fallita!");
		}
		//decremento il numero di mosse disponibili nel turno
		decrementaNumeroMosseRimanenti();
		statoPartita.getTurno().setSparatoriaEffettuata(true);
		resettaValori();
	}

	/**
	 * controlla che il pastore possa effettuare la sparatoria
	 */
	public boolean checkSparatoriaConsentita() {
		List<Regione> regioniCandidate = trovaRegioniSelezionabiliPerSparatoria(); 
		return !regioniCandidate.isEmpty(); 
	}

	/**
	 * Restituisce una lista di regioni su cui il pastore può effettuare l'accoppiamento. Queste regioni devono essere 
	 * adiacenti alla posizione del pastore e in esse devono esserci almeno una pecora e un ariete. 
	 */
	public List<Regione> trovaRegioniSelezionabiliPerAccoppiamento() {
		List<Regione> regioniDisponibili = new ArrayList<Regione>(); 
		List <Regione> regioniAdiacenti = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore()); 
		for(Regione regioneAdiacente: regioniAdiacenti) {
			if(regioneAdiacente.getTotaleArieti() > 0 && regioneAdiacente.getTotalePecoreBianche() > 0) {
				regioniDisponibili.add(regioneAdiacente); 
			}
		}
		return regioniDisponibili; 
	}
	
	/**
	 * consente al giocatore di compiere la mossa EFFETTUA_ACCOPPIAMENTO
	 * @param regione: la regione su cui si vuole effettuare l'accoppiamento
	 * @param valoreLancioDado: il valore del lancio del dado, da cui dipende la buona riuscita o meno dell'accoppiamento
	 */
	public void eseguiAccoppiamento(Regione regione, int valoreLancioDado) {
		if (valoreLancioDado == statoPartita.getTurno().getPosizioneGiocatore().getNumeroStrada()) {
			aggiungiAgnello(regione); 
			view.setTotaleAgnelliInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotaleAgnelli());
			view.setLabelComunicazioni("Accoppiamento riuscito!");
		} else {
			view.setLabelComunicazioni("Accoppiamento fallito!");
		}
		statoPartita.getTurno().setAccoppiamentoEffettuato(true);
		decrementaNumeroMosseRimanenti();
		resettaValori();
	}
	
	/**
	 * Ritorna true o false a seconda che ci sia almeno una regione su cui il pastore può effettuare l'accoppiamento 
	 */
	public boolean checkAccoppiamentoConsentito() {
		List<Regione> regioniDisponibili = trovaRegioniSelezionabiliPerAccoppiamento(); 
		return !regioniDisponibili.isEmpty(); 
	}
	
	/**
	 * controlla che sia effettuabile la mossa MUOVI_PEDINA
	 * @return: true o false, a seconda che la mossa sia effettuabile o meno
	 */
	public boolean checkSpostamentoPedinaConsentito() {
		boolean presenzaStradaLibera = false; 
		if(statoPartita.getTurno().getGiocatore().getDanari() > 0) {
			return true; 
		} else {
			List<Strada> stradeAdiacentiAlPastore = statoPartita.getMappa().getStradeAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore());
			for (Strada strada: stradeAdiacentiAlPastore) {
				if(strada.getLibera()) {
					presenzaStradaLibera = true; 
				}
			}
		return presenzaStradaLibera; 	
		}
	}
	
	/**
	 * controlla che ci siano tessere terreno acquistabili, quindi che il giocatore possa o meno effettuare 
	 * la mossa acquistaTesseraTerreno
	 */
	public boolean checkAcquistoTesseraTerrenoConsentito() {
		List<Integer> tessereAcquistabili = cercaTessereTerrenoAcquistabiliDalGiocatore();
		return !tessereAcquistabili.isEmpty(); 
	}
	
	/**
	 * controlla che il giocatore possa effettuare la mossa MUOVI_ANIMALE, ossia che ci siano degli animali da spostare
	 * @return: true o false, a seconda che il giocatore possa o meno effettuare tale mossa
	 */
	public boolean checkSpostamentoAnimaleConsentito() {
		List<Regione> regioniAdiacentiAlPastore = statoPartita.getMappa().getRegioniAdiacentiAStrada(statoPartita.getTurno().getPosizioneGiocatore());
		for (Regione regione: regioniAdiacentiAlPastore) {
			if ((regione.getTotalePecoreBianche()>0) || (regione.getTotaleArieti()>0) || (regione == statoPartita.getPosizionePecoraNera())) {
				return true; 
			}
		}
		return false; 
	}

	/**
	 * incrementa automaticamente i danari del giocatore del turno
	 * @param danariGuadagnati: danari da aggiungere al giocatore del turno
	 */
	public void incrementaDanari(int danariGuadagnati) { 
		int danari = statoPartita.getTurno().getGiocatore().getDanari() + danariGuadagnati; 
		statoPartita.getTurno().getGiocatore().setDanari(danari);
		int indiceGiocatore = -1; 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			if(statoPartita.getTurno().getGiocatore() == statoPartita.getGiocatori().get(i)) {
				indiceGiocatore = i; 
			}
		}
		view.setDanariGiocatore(indiceGiocatore, danari);
		}
	
	/**
	 * incrementa i danari del giocatore che viene passato come parametro
	 * @param danariGuadagnati: danari da sommare a quelli del giocatore
	 * @param giocatore: giocatore a cui aggiungere danari
	 */
	public void incrementaDanari(int danariGuadagnati, Giocatore giocatore) { 
		int danari = giocatore.getDanari() + danariGuadagnati; 
		giocatore.setDanari(danari);
		int indiceGiocatore = -1; 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			if(giocatore == statoPartita.getGiocatori().get(i)) {
				indiceGiocatore = i; 
			}
		}
		view.setDanariGiocatore(indiceGiocatore, danari); 
		}
	
	/**
	 * decrementa i danari del giocatore del turno
	 * @param danariSpesi: i danari da sottrarre a quelli che il giocatore possiede
	 */
	public void decrementaDanari(int danariSpesi) {
		int danari = statoPartita.getTurno().getGiocatore().getDanari() - danariSpesi; 
		statoPartita.getTurno().getGiocatore().setDanari(danari); 
		int indiceGiocatore = -1; 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			if(statoPartita.getTurno().getGiocatore() == statoPartita.getGiocatori().get(i)) {
				indiceGiocatore = i; 
			}
		}
		view.setDanariGiocatore(indiceGiocatore, danari);
		}
	
	/**
	 * decrementa i danari del giocatore passato per parametro della quantità di danari che viene sempre passata
	 * per parametro
	 * @param danariSpesi: danari da sottrarre al totale di quelli posseduti dal giocatore
	 * @param giocatore: giocatore a cui sottrarre i danari
	 */
	public void decrementaDanari(int danariSpesi, Giocatore giocatore) {
		int danari = giocatore.getDanari() - danariSpesi; 
		giocatore.setDanari(danari); 
		int indiceGiocatore = -1; 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			if(giocatore == statoPartita.getGiocatori().get(i)) {
				indiceGiocatore = i; 
			}
		}
		view.setDanariGiocatore(indiceGiocatore, danari); 
		}
	
	/**
	 * incrementa di una unità il costo della tessera terreno del tipo passato per parametro
	 * @param tipologia: tipo di terreno di cui deve essere incrementato il costo
	 */
	public void incrementaCostoTesseraTerreno(int tipologia) {
		statoPartita.setCostoTesseraTerreno(tipologia, statoPartita.getCostoTesseraTerreno(tipologia) + 1);
		view.setCostoTesseraTerreno(tipologia, statoPartita.getCostoTesseraTerreno(tipologia)); 
	}
	
	/**
	 * decrementa di una unità il totale delle pecore presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere applicata la sottrazione sul totale delle pecore
	 */
	public void decrementaTotalePecoreBiancheInRegione(Regione regione) {
		regione.setTotalePecoreBianche(regione.getTotalePecoreBianche() - 1);
		view.setTotalePecoreBiancheInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione),regione.getTotalePecoreBianche());
		if(regione.getTotaleAgnelli()==0 && regione.getTotaleArieti()==0 && regione.getTotalePecoreBianche() == 0) {
			view.visibilityBottoniAnimali(statoPartita.getMappa().getMappaVerticiInversa().get(regione), false);
		}
	}
	
	/**
	 * incrementa di una unità il totale delle pecore presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere effettuato l'incremento del totale delle pecore
	 */
	public void incrementaTotalePecoreBiancheInRegione(Regione regione) {
		regione.setTotalePecoreBianche(regione.getTotalePecoreBianche() + 1);
		view.setTotalePecoreBiancheInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotalePecoreBianche());
	}
	
	/**
	 * incrementa di una unità il totale degli agnelli presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere effettuato l'incremento del totale degli agnelli
	 */
	public void incrementaTotaleAgnelliInRegione(Regione regione) {
		regione.setTotaleAgnelli(regione.getTotaleAgnelli() + 1);
		view.setTotaleAgnelliInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione),regione.getTotaleAgnelli());
	}
	
	/**
	 * decrementa di una unità il totale degli arieti presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere applicata la sottrazione sul totale degli arieti
	 */
	public void decrementaTotaleArietiInRegione(Regione regione) {
		regione.setTotaleArieti(regione.getTotaleArieti() - 1);
		view.setTotaleArietiInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotaleArieti());
		if(regione.getTotaleAgnelli()==0 && regione.getTotaleArieti()==0 && regione.getTotalePecoreBianche() == 0) {
			view.visibilityBottoniAnimali(statoPartita.getMappa().getMappaVerticiInversa().get(regione), false);
		}
	}
	
	/**
	 * incrementa di una unità il totale degli arieti presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere effettuato l'incremento del totale degli arieti
	 */
	public void incrementaTotaleArietiInRegione(Regione regione) {
		regione.setTotaleArieti(regione.getTotaleArieti() + 1);
		view.setTotaleArietiInRegione(statoPartita.getMappa().getMappaVerticiInversa().get(regione), regione.getTotaleArieti());
	}
	
	/**
	 * decrementa di una unità il totale degli agnelli presenti nella regione passata per parametro
	 * @param regione: regione in cui deve essere applicata la sottrazione sul totale degli agnelli
	 */
	public void decrementaTotaleAgnelliInRegione(Regione regione) {
		regione.setTotaleAgnelli(regione.getTotaleAgnelli() - 1);
	}
	
	/**
	 * decrementa di una unità il numero delle mosse che il giocatore può effettuare nel turno
	 */
	public void decrementaNumeroMosseRimanenti() {
		statoPartita.getTurno().setNumeroMosseRimanenti(statoPartita.getTurno().getNumeroMosseRimanenti()-1); 
	}
	
	/**
	 * incrementa di una unità il numero totale dei recinti che sono stati utilizzati durante la partita
	 */
	public void incrementaNumeroRecintiUtilizzati() {
		statoPartita.setNumeroRecintiUtilizzati(statoPartita.getNumeroRecintiUtilizzati()+1);
	}
	
	/**
	 * incrementa di una unità il totale degli agnelli presenti nella regione passata per parametro, aggiunge alla lista 
	 * dei contatori un nuovo elemento e aggiunge alla lista delle regioni con agnelli la regione passata per parametro
	 * @param regione: regione in cui è stato aggiunto l'agnello
	 */
	public void aggiungiAgnello(Regione regione) {
		view.nascitaAgnello(statoPartita.getMappa().getMappaVerticiInversa().get(regione));
		regione.setTotaleAgnelli(regione.getTotaleAgnelli() + 1);
		regione.getContatoriVitaAgnello().add(2); 
		statoPartita.getPosizioniAgnelli().add(regione); 
	}
	
	/**
	 * viene chiamata all'inizio della partita: fa in modo che i giocatori scelgano le proprie posizioni di partenza
	 */
	public void scegliStradePartenza() {
		Giocatore giocatore = statoPartita.getGiocatori().get(0); 
		switch(statoPartita.getNumeroGiocatori()) {
		//se i giocatori sono 2, le strade di partenza da selezionare saranno 4, 2 per ogni giocatore
		case 2: avviaSceltaPosizioni(4, giocatore); 
				break; 
		case 3: avviaSceltaPosizioni(3, giocatore); 
	 			break;
		case 4:	avviaSceltaPosizioni(4, giocatore); 
				break; 
		default: 
				break; 
		}
		
	}
	
	public void avviaSceltaPosizioni(int totaleStradeDaSelezionare, Giocatore giocatore) {
		numeroStradeAncoraDaSelezionare = totaleStradeDaSelezionare;
		statoPartita.getTurno().setGiocatore(giocatore);
		sceltaStrada(giocatore); 
	}
	
	public void setStradaScelta(Strada stradaScelta) {
		this.stradaScelta = stradaScelta; 
	}
	
	public Strada getStradaScelta() {
		return this.stradaScelta; 
	}
	
	public void setRegioneScelta(Regione regioneScelta) {
		this.regioneScelta = regioneScelta; 
	}
	
	public Regione getRegioneScelta() {
		return this.regioneScelta; 
	}
	
	public void setTipoTerrenoScelto(Integer tipo) {
		this.tipoTerrenoScelto = tipo;  
	}
	
	public Integer getTipoTerrenoScelto() {
		return this.tipoTerrenoScelto; 
	}
	
	public void setMossaScelta(String tipo) {
		this.mossaScelta = tipo; 
	}
	
	public String getMossaScelta() {
		return this.mossaScelta; 
	}
	
	public void setAnimaleScelto(String animaleSelezionato) {
		this.animaleScelto = animaleSelezionato; 
	}
	
	public String getAnimaleScelto() {
		return this.animaleScelto; 
	}
	
	public void setValoreLancioDado(Integer valore) {
		this.valoreLancioDado = valore; 
	}
	
	public Integer getValoreLancioDado() {
		return this.valoreLancioDado; 
	}
	
	public void setGiocatoreCheScegliePosizione(Giocatore giocatore) {
		this.giocatoreCheScegliePosizione = giocatore; 
	}
	
	public Giocatore getGiocatoreCheScegliePosizione() {
		return this.giocatoreCheScegliePosizione; 
	}
	
	public boolean getAssegnata() {
		return this.assegnata; 
	}
	
	public void setAssegnata(boolean bool) {
		this.assegnata = bool; 
	}
	
	public void setSceltaPosizioneEffettuata(boolean valore) {
		this.sceltaPosizioneEffettuata = valore; 
	}
	
	public boolean getSceltaPosizioneEffettuata() {
		return this.sceltaPosizioneEffettuata; 
	}
	
	public boolean getFaseFinale() {
		return this.faseFinale; 
	}
	
	public void setFaseFinale(boolean faseFinale) {
		this.faseFinale = faseFinale; 
	}
	
	/**
	 * viene chiamata quando il numero di giocatori è uguale a 2 ed è quindi necessario che essi scelgano la propria 
	 * posizione di inizio turno. Chiama un method della view che visualizza a video quali bottoni essi possono
	 * selezionare (cioè quale delle due posizioni a loro assegnate possono scegliere)
	 */
	public void sceltaPosizioneTurno() {
		statoPartita.getTurno().setSelezioneSceltaStradaNecessaria(true);
		List<String> idStradeSelezionabili = new ArrayList<String>(); 
		idStradeSelezionabili.add(statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getTurno().getGiocatore().getPosizioneGiocatore())); 
		idStradeSelezionabili.add(statoPartita.getMappa().getMappaVerticiInversa().get(statoPartita.getTurno().getGiocatore().getPosizione2Giocatore()));
		view.setLabelComunicazioni("" + statoPartita.getTurno().getGiocatore().getNomeGiocatore() + ": seleziona la tua posizione nel turno");
		view.attivaBottoniStradeSelezionabili(idStradeSelezionabili);
	}
	
	/**
	 * riporta i valori del turno a quelli di default
	 */
	public void resettaValori() {
		this.sceltaPosizioneEffettuata = false; 
		this.animaleScelto = null; 
		this.giocatoreCheScegliePosizione = null; 
		this.assegnata = false;
		this.mossaScelta = null; 
		this.regioneScelta = null; 
		this.stradaScelta = null; 
		this.tipoTerrenoScelto = null; 
	}

	public int getNumeroStradeAncoraDaSelezionare() {
		return numeroStradeAncoraDaSelezionare;
	}

	public void setNumeroStradeAncoraDaSelezionare(int numeroStradeAncoraDaSelezionare) {
		this.numeroStradeAncoraDaSelezionare = numeroStradeAncoraDaSelezionare;
	}
	
	/**
	 * viene invocata all'inizio della partita, quando il giocatore deve scegliere la propria posizione 
	 * di partenza. 
	 * @param giocatore: giocatore che deve scegliere la posizione
	 */
	public void sceltaStrada(Giocatore giocatore) {
		view.setLabelComunicazioni(giocatore.getNomeGiocatore() + ": scegli la posizione di partenza");
		giocatoreCheScegliePosizione = giocatore; 
		mostraStradeSelezionabiliInizio(); 
		}
}