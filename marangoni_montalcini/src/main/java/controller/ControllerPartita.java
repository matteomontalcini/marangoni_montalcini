package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import model.Giocatore;
import model.Regione;
import model.StatoPartita;
import model.Strada;
import view.InterfacciaView;
import eventi.AnimaleSelezionato;
import eventi.EventoGenerico;
import eventi.MossaSelezionata;
import eventi.RegioneSelezionata;
import eventi.StradaSelezionata;
import eventi.TesseraTerrenoSelezionata;


public class ControllerPartita implements Observer {

	private StatoPartita statoPartita;
	
	private ControllerTurno controllerTurno; 
	
	private InterfacciaView view; 
	
	private static final String MUOVI_PEDINA = "MUOVI_PEDINA", EFFETTUA_ACCOPPIAMENTO = "EFFETTUA_ACCOPPIAMENTO", EFFETTUA_SPARATORIA = "EFFETTUA_SPARATORIA",
			ACQUISTA_TESSERA_TERRENO = "ACQUISTA_TESSERA_TERRENO", MUOVI_ANIMALE = "MUOVI_ANIMALE";
	
	private Timer timer = new Timer(); 

	/**
	 * costruttore
	 */
	public ControllerPartita(StatoPartita statoPartita, InterfacciaView view) {    
		this.statoPartita = statoPartita; 
		this.view = view; 
		this.controllerTurno = new ControllerTurno(statoPartita, view);  
	}

	/**
	 * Funzione che serve per richiamare il method della schermata principale che permette di visualizzare
	 * i nomi dei giocatori a video sulla schermata
	 */
	public void inserisciGiocatori() {
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			view.inserisciGiocatore(i); 
		}
	}

	/**
	 * Funzione che viene chiamata quando il gioco funziona in rete. Viene creato un nuovo giocatore prendendo come parametri
	 * l'indice corrispondente al numero di giocatori attualmente presenti +1, il nome del giocatore che vuole partecipare
	 * alla partita, la data inserita ed il numero di giocatori totali della partita. Il giocatore viene inserito all'interno
	 * di una HashMap in cui si fa corrispondere al numero di indice del giocatore, un giocatore. Il giocatore viene aggiunto
	 * alla lista di giocatori presente all'interno dello stato partita.
	 */
	public void inserisciGiocatore(int index, String nomeGiocatore, Date data, int numeroGiocatori) {
		Giocatore giocatore = new Giocatore (nomeGiocatore, data, numeroGiocatori); 
		statoPartita.getGiocatoriOnline().put(index,giocatore);
		statoPartita.getGiocatori().add(giocatore);
	}

	/**
	 * Funzione che permette di distribuire ai giocatori della partita le tessere iniziali.
	 */
	public void distribuisciTessereIniziali() {
		List<Integer> tessereAncoraDisponibili = new ArrayList<Integer>(); 
		for(int i = 0; i < 6; i++) {
			tessereAncoraDisponibili.add(i); 
		}
		//tessereAncoraDisponibili contiene i numeri da 0 a 5, cioè i tipi di terreno
		for(int j = 0; j < statoPartita.getNumeroGiocatori(); j++) {
			int k; 
			Random random = new Random(); 
			k = random.nextInt(tessereAncoraDisponibili.size()); 
			//viene generato un numero compreso tra 0 e 5 la prima volta, e via a scendere le volte successive
			statoPartita.getGiocatori().get(j).addTesseraTerrenoInPossesso(tessereAncoraDisponibili.get(k));
			tessereAncoraDisponibili.remove(tessereAncoraDisponibili.get(k)); 
		}
	}

	/**
	 * Funzione che scandisce la lista di giocatori e, confrontando le date, determina chi sarà il primo giocatore,
	 * se la data più recente è comune a più giocatori, viene scelto il giocatore più in fondo nella lista, ovvero quello
	 * che ha inserito la data per ultimo.
	 */
	public void stabilisciPrimoGiocatore() {
		Giocatore giocatore = statoPartita.getGiocatori().get(0); 
		for (int i = 1; i < statoPartita.getNumeroGiocatori(); i++) {
			if(statoPartita.getGiocatori().get(i).getUltimaCarezza().after(giocatore.getUltimaCarezza())) {
				giocatore = statoPartita.getGiocatori().get(i); 
			}
		}
		statoPartita.setPrimoGiocatore(giocatore);
	}

	/**
	 * Funzione che calcola il punteggio dei giocatori controllando il numero di pecore (bianche e nere) ed il numero di
	 * arieti presenti nel tipo di regione e moltiplicandolo per il numero di tessere che il giocatore ha di quel tipo.
	 */
	public void setPunteggioGiocatori() {
		for(Giocatore giocatore : statoPartita.getGiocatori()) {
			int punteggio = 0;  
			for(int j = 0; j < giocatore.getTessereTerrenoInPossesso().size(); j++) {
				punteggio = punteggio + totalePecoreInTipoDiTerreno(j)*(giocatore.getTessereTerrenoInPossesso().get(j)) + totaleArietiInTipoDiTerreno(j)*(giocatore.getTessereTerrenoInPossesso().get(j));

			}		
			//viene aggiunto al punteggio totalizzato grazie alle tessere terreno, alle pecore ed agli arieti, la quantità di danari
			punteggio += giocatore.getDanari();
			giocatore.setPunteggioTotalizzato(punteggio);
		}	
	}
	
	/**
	 * Funzione che scandisce la lista di giocatori e restituisce il punteggio massimo.
	 */
	public int trovaPunteggioMassimo() {
		int punteggio = 0; 
		for (Giocatore giocatore: statoPartita.getGiocatori()) {
			if(giocatore.getPunteggioTotalizzato() > punteggio) {
				punteggio = giocatore.getPunteggioTotalizzato(); 
			}
		}
		return punteggio; 
	}

	/**
	 * Funzione che cerca il punteggio di ogni giocatore e lo aggiunge ad una lista di punteggi che poi restituisce.
	 * La lista di punteggi è in ordine dal primo all'ultimo giocatore. 
	 */
	public List<Integer> restituisciPunteggiGiocatori() {
		List<Integer> punteggiGiocatori = new ArrayList<Integer>(); 
		for(int i = 0; i < statoPartita.getGiocatori().size(); i++) {
			int punteggio = statoPartita.getGiocatori().get(i).getPunteggioTotalizzato(); 
			punteggiGiocatori.add(punteggio); 
		}
		return punteggiGiocatori; 
	}

	/**
	 * Funzione che, dato il tipo di terreno, ne restituisce il totale delle pecore presenti nelle regioni di quel
	 * tipo, contando la pecora nera come due pecore bianche.
	 */
	public int totalePecoreInTipoDiTerreno(int tipoDiTerreno) {
		List<Regione> regioni = statoPartita.getMappa().getRegioniMappa(); 
		int totalePecore = 0; 
		for (Regione regione: regioni) {
			if(tipoDiTerreno == regione.getTipoDiTerreno()) {
				totalePecore = totalePecore + regione.getTotalePecoreBianche(); 
				if(regione == statoPartita.getPosizionePecoraNera()) {
					totalePecore += 2; 
				}
			}
		}
		return totalePecore; 	
	}

	/**
	 * Funzione che, dato il tipo di terreno, ne restituisce il totale degli arieti presenti nelle regioni di quel tipo.
	 */
	public int totaleArietiInTipoDiTerreno(int tipoDiTerreno) {
		List<Regione> regioni = statoPartita.getMappa().getRegioniMappa(); 
		int totaleArieti = 0; 
		for (Regione regione: regioni) {
			if(tipoDiTerreno == regione.getTipoDiTerreno()) {
				totaleArieti = totaleArieti + regione.getTotaleArieti(); 
			}
		}
		return totaleArieti; 	
	}

	/**
	 * Funzione che, preso in ingresso il numero di giocatori, si occupa di attribuire 20 danari ad ogni giocatore
	 * se i giocatori della partita sono 3 o 4, 30 danari ad ogni giocatore se i giocatori della partita sono 2.
	 */
	public void distribuisciDanari(int numeroGiocatori) {
		if(numeroGiocatori==2) {
			for(Giocatore giocatore: statoPartita.getGiocatori()) {
				giocatore.setDanari(30);
			}
		} else {
			for(Giocatore giocatore: statoPartita.getGiocatori()) {
				giocatore.setDanari(20);
			}
		}
	}

	public ControllerTurno getControllerTurno() {
		return this.controllerTurno; 
	}

	/**
	 * Funzione che, trovato il giocatore del turno nell'array di giocatori della partita, si occupa di visualizzare
	 * a schermo il numero di tessere terreno in possesso.
	 */
	public void visualizzaTessereInPossesso() {
		//setto inizialmente indice giocatore a un numero molto più grande di quello che potrò avere
		int indiceGiocatore = 200; 
		for(int j = 0; j < statoPartita.getNumeroGiocatori(); j++) {
			if(statoPartita.getGiocatori().get(j) == statoPartita.getTurno().getGiocatore()) {
				indiceGiocatore = j + 1;
			}
		}
		view.setTessereInPossesso(statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso(), indiceGiocatore);
	}

	/**
	 * Funzione che, presa una strada in ingresso, permette di assegnarla come posizione al giocatore settato nel turno
	 * come giocatore che deve scegliere la posizione. 
	 */
	public void assegnaPosizioni(Strada strada) {
		List<Giocatore> giocatori = statoPartita.getGiocatori(); 
		/*
		 *Scorro la lista dei giocatori, se il giocatore è uguale al giocatore del turno e la posizione 1 del giocatore è null
		 *allora setto la posizione e l'icona del giocatore nella strada e setto la variabile booleana della strada 'libera' a false, decremento inoltre
		 *il numero di strade ancora da selezionare nel controller turno.
		 *Se il giocatore è uguale al giocatore del turno e la posizione 1 del giocatore è diversa da null, vuol dire che sono nel caso di una partita a due
		 *giocatori e devo settare la seconda posizione del giocatore e la seconda icona del giocatore nella strada, setto la variabile booleana della strada
		 *'libera' a false e decremento il numero di strade ancora da selezionare nel controller turno.
		 */
		for(int i = 0; i < statoPartita.getNumeroGiocatori(); i++) {
			if(giocatori.get(i).equals(controllerTurno.getGiocatoreCheScegliePosizione()) && giocatori.get(i).getPosizioneGiocatore()==null) {
				giocatori.get(i).setPosizioneGiocatore(strada);
				view.setIconaGiocatoreInizio(statoPartita.getMappa().getMappaVerticiInversa().get(strada), i); 
				strada.setLiberaFalse();
				controllerTurno.setNumeroStradeAncoraDaSelezionare(controllerTurno.getNumeroStradeAncoraDaSelezionare() - 1);

			} else if (giocatori.get(i).equals(controllerTurno.getGiocatoreCheScegliePosizione()) && !(giocatori.get(i).getPosizioneGiocatore()==null)) {
				giocatori.get(i).setPosizione2Giocatore(strada);  
				view.setIconaGiocatoreInizio(statoPartita.getMappa().getMappaVerticiInversa().get(strada), i);
				strada.setLiberaFalse();
				controllerTurno.setNumeroStradeAncoraDaSelezionare(controllerTurno.getNumeroStradeAncoraDaSelezionare() - 1);
			}
		}
		Giocatore giocatore = null; 
		/*Scandisco la lista dei giocatori, quando trovo il giocatore settato nel controller turno come giocatore che deve scegliere la posizione
		 *controllo, se il giocatore è l'ultimo della lista, assegno alla variabile giocatore il primo giocatore della lista (in modo che la lista sia circolare),
		 *altrimenti assegno il giocatore successivo nella lista. In seguito resetto i valori di controller turno, setto nel turno il giocatore a quello trovato
		 *visualizzo le tessere terreno in possesso del giocatore nella schermata di gioco e chiamo la funzione sceltaStrada passandogli il giocatore trovato
		 *in modo da settare anche a lui la strada di partenza. Continuo finche il numero di strade da selezionare non arriva a 0.
		 *Se il numero di strade da selezionare è posto a 0, parto dal primo giocatore, ne visualizzo le tessere terreno in possesso e comincio la partita.
		 */
		if(controllerTurno.getNumeroStradeAncoraDaSelezionare() > 0) {
			for (int i=0; i<statoPartita.getNumeroGiocatori(); i++) {
				if(controllerTurno.getGiocatoreCheScegliePosizione() == statoPartita.getGiocatori().get(i)) {
					if(i == statoPartita.getNumeroGiocatori()-1) {
						giocatore = statoPartita.getGiocatori().get(0); 
					} else {
						giocatore = statoPartita.getGiocatori().get(i+1); 
					}
				}
			}
			controllerTurno.resettaValori();
			statoPartita.getTurno().setGiocatore(giocatore);
			visualizzaTessereInPossesso();
			controllerTurno.sceltaStrada(giocatore);
		} else if (controllerTurno.getNumeroStradeAncoraDaSelezionare() == 0 ) {
			statoPartita.getTurno().setGiocatore(statoPartita.getPrimoGiocatore());
			visualizzaTessereInPossesso(); 
			controllerTurno.eseguiTurno();
		}
	}

	/**
	 * Funzione che, dato un giocatore, restituisce il giocatore del turno successivo.
	 */
	public Giocatore trovaGiocatoreDelTurnoSuccessivo(Giocatore giocatoreDelTurnoFinito) {
		for (int i=0; i<statoPartita.getNumeroGiocatori(); i++) {
			if(giocatoreDelTurnoFinito == statoPartita.getGiocatori().get(i)) {
				if(i == statoPartita.getNumeroGiocatori()-1) {
					return statoPartita.getGiocatori().get(0); 
				} else {
					return statoPartita.getGiocatori().get(i+1); 
				}
			}
		} 
		return null; 
	}

	/**
	 * Funzione che gestisce la fine del turno (resettando i valori del turno) e ne costruisce uno nuovo assegnandogli
	 * il giocatore successivo a quello attuale mediante 'resettaValoriTurno'.
	 */
	public void costruisciNuovoTurno() {
		view.disattivaBottoniMosse();
		view.setLabelComunicazioni("Fine del Turno!");
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				controllerTurno.resettaValori();
			}
		}, 1500);

		/*
		 * Se siamo nella fase finale e il giocatore successivo è uguale al primo giocatore, allora la partita è finita e sono da mostrare
		 * i risultati della partita. Altrimenti si deve continuare con i turni.
		 */
		Giocatore giocatore = trovaGiocatoreDelTurnoSuccessivo(statoPartita.getTurno().getGiocatore());
		if(controllerTurno.getFaseFinale() && giocatore.equals(statoPartita.getPrimoGiocatore())) {
			setPunteggioGiocatori();
			view.setLabelComunicazioni("FINE PARTITA!");
			view.generaRisultato(restituisciPunteggiGiocatori(), trovaPunteggioMassimo()); 
		} else {
			statoPartita.getTurno().resettaValoriTurno(giocatore);
			visualizzaTessereInPossesso(); 
			controllerTurno.eseguiTurno(); 
		}
	}
	
	public void gestisciMossaTerminata(int attesa) {
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				if(statoPartita.getTurno().getNumeroMosseRimanenti() > 0) {
					view.setLabelComunicazioni("Scegli una mossa tra quelle consentite");
					controllerTurno.mostraMosseConsentite();
				} else {
					costruisciNuovoTurno();
				}

			}
		}, attesa);
		
	}
	
	
	/**
	 * Method che controlla la validità dei nomi e delle date inserite dalla view
	 * @param nomi
	 * @param date
	 * @return
	 */
	public boolean controlloNomiEDate(List<String> nomi, List<Date> date) {
		boolean continuare = true; 
		for(int i = 0; i < nomi.size(); i++) {
			Date dataOdierna = new Date();
			String nomeGiocatoreOnline = nomi.get(i); 
			Date dataInserita = date.get(i); 
			if (nomeGiocatoreOnline.isEmpty()){
				continuare = false;
				JOptionPane.showMessageDialog(null,
						"Nomi inseriti errati: impossibile avviare il gioco",
						"Errore",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(this.getClass().getResource("/errore.png")));
			} else if(nomeGiocatoreOnline.startsWith(" ")){
				continuare = false;
				JOptionPane.showMessageDialog(null,
						"Nomi inseriti errati: impossibile avviare il gioco",
						"Errore",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(this.getClass().getResource("/errore.png")));
			} else if(!controlloCaratteriAmmessi(nomeGiocatoreOnline)){
				continuare = false;
				JOptionPane.showMessageDialog(null,
						"Nomi inseriti errati: impossibile avviare il gioco",
						"Errore",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(this.getClass().getResource("/errore.png")));
			} else if(dataInserita == null){
				continuare = false;
				JOptionPane.showMessageDialog(null,
						"Inserimento date non valido: impossibile avviare il gioco",
						"Errore",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(this.getClass().getResource("/errore.png")));
			} else if(dataOdierna.before(dataInserita)) {
				continuare = false;
				JOptionPane.showMessageDialog(null,
						"Inserimento date non valido: impossibile avviare il gioco",
						"Errore",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(this.getClass().getResource("/errore.png")));
			}
		}
		return continuare; 
		
	}
	
	/**
	 * Method che controlla i caratteri presenti nella stringa dei nomi
	 * @param string: nome del giocatore
	 */
	public boolean controlloCaratteriAmmessi(String string) {
		return !(string.contains(".") || string.contains(",") || string.contains(";") || string.contains(":") || string.contains("'") || string.contains("|") || string.contains("£") || string.contains("%") || string.contains("/") || string.contains("=") || string.contains("^") || string.contains("ç") || string.contains("@") || string.contains("§")); 
	
	}	

	/**
	 * Il controllerPartita è Observer nei confronti della schermataPrincipale: quando questa genera un evento, lo 
	 * notifica al controllerPartita. Tramite questo method, a seconda della casistica dell'evento, si chiamano opportuni metodi del 
	 * controller, dello stato partita e della view in modo che nella modalità di gioco remota, tutti i giocatori vedano a video
	 * le mosse del giocatore del turno.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if(!(arg instanceof EventoGenerico)) {
			throw new IllegalArgumentException();
		}	
		if(((EventoGenerico)arg).getIndiceGiocatore() == 0 || statoPartita.getTurno().getGiocatore() == statoPartita.getGiocatori().get(((EventoGenerico)arg).getIndiceGiocatore()-1)) {
			switch(((EventoGenerico)arg).getTipo()) {
			case EVENTO_GENERICO:
				break;
			case ANIMALE_SELEZIONATO: 
				controllerTurno.setAnimaleScelto(((AnimaleSelezionato) arg).getAnimaleSelezionato());
				view.disattivaPannelloSelezioneAnimali();
				switch (controllerTurno.getMossaScelta()) {
				case MUOVI_ANIMALE :
					controllerTurno.effettuaSpostamentoAnimale(controllerTurno.getRegioneScelta());
					gestisciMossaTerminata(1000);
					break; 
				case EFFETTUA_SPARATORIA: 
					controllerTurno.effettuaLancioDado();
					timer.schedule(new TimerTask() {

						@Override
						public void run() {
							controllerTurno.eseguiSparatoria(controllerTurno.getRegioneScelta(), controllerTurno.getAnimaleScelto(), controllerTurno.getValoreLancioDado());

						}
					}, 2000);

					gestisciMossaTerminata(3000);
					break; 
				default: 
					break; 
				}
				break;
			case MOSSA_SELEZIONATA:
				controllerTurno.setMossaScelta(((MossaSelezionata)arg).getMossaSelezionata());
				switch (((MossaSelezionata) arg).getMossaSelezionata()) {
				case MUOVI_PEDINA: 
					view.disattivaBottoniMosse(0);
					view.setLabelComunicazioni("Scegli la strada in cui vuoi spostare la pedina");
					view.attivaBottoniStradeSelezionabili(controllerTurno.mostraStradeSelezionabili()); 
					break;
				case MUOVI_ANIMALE: 
					view.disattivaBottoniMosse(1);
					view.setLabelComunicazioni("Scegli la regione da cui vuoi spostare un animale");
					view.attivaBottoniRegioni();
					break; 
				case ACQUISTA_TESSERA_TERRENO: 
					view.disattivaBottoniMosse(2);
					view.setLabelComunicazioni("Scegli la tessera terreno che vuoi acquistare");
					controllerTurno.mostraTessereTerrenoAcquistabili(); 
					break; 
				case EFFETTUA_ACCOPPIAMENTO: 
					view.disattivaBottoniMosse(4);
					view.setLabelComunicazioni("Scegli la regione in cui vuoi effettuare l'accoppiamento");
					view.attivaBottoniRegioni();
					break; 
				case EFFETTUA_SPARATORIA: 
					view.disattivaBottoniMosse(5);
					view.setLabelComunicazioni("Scegli la regione su cui vuoi effettuare la sparatoria");
					view.attivaBottoniRegioni();
					break; 
				default: 
					break; 
				}
				break;
			case REGIONE_SELEZIONATA:
				Regione regione = (Regione) statoPartita.getMappa().getMappaVertici().get(((RegioneSelezionata)arg).getIdRegione()); 
				controllerTurno.setRegioneScelta(regione);
				switch (controllerTurno.getMossaScelta()) {
				case MUOVI_ANIMALE: 
					if(controllerTurno.checkRegioneSelezionataValida(((RegioneSelezionata)arg).getIdRegione(), controllerTurno.trovaRegioniSelezionabiliPerSpostamentoAnimale())) {
						view.disattivaBottoniRegioni();
						view.attivaAnimaliSelezionabili(controllerTurno.mostraAnimaliSelezionabiliPerSpostamento(controllerTurno.getRegioneScelta())); 
					} else {
						view.setLabelComunicazioni("Regione scelta errata: seleziona un'altra regione");	
					}
					break; 
				case EFFETTUA_SPARATORIA: 
					if(controllerTurno.checkRegioneSelezionataValida(((RegioneSelezionata)arg).getIdRegione(), controllerTurno.trovaRegioniSelezionabiliPerSparatoria())) {
						view.disattivaBottoniRegioni();
						view.attivaAnimaliSelezionabili(controllerTurno.mostraAnimaliSelezionabiliPerSparatoria(controllerTurno.getRegioneScelta()));
					} else {
						view.setLabelComunicazioni("Regione scelta errata: seleziona un'altra regione");
					}
					break; 
				case EFFETTUA_ACCOPPIAMENTO: 
					if(controllerTurno.checkRegioneSelezionataValida(((RegioneSelezionata)arg).getIdRegione(), controllerTurno.trovaRegioniSelezionabiliPerAccoppiamento())) {
						view.disattivaBottoniRegioni();
						controllerTurno.effettuaLancioDado();
						timer.schedule(new TimerTask() {

							@Override
							public void run() {
								controllerTurno.eseguiAccoppiamento(controllerTurno.getRegioneScelta(), controllerTurno.getValoreLancioDado());
							}
						}, 2000);

						gestisciMossaTerminata(3000);
					} else {
						view.setLabelComunicazioni("Regione scelta errata: seleziona un'altra regione");
					}
					break; 
				default: 
					break; 
				}
				break;
			case STRADA_SELEZIONATA:
				Strada strada = (Strada) statoPartita.getMappa().getMappaVertici().get(((StradaSelezionata)arg).getIdStrada()); 	
				controllerTurno.setStradaScelta(strada); 
				if(statoPartita.getTurno().getSelezioneStradaNecessaria()) {
					statoPartita.getTurno().setSelezioneSceltaStradaNecessaria(false);
					controllerTurno.disattivaStradeSelezionabiliInizio();
					statoPartita.getTurno().setPosizioneGiocatore(strada);
					timer.schedule(new TimerTask() {

						@Override
						public void run() {
							controllerTurno.svolgimentoTurno();

						}
					}, 200);


				} else if(!statoPartita.getTurno().getSelezioneStradaNecessaria() && MUOVI_PEDINA.equals(controllerTurno.getMossaScelta())) {
					controllerTurno.spostaPedina(controllerTurno.getStradaScelta()); 
					view.disattivaBottoniStradeSelezionabili(controllerTurno.mostraStradeSelezionabili());
					gestisciMossaTerminata(1000);
				} else if (!statoPartita.getTurno().getSelezioneStradaNecessaria() && !MUOVI_PEDINA.equals(controllerTurno.getMossaScelta())){
					assegnaPosizioni(strada);
				}
				break;
			case CONFERMA_RICEZIONE_AVVISO_NO_MOSSE: 
				view.disattivaPannelloMosseNonDisponibili();
				costruisciNuovoTurno();
				break; 
			case CHIUSURA_FRAME_GIOCATORE: 
				view.chiudiSchermata(); 
				break; 
			case TESSERA_TERRENO_SELEZIONATA:
				view.disattivaBottoniTessereTerreno();
				controllerTurno.setTipoTerrenoScelto(((TesseraTerrenoSelezionata)arg).getTipoDiTerreno());
				controllerTurno.eseguiAcquistoTesseraTerreno();
				visualizzaTessereInPossesso();

				gestisciMossaTerminata(1000);
				break;
			default:
				break;

			}

		} 
	}


}
