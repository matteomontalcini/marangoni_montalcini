package marangoni_montalcini;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Giocatore;
import model.Regione;
import model.StatoPartita;
import model.Strada;
import model.Turno;

import org.junit.Before;
import org.junit.Test;

import view.ImplementazioneViewFittizia;
import view.InterfacciaView;
import controller.ControllerTurno;

public class TestControllerTurno {

	Turno turno, turno2;

	ControllerTurno controllerTurno, controllerTurno2;

	Strada strada29, strada22, strada00, strada03, strada02; 

	StatoPartita statoPartita, statoPartita2; 

	InterfacciaView view; 

	Regione regione00, regione01, regione12, regione13; 

	Giocatore giocatore1, giocatore2, giocatore3, giocatore4, giocatore5; 

	@Before
	public void setUp() throws Exception {
		//variabili per il primo test
		Date data = new Date();
		statoPartita = new StatoPartita(3); 
		giocatore1 = new Giocatore("Matteo", data, 3);
		strada00 = (Strada) statoPartita.getMappa().getMappaVertici().get("Strada00");
		giocatore1.setPosizioneGiocatore(strada00);
		strada00.setLiberaFalse();
		giocatore2 = new Giocatore("Luca", data, 3); 
		strada02 = (Strada) statoPartita.getMappa().getMappaVertici().get("Strada02");
		giocatore2.setPosizioneGiocatore(strada02);
		strada02.setLiberaFalse();
		giocatore3 = new Giocatore("Franco", data, 3); 
		strada03 = (Strada) statoPartita.getMappa().getMappaVertici().get("Strada03");
		giocatore3.setPosizioneGiocatore(strada03);
		strada03.setLiberaFalse();
		strada29 = (Strada)statoPartita.getMappa().getMappaVertici().get("Strada29"); 
		regione00 =(Regione) statoPartita.getMappa().getMappaVertici().get("Regione00"); 
		regione00.setTotaleAgnelli(0);
		regione00.setTotaleArieti(1);
		regione00.setTotalePecoreBianche(1);
		regione01 =(Regione) statoPartita.getMappa().getMappaVertici().get("Regione01"); 
		regione01.setTotaleAgnelli(0);
		regione01.setTotaleArieti(0);
		regione01.setTotalePecoreBianche(0);
		statoPartita.inizializzaCostiTessereTerreno();
		statoPartita.setCostoTesseraTerreno(2, 5);
		statoPartita.setCostoTesseraTerreno(0, 2);
		statoPartita.addGiocatore(giocatore1);
		statoPartita.addGiocatore(giocatore2);
		statoPartita.addGiocatore(giocatore3);
		List<String> nomi = new ArrayList<String>(); 
		nomi.add(giocatore1.getNomeGiocatore()); 
		nomi.add(giocatore2.getNomeGiocatore()); 
		nomi.add(giocatore3.getNomeGiocatore()); 
		view = new ImplementazioneViewFittizia(); 
		controllerTurno = new ControllerTurno(statoPartita, view); 
		controllerTurno.setTipoTerrenoScelto(0);
		turno = new Turno(giocatore1);
		turno.setPosizioneGiocatore(giocatore1.getPosizioneGiocatore());
		statoPartita.setTurno(turno);
		turno.setNumeroMosseRimanenti(1);
		turno.setPastoreMosso(false);
		turno.setTerrenoAcquistato(false);

		//variabili per il secondo test
		statoPartita2 = new StatoPartita(2); 
		giocatore4 = new Giocatore("Matteo", data, 2);
		giocatore4.setPosizioneGiocatore((Strada) statoPartita2.getMappa().getMappaVertici().get("Strada31"));
		giocatore4.setPosizione2Giocatore((Strada)statoPartita2.getMappa().getMappaVertici().get("Strada04"));
		giocatore5 = new Giocatore("Luca", data, 2); 
		giocatore5.setPosizioneGiocatore((Strada) statoPartita2.getMappa().getMappaVertici().get("Strada35"));
		giocatore5.setPosizione2Giocatore((Strada)statoPartita2.getMappa().getMappaVertici().get("Strada28"));
		giocatore5.setDanari(26); 
		strada22 = (Strada)statoPartita2.getMappa().getMappaVertici().get("Strada22");
		regione12 =(Regione) statoPartita2.getMappa().getMappaVertici().get("Regione12"); 
		regione12.setTotaleAgnelli(0);
		regione12.setTotaleArieti(0);
		regione12.setTotalePecoreBianche(1);
		regione13 =(Regione) statoPartita2.getMappa().getMappaVertici().get("Regione13"); 
		regione13.setTotaleAgnelli(1);
		regione13.setTotaleArieti(1);
		regione13.setTotalePecoreBianche(0);
		statoPartita2.inizializzaCostiTessereTerreno();
		statoPartita2.setCostoTesseraTerreno(5, 3);
		statoPartita2.addGiocatore(giocatore4);
		statoPartita2.addGiocatore(giocatore5);
		List<String> nomi2 = new ArrayList<String>(); 
		nomi2.add(giocatore4.getNomeGiocatore()); 
		nomi2.add(giocatore5.getNomeGiocatore()); 
		controllerTurno2 = new ControllerTurno(statoPartita2, view); 
		controllerTurno2.setTipoTerrenoScelto(5);
		turno2 = new Turno(giocatore4);
		turno2.setPosizioneGiocatore(giocatore4.getPosizioneGiocatore());
		statoPartita2.setTurno(turno2);
		turno2.setNumeroMosseRimanenti(2);
		turno2.setPastoreMosso(false);
		turno2.setAnimaleMosso(true);
	}

	@Test
	public void testTrovaMosseConsentite() {
		//primo test
		List<Integer> mossePossibili = controllerTurno.trovaMosseConsentite(); 
		assertEquals("La lista mossePossibili deve contenere un solo elemento", 1, mossePossibili.size());
		assertEquals("L'unico elemento presente deve essere 0, che corrisponde a muoviPedina", (Integer) 0, mossePossibili.get(0)); 

		//secondo test
		List<Integer> mossePossibili2 = controllerTurno2.trovaMosseConsentite(); 
		assertEquals("La lista mossePossibili deve contenere tre elementi", 3, mossePossibili2.size());
		assertEquals("Deve essere presente la mossa muovi pedina, che corrisponde all'intero 0", (Integer) 0, mossePossibili2.get(0)); 
		assertEquals("Deve essere presente la mossa acquista tessera terreno, che corrisponde all'intero 2", (Integer) 2, mossePossibili2.get(1)); 
		assertEquals("Deve essere presente la mossa effettua sparatoria, che corrisponde all'intero 3", (Integer) 3, mossePossibili2.get(2)); 
	}

	@Test
	public void testTrovaRegioniSelezionabiliPerSparatoria() {
		//primo test
		List<Regione> regioniSelezionabili = controllerTurno.trovaRegioniSelezionabiliPerSparatoria();
		assertEquals("La lista di regioniSelezionabili contiene un solo elemento", 1, regioniSelezionabili.size()); 
		assertEquals("L'unica regione selezionabile deve essere quella con id = Regione00", regione00, regioniSelezionabili.get(0)); 

		//secondo test
		List<Regione> regioniSelezionabili2 = controllerTurno2.trovaRegioniSelezionabiliPerSparatoria(); 
		assertEquals("La lista di regioniSelezionabili contiene due elementi", 2, regioniSelezionabili2.size()); 
		assertEquals("Una delle regioni selezionabili deve essere la regione12", true, regioniSelezionabili2.contains(regione12)); 
		assertEquals("Una delle regioni selezionabili deve essere la regione13", true, regioniSelezionabili2.contains(regione13)); 
	}

	@Test
	public void testSpostaPedina() {
		//primo test
		controllerTurno.spostaPedina(strada29);
		assertEquals("La nuova posizione del giocatore è strada29", strada29, statoPartita.getTurno().getPosizioneGiocatore()); 
		assertEquals("La nuova posizione del giocatore è strada29", strada29, statoPartita.getTurno().getGiocatore().getPosizioneGiocatore()); 
		assertEquals("La strada di destinazione non è più libera", false, strada29.getLibera()); 
		assertEquals("I danari del giocatore sono diminuiti di uno perchè la strada non è adiacente", 19, statoPartita.getTurno().getGiocatore().getDanari()); 
		assertEquals("Numero mosse rimanenti sono diminuite", 0, statoPartita.getTurno().getNumeroMosseRimanenti()); 
		assertEquals("In turno, pastoreMosso deve essere settato a true", true, statoPartita.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita.getTurno().getAnimaleMosso()); 

		//secondo test
		controllerTurno2.spostaPedina(strada22);
		assertEquals("La nuova posizione del giocatore è strada22", strada22, statoPartita2.getTurno().getPosizioneGiocatore()); 
		assertEquals("La nuova posizione del giocatore è strada22", strada22, statoPartita2.getTurno().getGiocatore().getPosizioneGiocatore()); 
		assertEquals("La strada di destinazione non è più libera", false, strada22.getLibera()); 
		assertEquals("I danari del giocatore sono diminuiti di uno perchè la strada non è adiacente", 29, statoPartita2.getTurno().getGiocatore().getDanari()); 
		assertEquals("Numero mosse rimanenti sono diminuite", 1, statoPartita2.getTurno().getNumeroMosseRimanenti()); 
		assertEquals("In turno, pastoreMosso deve essere settato a true", true, statoPartita2.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita2.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita2.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita2.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita2.getTurno().getAnimaleMosso()); 
	}

	@Test
	public void testCheckSparatoriaConsentita() {
		//primo test
		assertEquals("La lista non è vuota, quindi deve restituire true", true, controllerTurno.checkSparatoriaConsentita()); 

		//secondo test
		assertEquals("La lista non è vuota, quindi deve restituire true", true, controllerTurno2.checkSparatoriaConsentita()); 
	}

	@Test
	public void testTrovaRegioniSelezionabiliPerAccoppiamento() {
		//primo test
		List<Regione> regioniSelezionabili = controllerTurno.trovaRegioniSelezionabiliPerAccoppiamento();
		assertEquals("La lista di regioniSelezionabili contiene un solo elemento", 1, regioniSelezionabili.size()); 
		assertEquals("L'unica regione selezionabile deve essere quella con id = Regione00", regione00, regioniSelezionabili.get(0)); 

		//secondo test
		List<Regione> regioniSelezionabili2 = controllerTurno2.trovaRegioniSelezionabiliPerAccoppiamento();
		assertEquals("La lista di regioniSelezionabili è vuota", 0, regioniSelezionabili2.size()); 
	}


	@Test
	public void testCheckAccoppiamentoConsentito() {
		//primo test
		assertEquals("La lista non è vuota, quindi deve restituire true", true, controllerTurno.checkAccoppiamentoConsentito()); 

		//secondo test
		assertEquals("La lista è vuota, quindi deve restituire false", false, controllerTurno2.checkAccoppiamentoConsentito()); 
	}


	@Test
	public void testCheckSpostamentoPedinaConsentito() {
		//primo test
		assertEquals("Il giocatore ha più di 0 danari e le strade non sono tutte occupate, quindi la mossa è consentita", true, controllerTurno.checkSpostamentoPedinaConsentito());

		//secondo tst
		assertEquals("Il giocatore ha più di 0 danari e le strade non sono tutte occupate, quindi la mossa è consentita", true, controllerTurno2.checkSpostamentoPedinaConsentito());
	}



	@Test
	public void testCercaTessereTerrenoAcquistabiliDalGiocatore() {
		//primo test
		List<Integer> tessereAcquistabili = controllerTurno.cercaTessereTerrenoAcquistabiliDalGiocatore(); 
		assertEquals("La lista di tessereAcquistabili contiene un solo elemento", 1, tessereAcquistabili.size()); 
		assertEquals("L'unica tessere acquistabile deve essere di tipo 0", (Integer) 0, tessereAcquistabili.get(0));

		//secondo test
		List<Integer> tessereAcquistabili2 = controllerTurno2.cercaTessereTerrenoAcquistabiliDalGiocatore(); 
		assertEquals("La lista di tessereAcquistabili contiene due elementi, comunque dello stesso tipo", 2, tessereAcquistabili2.size()); 
		assertEquals("L'unica tessere acquistabile deve essere di tipo 5", (Integer) 5, tessereAcquistabili2.get(0));
		assertEquals("L'unica tessere acquistabile deve essere di tipo 5", (Integer) 5, tessereAcquistabili2.get(1));
	}

	@Test
	public void testCheckAcquistoTesseraTerrenoConsentito() {
		//primo test
		assertEquals("La lista non è vuota, quindi deve restituire true", true, controllerTurno.checkAcquistoTesseraTerrenoConsentito()); 

		//secondo test
		assertEquals("La lista non è vuota, quindi deve restituire true", true, controllerTurno2.checkAcquistoTesseraTerrenoConsentito()); 
	}

	@Test
	public void testEseguiAcquistoTesseraTerreno() {
		//primo test
		controllerTurno.eseguiAcquistoTesseraTerreno();
		assertEquals("Il giocatore ha acquistato una tessera, quindi ora ne ha una del tipo 0 (l'unico acquisto possibile)", (Integer) 1, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(0));
		assertEquals("Il giocatore non possiede tessere terreno di tipo 1", (Integer) 0, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(1)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 2", (Integer) 0, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(2)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 3", (Integer) 0, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(3)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 4", (Integer) 0, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(4)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 5", (Integer) 0, statoPartita.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(5)); 
		assertEquals("Il numero di danari è calato a 18", 18, statoPartita.getTurno().getGiocatore().getDanari()); 
		assertEquals("Il costo della tessera terreno di tipo 0 è salito a 3",  3, statoPartita.getCostoTesseraTerreno(0));

		//secondo test
		controllerTurno2.eseguiAcquistoTesseraTerreno();
		assertEquals("Il giocatore ha acquistato una tessera, quindi ora ne ha una del tipo 5 (l'unico acquisto possibile)", (Integer) 1, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(5));
		assertEquals("Il giocatore non possiede tessere terreno di tipo 0", (Integer) 0, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(0)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 1", (Integer) 0, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(1)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 2", (Integer) 0, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(2)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 3", (Integer) 0, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(3)); 
		assertEquals("Il giocatore non possiede tessere terreno di tipo 4", (Integer) 0, statoPartita2.getTurno().getGiocatore().getTessereTerrenoInPossesso().get(4)); 
		assertEquals("Il numero di danari è calato a 27", 27, statoPartita2.getTurno().getGiocatore().getDanari()); 
		assertEquals("Il costo della tessera terreno di tipo 5 è salito a 4",  4, statoPartita2.getCostoTesseraTerreno(5));
	}

	@Test
	public void testCheckSpostamentoAnimaleConsentito() {
		//primo test
		assertEquals("Nella regione00 ci sono sia una pecora, sia un ariete: la mossa è consentita", true, controllerTurno.checkSpostamentoAnimaleConsentito()); 

		//secondo test
		assertEquals("Nella regione12 c'è una pecora, nella regione13 c'è un ariete: la mossa è consentita", true, controllerTurno2.checkSpostamentoAnimaleConsentito()); 
	}

	@Test
	public void testDecrementaDanari() {
		controllerTurno2.decrementaDanari(4);
		assertEquals("Il numero di danari è calato a 26", 26, statoPartita2.getTurno().getGiocatore().getDanari()); 
	}

	@Test
	public void testIncrementaDanari() {
		controllerTurno2.incrementaDanari(4);
		assertEquals("Il numero di danari è salito a 34", 34, statoPartita2.getTurno().getGiocatore().getDanari()); 
	}

	@Test
	public void testDecrementaDanariGiocatore() {
		controllerTurno2.decrementaDanari(4, giocatore5);
		assertEquals("Il numero di danari è calato a 22", 22, giocatore5.getDanari()); 
	}

	@Test
	public void testIncrementaDanariGiocatore() {
		controllerTurno2.incrementaDanari(4, giocatore5);
		assertEquals("Il numero di danari è salito a 30", 30, giocatore5.getDanari()); 
	}

	@Test
	public void testDecrementaNumeroMosseRimanenti() {
		controllerTurno2.decrementaNumeroMosseRimanenti();
		assertEquals("Il numero di mosse rimanenti è sceso a 1", 1, statoPartita2.getTurno().getNumeroMosseRimanenti()); 
	}

	@Test
	public void testDecrementaTotaleAgnelliInRegione() {
		controllerTurno2.decrementaTotaleAgnelliInRegione(regione13);
		assertEquals("Il numero di agnelli presenti nella regione è sceso a 0", 0, regione13.getTotaleAgnelli()); 
	}

	@Test
	public void testIncrementaTotaleAgnelliInRegione() {
		controllerTurno2.incrementaTotaleAgnelliInRegione(regione13);
		assertEquals("Il numero di agnelli presenti nella regione è salito a 2", 2, regione13.getTotaleAgnelli()); 
	}

	@Test
	public void testDecrementaTotaleArietiInRegione() {
		controllerTurno2.decrementaTotaleArietiInRegione(regione13);
		assertEquals("Il numero di agnelli presenti nella regione è sceso a 0", 0, regione13.getTotaleArieti()); 
	}

	@Test
	public void testIncrementaTotaleArietiInRegione() {
		controllerTurno2.incrementaTotaleArietiInRegione(regione13);
		assertEquals("Il numero di agnelli presenti nella regione è salito a 2", 2, regione13.getTotaleArieti()); 
	}

	@Test
	public void testDecrementaTotalePecoreBiancheInRegione() {
		controllerTurno2.decrementaTotalePecoreBiancheInRegione(regione12);
		assertEquals("Il numero di pecore bianche presenti nella regione è sceso a 0", 0, regione12.getTotalePecoreBianche()); 
	}

	@Test
	public void testIncrementaTotalePecoreBiancheInRegione() {
		controllerTurno2.incrementaTotalePecoreBiancheInRegione(regione12);
		assertEquals("Il numero di pecore bianche presenti nella regione è salito a 2", 2, regione12.getTotalePecoreBianche()); 
	}

	@Test
	public void testLanciaDado() {
		int valore = controllerTurno.lancioDado(); 
		controllerTurno.setValoreLancioDado(valore);
		assertEquals("E' stato settato il valore nel controller turno ed è pari al valore generato", (Integer) valore, controllerTurno.getValoreLancioDado()); 
		assertEquals("Il valore è minore di 7", true, controllerTurno.getValoreLancioDado() < 7); 
		assertEquals("Il valore è maggiore di 0", true, controllerTurno.getValoreLancioDado() > 0); 
	}

	@Test
	public void testEffettuaSpostamentoAnimale() {
		statoPartita2.getTurno().setNumeroMosseRimanenti(3);
		statoPartita2.getTurno().setAnimaleMosso(false);
		controllerTurno2.setAnimaleScelto("PECORA_BIANCA");
		controllerTurno2.effettuaSpostamentoAnimale(regione12);
		assertEquals("Nella regione di partenza (regione12) non ci sono più pecore bianche", 0, regione12.getTotalePecoreBianche());
		assertEquals("Nella regione di partenza (regione13) c'è una pecora bianca", 1, regione13.getTotalePecoreBianche());
		assertEquals("Numero mosse rimanenti è diminuito di 1", 2, statoPartita2.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita2.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita2.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita2.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita2.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a true", true, statoPartita2.getTurno().getAnimaleMosso()); 

		statoPartita2.getTurno().setAnimaleMosso(false);
		controllerTurno2.setAnimaleScelto("ARIETE");
		controllerTurno2.effettuaSpostamentoAnimale(regione13);
		assertEquals("Nella regione di partenza (regione13) non ci sono più arieti", 0, regione13.getTotaleArieti());
		assertEquals("Nella regione di partenza (regione12) c'è un ariete", 1, regione12.getTotaleArieti());
		assertEquals("Numero mosse rimanenti è diminuito di 1", 1, statoPartita2.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita2.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita2.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita2.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita2.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a true", true, statoPartita2.getTurno().getAnimaleMosso()); 
	}

	@Test
	public void testEseguiAccoppiamento() {
		//primo test
		statoPartita.getTurno().setNumeroMosseRimanenti(3);
		controllerTurno.setValoreLancioDado(2);
		controllerTurno.eseguiAccoppiamento(regione00, controllerTurno.getValoreLancioDado());
		assertEquals("L'accoppiamento è andato a buon fine: totale agnelli = 1", 1, regione00.getTotaleAgnelli()); 
		assertEquals("Numero mosse rimanenti è diminuito di 1", 2, statoPartita.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a true", true, statoPartita.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita.getTurno().getAnimaleMosso()); 

		//secondo test
		statoPartita.getTurno().setAccoppiamentoEffettuato(false);
		controllerTurno.setValoreLancioDado(3);
		controllerTurno.eseguiAccoppiamento(regione00, controllerTurno.getValoreLancioDado());
		assertEquals("L'accoppiamento non è andato a buon fine: totale agnelli rimane a 1", 1, regione00.getTotaleAgnelli()); 
		assertEquals("Numero mosse rimanenti è diminuito di 1", 1, statoPartita.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a false", false, statoPartita.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a true", true, statoPartita.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita.getTurno().getAnimaleMosso()); 
	}

	@Test
	public void testEseguiSparatoria() {
		statoPartita.getTurno().setNumeroMosseRimanenti(3);
		controllerTurno.setValoreLancioDado(3);
		controllerTurno.setAnimaleScelto("ARIETE");
		controllerTurno.eseguiSparatoria(regione00, controllerTurno.getAnimaleScelto(), controllerTurno.getValoreLancioDado()); 
		assertEquals("La sparatoria non è andata a buon fine: totale arieti rimane = 1", 1, regione00.getTotaleArieti());
		assertEquals("Numero mosse rimanenti è diminuito di 1", 2, statoPartita.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a true", true, statoPartita.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita.getTurno().getAnimaleMosso()); 

		statoPartita.getTurno().setSparatoriaEffettuata(false);
		controllerTurno.setValoreLancioDado(2);
		controllerTurno.setAnimaleScelto("ARIETE");
		controllerTurno.eseguiSparatoria(regione00, controllerTurno.getAnimaleScelto(), controllerTurno.getValoreLancioDado()); 
		assertEquals("La sparatoria è andata a buon fine: totale arieti = 0", 0, regione00.getTotaleArieti());
		assertEquals("Numero mosse rimanenti è diminuito di 1", 1, statoPartita.getTurno().getNumeroMosseRimanenti());
		assertEquals("In turno, pastoreMosso deve essere settato a false", false, statoPartita.getTurno().getPastoreMosso()); 
		assertEquals("In turno, terrenoAcquistato deve essere settato a false", false, statoPartita.getTurno().getTerrenoAcquistato()); 
		assertEquals("In turno, sparatoriaEffettuata deve essere settato a true", true, statoPartita.getTurno().getSparatoriaEffettuata()); 
		assertEquals("In turno, accoppiamentoEffettuato deve essere settato a false", false, statoPartita.getTurno().getAccoppiamentoEffettuato()); 
		assertEquals("In turno, animaleMosso deve essere settato a false", false, statoPartita.getTurno().getAnimaleMosso()); 
	}

	@Test
	public void testIncrementaCostoTesseraTerreno() {
		controllerTurno.incrementaCostoTesseraTerreno(0);
		assertEquals("Il costo è aumentato a 3", 3, statoPartita.getCostoTesseraTerreno(0));
	}

	@Test
	public void testIncrementaNumeroRecintiUtilizzati() {
		controllerTurno.incrementaNumeroRecintiUtilizzati();
		assertEquals("Il numero di recinti utilizzati è salito a 1", 1, statoPartita.getNumeroRecintiUtilizzati()); 
	}

	@Test
	public void testMostraAnimaliSelezionabiliPerSparatoria() {
		List<Integer> animaliSelezionabili = controllerTurno2.mostraAnimaliSelezionabiliPerSparatoria(regione12); 
		assertEquals("La lista contiene un solo elemento", 1, animaliSelezionabili.size()); 
		assertEquals("L'animale selezionabile è una pecora bianca (intero corrispondente: 0)", (Integer) 0, animaliSelezionabili.get(0));

		List<Integer> animaliSelezionabili2 = controllerTurno2.mostraAnimaliSelezionabiliPerSparatoria(regione13); 
		assertEquals("La lista contiene un solo elemento", 1, animaliSelezionabili2.size()); 
		assertEquals("L'animale selezionabile è un ariete (intero corrispondente: 1)", (Integer) 1, animaliSelezionabili2.get(0));
	}

	@Test
	public void testMostraAnimaliSelezionabiliPerSpostamento() {
		List<Integer> animaliSelezionabili = controllerTurno.mostraAnimaliSelezionabiliPerSpostamento(regione00); 
		assertEquals("La lista contiene due elementi", 2, animaliSelezionabili.size()); 
		assertEquals("Un animale selezionabile è una pecora bianca (intero corrispondente: 0)", true, animaliSelezionabili.contains(0));
		assertEquals("Un animale selezionabile è un ariete (intero corrispondente: 1)", true, animaliSelezionabili.contains(1));
	}

	@Test
	public void testMostraStradeSelezionabili() {
		List<String> idStradeSelezionabili = controllerTurno.mostraStradeSelezionabili(); 
		assertEquals("Ci sono 39 strade libere", 39, idStradeSelezionabili.size()); 
		assertEquals("Non contiene la strada00", false, idStradeSelezionabili.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada00)));
		assertEquals("Non contiene la strada02", false, idStradeSelezionabili.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada02)));
		assertEquals("Non contiene la strada03", false, idStradeSelezionabili.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada03)));
		controllerTurno.spostaPedina(strada29);

		List<String> idStradeSelezionabili2 = controllerTurno.mostraStradeSelezionabili(); 
		assertEquals("Ci sono 38 strade libere", 38, idStradeSelezionabili2.size()); 
		assertEquals("Non contiene la strada00", false, idStradeSelezionabili2.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada00)));
		assertEquals("Non contiene la strada02", false, idStradeSelezionabili2.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada02)));
		assertEquals("Non contiene la strada03", false, idStradeSelezionabili2.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada03)));
		assertEquals("Non contiene la strada29", false, idStradeSelezionabili2.contains(statoPartita.getMappa().getMappaVerticiInversa().get(strada29)));	
	}

	@Test
	public void testMutazioneAgnello() {
		controllerTurno2.mutazioneAgnello(regione13);
		assertEquals("Non ci sono più agnelli nella regione", 0, regione13.getTotaleAgnelli()); 
		assertEquals("L'agnello si è trasformato in pecora bianca o in ariete", 2, regione13.getTotaleArieti() + regione13.getTotalePecoreBianche());
		
		regione13.setTotaleAgnelli(1); 
		controllerTurno2.mutazioneAgnello(regione13); 
		assertEquals("Non ci sono più agnelli nella regione", 0, regione13.getTotaleAgnelli()); 
		assertEquals("L'agnello si è trasformato in pecora bianca o in ariete", 3, regione13.getTotaleArieti() + regione13.getTotalePecoreBianche());
	
	}	
	
	@Test
	public void testResettaValori() {
		controllerTurno.resettaValori(); 
		assertEquals("L'attributo sceltaPosizioneEffettuata è settato a false", false, controllerTurno.getSceltaPosizioneEffettuata()); 
		assertEquals("L'attributo animaleScelto è settato a null", null, controllerTurno.getAnimaleScelto()); 
		assertEquals("L'attributo giocatoreCheScegliePosizione è settato a null", null, controllerTurno.getGiocatoreCheScegliePosizione()); 
		assertEquals("L'attributo assegnata è settato a false", false, controllerTurno.getAssegnata()); 
		assertEquals("L'attributo mossaScelta è settato a null", null, controllerTurno.getMossaScelta()); 
		assertEquals("L'attributo regioneScelta è settato a null", null, controllerTurno.getRegioneScelta()); 
		assertEquals("L'attributo stradaScelta è settato a null", null, controllerTurno.getStradaScelta()); 
		assertEquals("L'attributo tipoTerrenoScelto è settato a null", null, controllerTurno.getTipoTerrenoScelto()); 
	}
	
	@Test
	public void testScegliStradePartenza() {
		controllerTurno.scegliStradePartenza();
		assertEquals("Il numero di strade da selezionare è 3", 3, controllerTurno.getNumeroStradeAncoraDaSelezionare()); 
		
		controllerTurno2.scegliStradePartenza();
		assertEquals("Il numero di strade da selezionare è 4", 4, controllerTurno2.getNumeroStradeAncoraDaSelezionare()); 
		
	}
	
	@Test
	public void testSpostamentoAutomaticoPecoraNera() {
		statoPartita.setPosizionePecoraNera(regione00);
		strada00.setLiberaFalse();
		strada02.setLiberaFalse();
		strada03.setLiberaFalse();
		controllerTurno.setValoreLancioDado(2);
		controllerTurno.spostamentoAutomaticoPecoraNera(controllerTurno.getValoreLancioDado());
		assertEquals("La pecora nera si trova nella regione00", regione00, statoPartita.getPosizionePecoraNera()); 
		
		controllerTurno.setValoreLancioDado(1);
		controllerTurno.spostamentoAutomaticoPecoraNera(controllerTurno.getValoreLancioDado());
		assertEquals("La pecora nera è rimasta nella regione00", regione00, statoPartita.getPosizionePecoraNera());
	}
	
	@Test
	public void testSvolgimentoTurno() {
		controllerTurno.svolgimentoTurno(); 
		assertEquals("L'attributo sceltaPosizioneEffettuata è settato a false", false, controllerTurno.getSceltaPosizioneEffettuata()); 
		assertEquals("L'attributo animaleScelto è settato a null", null, controllerTurno.getAnimaleScelto()); 
		assertEquals("L'attributo giocatoreCheScegliePosizione è settato a null", null, controllerTurno.getGiocatoreCheScegliePosizione()); 
		assertEquals("L'attributo assegnata è settato a false", false, controllerTurno.getAssegnata()); 
		assertEquals("L'attributo mossaScelta è settato a null", null, controllerTurno.getMossaScelta()); 
		assertEquals("L'attributo regioneScelta è settato a null", null, controllerTurno.getRegioneScelta()); 
		assertEquals("L'attributo stradaScelta è settato a null", null, controllerTurno.getStradaScelta()); 
		assertEquals("L'attributo tipoTerrenoScelto è settato a null", null, controllerTurno.getTipoTerrenoScelto());
		assertEquals("Il valore del lancio del dado è diverso da 0", true, controllerTurno.getValoreLancioDado() != 0); 

	}
	
	@Test
	public void testEseguiTurno() {
		statoPartita.getTurno().setPosizioneGiocatore(null);
		controllerTurno.eseguiTurno();
		assertEquals("La posizione del giocatore nel turno è diversa da null", true, statoPartita.getTurno().getPosizioneGiocatore() != null); 
		assertEquals("La posizione del giocatore nel turno è strada00", strada00, statoPartita.getTurno().getPosizioneGiocatore()); 
		assertEquals("La variabile selezioneStradaNecessaria è false", false, statoPartita.getTurno().getSelezioneStradaNecessaria());
		
		statoPartita2.getTurno().setPosizioneGiocatore(null);
		controllerTurno2.eseguiTurno();
		assertEquals("La posizione del giocatore nel turno è = null", false, statoPartita2.getTurno().getPosizioneGiocatore() != null); 
	}
	
	@Test
	public void testSpostamentoAutomaticoLupo() {
		statoPartita.setPosizioneLupo((Regione) statoPartita.getMappa().getMappaVertici().get("Sheepsburg"));
		
		controllerTurno.spostamentoAutomaticoLupo();
		assertEquals("La posizione del lupo non è più Sheepsburg", false, statoPartita.getPosizioneLupo().equals((Regione) statoPartita.getMappa().getMappaVertici().get("Sheepsburg")));
		assertEquals("La somma delle pecore bianche e degli arieti nella regione del lupo è scesa a 1", 1, statoPartita.getPosizioneLupo().getTotaleArieti() + statoPartita.getPosizioneLupo().getTotalePecoreBianche());
		
		
		statoPartita2.setPosizioneLupo((Regione) statoPartita2.getMappa().getMappaVertici().get("Sheepsburg"));
		List<Strada> stradeAdiacenti = statoPartita2.getMappa().getStradeAdiacentiARegione(statoPartita2.getPosizioneLupo());
		for(Strada strada: stradeAdiacenti) {
			strada.setLiberaFalse();
			List<Regione> regioniAdiacenti = statoPartita2.getMappa().getRegioniAdiacentiAStrada(strada); 
			if(regioniAdiacenti.get(0) != statoPartita2.getPosizioneLupo()) {
				regioniAdiacenti.get(0).setTotaleAgnelli(0);
				regioniAdiacenti.get(0).setTotaleArieti(1);
				regioniAdiacenti.get(0).setTotalePecoreBianche(1);
			} else {
				regioniAdiacenti.get(1).setTotaleAgnelli(0);
				regioniAdiacenti.get(1).setTotaleArieti(1);
				regioniAdiacenti.get(1).setTotalePecoreBianche(1);
			}
		}
		controllerTurno2.spostamentoAutomaticoLupo();
		assertEquals("La posizione del lupo non è più Sheepsburg", false, statoPartita2.getPosizioneLupo().equals((Regione) statoPartita2.getMappa().getMappaVertici().get("Sheepsburg")));
		assertEquals("La somma delle pecore bianche e degli arieti nella regione del lupo è scesa a 1", 1, statoPartita2.getPosizioneLupo().getTotaleArieti() + statoPartita2.getPosizioneLupo().getTotalePecoreBianche());
		
	}
	
	
}
