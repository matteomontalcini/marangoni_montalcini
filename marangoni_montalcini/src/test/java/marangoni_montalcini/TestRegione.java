package marangoni_montalcini;

import static org.junit.Assert.assertEquals;
import model.Regione;

import org.junit.Before;
import org.junit.Test;

public class TestRegione {
	
	private Regione regioneTest;
	
	@Before
	public void setUpTestRegione(){
		regioneTest = new Regione(2);
	}

	@Test
	public void testRegione() {
		assertEquals("Il tipo di terreno è 2", 2, regioneTest.getTipoDiTerreno());
		assertEquals("Il numero di pecore bianche dev'essere settato ad 1", 1 , regioneTest.getTotalePecoreBianche());
		assertEquals("Il numero di arieti dev'essere settato ad 1", 1 , regioneTest.getTotaleArieti());
		assertEquals("Il numero di agnelli dev'essere settato ad 0", 0 , regioneTest.getTotaleAgnelli());
	}

}
