package marangoni_montalcini;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import model.Giocatore;
import model.Turno;

import org.junit.Before;
import org.junit.Test;

public class TestTurno {

	static Date data = new Date(); 
	private Giocatore giocatoreTest;
	private Turno turnoTest;
	
	@Before
	public void setUpTestTurno(){
		data.setDate(22);
		data.setMonth(4);
		data.setYear(114);
		giocatoreTest = new Giocatore("Matteo", data,2);
		turnoTest = new Turno(giocatoreTest);
	}
	
	@Test
	public void testTurno() {
		assertEquals("Il nome del giocatore dev'essere Matteo", "Matteo", turnoTest.getGiocatore().getNomeGiocatore());
		assertEquals("La data dev'essere uguale a 22-05-2014", data, giocatoreTest.getUltimaCarezza());

	}

}
