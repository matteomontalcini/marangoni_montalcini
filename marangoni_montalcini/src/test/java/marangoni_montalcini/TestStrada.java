package marangoni_montalcini;

import static org.junit.Assert.assertEquals;
import model.Strada;

import org.junit.Before;
import org.junit.Test;

public class TestStrada {

	private Strada stradaTest;
	
	@Before
	public void setUpTestStrada(){
		stradaTest = new Strada(2, 20, 30);
	}

	@Test
	public void testStrada() {
		assertEquals("Il numero di strada dev'essere uguale a quello passato, in questo caso 2", 2 , stradaTest.getNumeroStrada());
		assertEquals("La strada dev'essere libera", true, stradaTest.getLibera());
	}
	
}
