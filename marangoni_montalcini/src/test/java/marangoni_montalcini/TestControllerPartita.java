package marangoni_montalcini;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Giocatore;
import model.Regione;
import model.StatoPartita;
import model.Strada;
import model.Turno;

import org.junit.Before;
import org.junit.Test;

import view.ImplementazioneViewFittizia;
import view.InterfacciaView;
import controller.ControllerPartita;
import eventi.AnimaleSelezionato;
import eventi.ConfermaRicezioneAvvisoNoMosse;
import eventi.MossaSelezionata;
import eventi.RegioneSelezionata;
import eventi.StradaSelezionata;
import eventi.TesseraTerrenoSelezionata;

public class TestControllerPartita {
	
	StatoPartita statoPartita1, statoPartita2, statoPartita3, statoPartita4;
	
	ControllerPartita controllerPartita1 , controllerPartita2, controllerPartita3, controllerPartita4;
	
	ImplementazioneViewFittizia view;
	
	Giocatore giocatore1, giocatore2, giocatore3, giocatore4, giocatore5, giocatore6, giocatore7, giocatore8, giocatoreTurnoSuccessivo;
	
	Date data, data1, data2, data3, data4, data5, data6, data7, data8;
	
	//regioni dichiarate in ordine per tipo di terreno
	Regione regione00, regione05, regione01, regione14, regione10, regione12, sheepsburg;
	
	//regioni di tipo uno e due dichiarate per il terzo test
	Regione regione06, regione02, regione15;
	
	Strada strada00, strada01, strada02, strada03, strada04, strada05, strada06, strada07, strada08;
	
	List<Date> date = new ArrayList<Date>();
	
	List<String> stradeAncoraSelezionabili = new ArrayList<String>();
	
	Turno turno1;
	
	Strada strada25, strada35; 

	@Before
	public void setUp() throws Exception {
		//variabili per primo test
		data1 = new Date();
		data1.setDate(12);
		data2 = new Date();
		data2.setDate(11);
		data3 = new Date();
		data3.setDate(10);
		giocatore1 = new Giocatore("Matteo", data1, 3);
		giocatore2 = new Giocatore("Daniele", data2, 3);
		giocatore3 = new Giocatore("Luca", data3, 3);
		giocatore1.setDanari(1);
		giocatore2.setDanari(2);
		giocatore3.setDanari(3);
		statoPartita1 = new StatoPartita(3);
		statoPartita1.addGiocatore(giocatore1);
		statoPartita1.addGiocatore(giocatore2);
		statoPartita1.addGiocatore(giocatore3);
		statoPartita1.inizializzaCostiTessereTerreno();
		view = new ImplementazioneViewFittizia();
		controllerPartita1 = new ControllerPartita(statoPartita1, view);
		turno1 = new Turno(giocatore1); 
		strada25 = (Strada) statoPartita1.getMappa().getMappaVertici().get("Strada25"); 
		strada35 = (Strada) statoPartita1.getMappa().getMappaVertici().get("Strada35");
		giocatore1.setPosizioneGiocatore(strada25);
		turno1.setPosizioneGiocatore(giocatore1.getPosizioneGiocatore());
		statoPartita1.setTurno(turno1);
		
		regione00 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione00");
		regione05 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione05");
		regione01 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione01");
		regione14 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione14");
		regione10 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione10");
		regione12 = (Regione) statoPartita1.getMappa().getMappaVertici().get("Regione12");
		sheepsburg = (Regione) statoPartita1.getMappa().getMappaVertici().get("Sheepsburg");
		
		strada00 = (Strada) statoPartita1.getMappa().getMappaVertici().get("Strada00");
		strada01 = (Strada) statoPartita1.getMappa().getMappaVertici().get("Strada01");
		strada02 = (Strada) statoPartita1.getMappa().getMappaVertici().get("Strada02");
		
		//variabili per secondo test
		data4 = new Date();
		data4.setDate(11);
		data5 = new Date();
		data5.setDate(12);
		giocatore4 = new Giocatore("Matteo", data4, 2);
		giocatore5 = new Giocatore("Daniele", data5, 2);
		statoPartita2 = new StatoPartita(2);
		statoPartita2.addGiocatore(giocatore4);
		statoPartita2.addGiocatore(giocatore5);
		controllerPartita2 = new ControllerPartita(statoPartita2, view);
		
		strada03 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada03");
		strada04 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada04");
		strada05 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada05");
		strada06 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada06");
		strada07 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada07");
		strada08 = (Strada) statoPartita2.getMappa().getMappaVertici().get("Strada08");
		
		
		
		//variabili per il terzo test
		data6 = new Date();
		data6.setDate(11);
		data7 = new Date();
		data7.setDate(10);
		data8 = new Date();
		data8.setDate(12);
		giocatore6 = new Giocatore("Matteo", data6, 3);
		giocatore7 = new Giocatore("Daniele", data7, 3);
		giocatore8 = new Giocatore("Luca", data8, 3);
		statoPartita3 = new StatoPartita(3);
		statoPartita3.addGiocatore(giocatore6);
		statoPartita3.addGiocatore(giocatore7);
		statoPartita3.addGiocatore(giocatore8);
		controllerPartita3 = new ControllerPartita(statoPartita3, view);
		giocatore6.setPunteggioTotalizzato(10);
		giocatore7.setPunteggioTotalizzato(11);
		giocatore8.setPunteggioTotalizzato(12);
		
		regione06 = (Regione) statoPartita3.getMappa().getMappaVertici().get("Regione06");
		regione02 = (Regione) statoPartita3.getMappa().getMappaVertici().get("Regione02");
		regione15 = (Regione) statoPartita3.getMappa().getMappaVertici().get("Regione15");
		
		//variabili per il test con lo statoPartita online
		data = new Date();
		statoPartita4 = new StatoPartita(2);
		controllerPartita4 = new ControllerPartita(statoPartita4, view);
	}
	
	@Test
	public void testAssegnaPosizioni() {
		//primo test
		assertEquals("La strada del giocatore 1 è settata a strada25", strada25, giocatore1.getPosizioneGiocatore());
	
		assertEquals("La strada del giocatore 2 è settata a null", null, giocatore2.getPosizioneGiocatore());
		controllerPartita1.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore2);
		controllerPartita1.assegnaPosizioni(strada01);
		assertEquals("La strada del giocatore 2 è settata a strada01", strada01, giocatore2.getPosizioneGiocatore());
		assertEquals("La strada01 ha la variabile booleana 'libera' settata a false", false, strada01.getLibera());
	
		assertEquals("La strada del giocatore 3 è settata a null", null, giocatore3.getPosizioneGiocatore());
		controllerPartita1.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore3);
		controllerPartita1.assegnaPosizioni(strada02);
		assertEquals("La strada del giocatore 3 è settata a strada02", strada02, giocatore3.getPosizioneGiocatore());
		assertEquals("La strada02 ha la variabile booleana 'libera' settata a false", false, strada02.getLibera());
	
		//secondo test
		assertEquals("La strada del giocatore 4 è settata a null", null, giocatore4.getPosizioneGiocatore());
		controllerPartita2.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore4);
		controllerPartita2.assegnaPosizioni(strada03);
		assertEquals("La strada del giocatore 4 è settata a strada03", strada03, giocatore4.getPosizioneGiocatore());
		assertEquals("La strada03 ha la variabile booleana 'libera' settata a false", false, strada03.getLibera());

		assertEquals("La strada del giocatore 5 è settata a null", null, giocatore5.getPosizioneGiocatore());
		controllerPartita2.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore5);
		controllerPartita2.assegnaPosizioni(strada04);
		assertEquals("La strada del giocatore 5 è settata a strada04", strada04, giocatore5.getPosizioneGiocatore());
		assertEquals("La strada04 ha la variabile booleana 'libera' settata a false", false, strada04.getLibera());

		assertEquals("La seconda strada del giocatore 4 è settata a null", null, giocatore4.getPosizione2Giocatore());
		controllerPartita2.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore4);
		controllerPartita2.assegnaPosizioni(strada05);
		assertEquals("La strada del giocatore 4 è settata a strada05", strada05, giocatore4.getPosizione2Giocatore());
		assertEquals("La strada05 ha la variabile booleana 'libera' settata a false", false, strada05.getLibera());

		assertEquals("La seconda strada del giocatore 5 è settata a null", null, giocatore5.getPosizione2Giocatore());
		controllerPartita2.getControllerTurno().setGiocatoreCheScegliePosizione(giocatore5);
		controllerPartita2.assegnaPosizioni(strada06);
		assertEquals("La strada del giocatore 5 è settata a strada06", strada06, giocatore5.getPosizione2Giocatore());
		assertEquals("La strada06 ha la variabile booleana 'libera' settata a false", false, strada06.getLibera());
	}
	
	@Test
	public void testCostruisciNuovoTurno() {
		//primo test
		statoPartita1.setTurno(new Turno(giocatore3));
		controllerPartita1.getControllerTurno().setFaseFinale(true);
		controllerPartita1.stabilisciPrimoGiocatore();
		controllerPartita1.costruisciNuovoTurno();
		assertEquals("Il punteggio del giocatore1 è pari ai suoi danari ovvero 1", 1, giocatore1.getPunteggioTotalizzato());
		assertEquals("Il punteggio del giocatore2 è pari ai suoi danari ovvero 2", 2, giocatore2.getPunteggioTotalizzato());
		assertEquals("Il punteggio del giocatore3 è pari ai suoi danari ovvero 3", 3, giocatore3.getPunteggioTotalizzato());
		
		//secondo test
		statoPartita2.setTurno(new Turno(giocatore4));
		giocatore5.setPosizioneGiocatore(strada03);
		controllerPartita2.costruisciNuovoTurno();
		assertEquals("Il giocatore del turno è stato resettato", giocatore5, statoPartita2.getTurno().getGiocatore());
		assertEquals("La posizione del giocatore dev'essere la strada03", strada03, statoPartita2.getTurno().getPosizioneGiocatore());
		assertEquals("La selezione della strada necessaria è false", false, statoPartita2.getTurno().getSelezioneStradaNecessaria());
		assertEquals("Il valore del lancio del dado dev'essere pari a 0", 0, statoPartita2.getTurno().getValoreLancioDado());
		assertEquals("Il valore della variabile booleana pastore mosso dev'essere settato a false", false, statoPartita2.getTurno().getPastoreMosso());
		assertEquals("Il valore della variabile booleana animale mosso dev'essere settato a false", false, statoPartita2.getTurno().getAnimaleMosso());
		assertEquals("Il valore della variabile booleana terreno acquistato dev'essere settato a false", false, statoPartita2.getTurno().getTerrenoAcquistato());
		assertEquals("Il valore della variabile booleana pastore mosso dev'essere settato a false", false, statoPartita2.getTurno().getPastoreMosso());
		assertEquals("Il valore della variabile booleana accoppiamento effettuato dev'essere settato a false", false, statoPartita2.getTurno().getAccoppiamentoEffettuato());
		assertEquals("Il valore della variabile booleana sparatoria effettuata dev'essere settato a false", false, statoPartita2.getTurno().getSparatoriaEffettuata());
		assertEquals("Il valore della variabile numero mosse disponibili dev'essere settata a 3", 3, statoPartita2.getTurno().getNumeroMosseRimanenti());
	}

	@Test
	public void testDistribuisciDanari() {
		//primo test
		controllerPartita1.distribuisciDanari(statoPartita1.getNumeroGiocatori());
		assertEquals("Il giocatore1 deve avere 20 danari", 20, giocatore1.getDanari());
		assertEquals("Il giocatore2 deve avere 20 danari", 20, giocatore2.getDanari());
		assertEquals("Il giocatore3 deve avere 20 danari", 20, giocatore3.getDanari());
		
		//secondo test
		controllerPartita2.distribuisciDanari(statoPartita2.getNumeroGiocatori());
		assertEquals("Il giocatore4 deve avere 30 danari", 30, giocatore4.getDanari());
		assertEquals("Il giocatore5 deve avere 30 danari", 30, giocatore5.getDanari());
	}
	
	@Test
	public void testDistribuisciTessereIniziali() {
		controllerPartita1.distribuisciTessereIniziali();
		assertEquals("Il giocatore deve avere la somma del numero delle tessere terreno = 1", 1, giocatore1.getTessereTerrenoInPossesso().get(0) + giocatore1.getTessereTerrenoInPossesso().get(1) + giocatore1.getTessereTerrenoInPossesso().get(2) + giocatore1.getTessereTerrenoInPossesso().get(3) + giocatore1.getTessereTerrenoInPossesso().get(4) + giocatore1.getTessereTerrenoInPossesso().get(5));
		assertEquals("Il giocatore deve avere la somma del numero delle tessere terreno = 1", 1, giocatore2.getTessereTerrenoInPossesso().get(0) + giocatore2.getTessereTerrenoInPossesso().get(1) + giocatore2.getTessereTerrenoInPossesso().get(2) + giocatore2.getTessereTerrenoInPossesso().get(3) + giocatore2.getTessereTerrenoInPossesso().get(4) + giocatore2.getTessereTerrenoInPossesso().get(5));
		assertEquals("Il giocatore deve avere la somma del numero delle tessere terreno = 1", 1, giocatore3.getTessereTerrenoInPossesso().get(0) + giocatore3.getTessereTerrenoInPossesso().get(1) + giocatore3.getTessereTerrenoInPossesso().get(2) + giocatore3.getTessereTerrenoInPossesso().get(3) + giocatore3.getTessereTerrenoInPossesso().get(4) + giocatore3.getTessereTerrenoInPossesso().get(5));
		int indexTesseraTerrenoPrimoGiocatore = -1, indexTesseraTerrenoSecondoGiocatore = -1, indexTesseraTerrenoTerzoGiocatore = -1;
		boolean inizializzata1 = false, inizializzata2 = false, inizializzata3 = false;
		for(int i=0; i < giocatore1.getTessereTerrenoInPossesso().size(); i++ ){
			if(giocatore1.getTessereTerrenoInPossesso().get(i) == 1) {
				indexTesseraTerrenoPrimoGiocatore = i;
				inizializzata1 = true;
			} else if(giocatore2.getTessereTerrenoInPossesso().get(i) == 1) {
				indexTesseraTerrenoSecondoGiocatore = i;
				inizializzata2 = true;
			} else if(giocatore3.getTessereTerrenoInPossesso().get(i) == 1) {
				indexTesseraTerrenoTerzoGiocatore = i;
				inizializzata3 = true;
			}
		}
		assertEquals("La tessera terreno del primo giocatore dev'essere stata inizializzata", true, inizializzata1);
		assertEquals("La tessera terreno del secondo giocatore dev'essere stata inizializzata", true, inizializzata2);
		assertEquals("La tessera terreno del terzo giocatore dev'essere stata inizializzata", true, inizializzata3);
		assertEquals("Il giocatore1 ed il giocatore2 non devono avere la stessa tessera terreno", false, indexTesseraTerrenoPrimoGiocatore == indexTesseraTerrenoSecondoGiocatore);
		assertEquals("Il giocatore1 ed il giocatore3 non devono avere la stessa tessera terreno", false, indexTesseraTerrenoPrimoGiocatore == indexTesseraTerrenoTerzoGiocatore);
		assertEquals("Il giocatore2 ed il giocatore3 non devono avere la stessa tessera terreno", false, indexTesseraTerrenoSecondoGiocatore == indexTesseraTerrenoTerzoGiocatore);
	}

	@Test
	public void testInserisciGiocatore() {
		controllerPartita4.inserisciGiocatore(0, "Matteo", data, 2);
		assertEquals("L'Hash map contiene solo 1 giocatore", 1, statoPartita4.getGiocatoriOnline().size());
		assertEquals("Il nome del giocatore è Matteo", "Matteo", statoPartita4.getGiocatoriOnline().get(0).getNomeGiocatore());
		assertEquals("I danari sono 30", 30, statoPartita4.getGiocatoriOnline().get(0).getDanari());
		assertEquals("La data dell'ultima carezza è stata settata", data, statoPartita4.getGiocatoriOnline().get(0).getUltimaCarezza());
		controllerPartita4.inserisciGiocatore(1, "Daniele", data, 2);
		assertEquals("Il nome del giocatore è Daniele", "Daniele", statoPartita4.getGiocatoriOnline().get(1).getNomeGiocatore());
		assertEquals("I danari sono 30", 30, statoPartita4.getGiocatoriOnline().get(1).getDanari());
		assertEquals("La data dell'ultima carezza è stata settata", data, statoPartita4.getGiocatoriOnline().get(1).getUltimaCarezza());
	}
	
	@Test
	public void testRestituisciPunteggiGiocatori() {
		List<Integer> punteggiGiocatori = controllerPartita3.restituisciPunteggiGiocatori();
		assertEquals("Ci devono essere solo 3 elementi", 3, punteggiGiocatori.size());
		assertEquals("Il primo elemento dev'essere = 10", (Integer) 10, punteggiGiocatori.get(0));
		assertEquals("Il primo elemento dev'essere = 11", (Integer) 11, punteggiGiocatori.get(1));
		assertEquals("Il primo elemento dev'essere = 12", (Integer) 12, punteggiGiocatori.get(2));
	}
	
	@Test
	public void testSetPunteggioGiocatori() {
		giocatore6.setDanari(2);
		giocatore7.setDanari(10);
		giocatore8.setDanari(20);
		
		giocatore6.addTesseraTerrenoInPossesso(0);
		giocatore6.addTesseraTerrenoInPossesso(0);
		
		giocatore7.addTesseraTerrenoInPossesso(0);
		giocatore7.addTesseraTerrenoInPossesso(1);
		giocatore7.addTesseraTerrenoInPossesso(2);
		controllerPartita3.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione06); //tipo1
		controllerPartita3.getControllerTurno().incrementaTotaleArietiInRegione(regione02); //tipo2
		
		giocatore8.addTesseraTerrenoInPossesso(4);
		giocatore8.addTesseraTerrenoInPossesso(4);
		statoPartita3.setPosizionePecoraNera(regione15);
		
		controllerPartita3.setPunteggioGiocatori();
		
		assertEquals("Il giocatore6 ha 2 danari + in ogni regione di tipo 0 ci sono 1 pecora bianca ed 1 ariete, le regioni di tipo 0 sono 3. Le pecore bianche totali nel tipo di terreno0 sono 3, gli arieti totali nel tipo di terreno0 sono 3. Quindi, avendo il giocatore 2 tessere del tipo di terreno0 (non ho distribuito le tessere iniziale) il punteggio dovrà essere 14 (2danari+ 2*3 + 2*3)", 14, giocatore6.getPunteggioTotalizzato());
		
		assertEquals("Il giocatore7 ha 10 danari + in ogni regione di tipo 0 ci sono 1 pecora bianca ed 1 ariete, le regioni di tipo 0 sono 3. Le pecore bianche totali nel tipo di terreno0 sono 3, gli arieti totali nel tipo di terreno0 sono 3. "
				+ "in ogni regione di tipo 1 ci sono 1 pecora bianca (tranne che nella regione05 in cui ce ne sono 2) ed 1 ariete, le regioni di tipo 1 sono 3. Le pecore bianche totali nel tipo di terreno1 sono 4, gli arieti totali nel tipo di terreno1 sono 3."
				+ "in ogni regione di tipo 2 ci sono 1 pecora bianca ed un ariete (tranne che nella regione01), le regione di tipo 2 sono 3. Le pecore totali nel tipo di terreno2 sono 3, gli arieti totali nel tipo di terreno 2 sono 4. "
				+ "Quindi, avendo il giocatore 1 tessera del tipo di terreno1, una di tipo terreno0, una di tipo terreno1, una di tipo terreno2 (non ho distribuito le tessere iniziale) il punteggio dovrà essere 14 (10 danari+ 3*1 + 3*1 + 4*1 + 3*1 + 3*1 + 4*1)", 30, giocatore7.getPunteggioTotalizzato());
		assertEquals("Il giocatore8 ha 20 danari + in ogni regione di tipo 4 ci sono 1 pecora bianca ed 1 ariete. Ci sono 3 regioni di 4. Il giocatore ha 2 tessere di tipo 4. In una delle regioni di tipo 4 c'è la pecora nera che vale 2. Il punteggio dovrà essere 36 (20 danari+ 5*2 + 3*2)", 36, giocatore8.getPunteggioTotalizzato());
	}

	@Test
	public void testStabilisciPrimoGiocatore() {
		//primo test
		controllerPartita1.stabilisciPrimoGiocatore();
		assertEquals("Il primo giocatore dev'essere quello che ha accarezzato la pecora più recentemente, il giocatore1", giocatore1, statoPartita1.getPrimoGiocatore());
		
		//secondo test
		controllerPartita2.stabilisciPrimoGiocatore();
		assertEquals("Il primo giocatore dev'essere quello che ha accarezzato la pecora più recentemente, il giocatore5", giocatore5, statoPartita2.getPrimoGiocatore());
		
		//terzo test
		controllerPartita3.stabilisciPrimoGiocatore();
		assertEquals("Il primo giocatore dev'essere quello che ha accarezzato la pecora più recentemente, il giocatore8", giocatore8, statoPartita3.getPrimoGiocatore());
	}

	@Test
	public void testTotaleArietiInTipoDiTerreno() {
		//test su tipo di terreno 6, tipo di terreno associato a sheepsburg
		assertEquals("Il numero totale di arieti a sheepsburg dev'essere settato a 0, non ci sono arieti", 0, controllerPartita1.totaleArietiInTipoDiTerreno(6));

		//test su terreno di tipo 0
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 0 e c'è un ariete per ogni regione di tipo 0", 3, controllerPartita1.totaleArietiInTipoDiTerreno(0));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione00);
		assertEquals("Il numero totale di arieti nella regione00 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 0 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(0));

		//test su terreno di tipo 1
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 1 e c'è un ariete per ogni regione di tipo 1", 3, controllerPartita1.totaleArietiInTipoDiTerreno(1));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione05);
		assertEquals("Il numero totale di arieti nella regione05 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 1 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(1));

		//test su terreno di tipo 2
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 2 e c'è un ariete per ogni regione di tipo 2", 3, controllerPartita1.totaleArietiInTipoDiTerreno(2));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione01);
		assertEquals("Il numero totale di arieti nella regione01 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 2 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(2));
		
		//test su terreno di tipo 3
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 3 e c'è un ariete per ogni regione di tipo 3", 3, controllerPartita1.totaleArietiInTipoDiTerreno(3));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione14);
		assertEquals("Il numero totale di arieti nella regione14 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 3 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(3));

		//test su terreno di tipo 4
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 4 e c'è un ariete per ogni regione di tipo 4", 3, controllerPartita1.totaleArietiInTipoDiTerreno(4));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione10);
		assertEquals("Il numero totale di arieti nella regione10 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 4 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(4));

		//test su terreno di tipo 5
		assertEquals("Il numero totale di arieti dev'essere settato a 3, ci sono 3 regioni di tipo 5 e c'è un ariete per ogni regione di tipo 5", 3, controllerPartita1.totaleArietiInTipoDiTerreno(5));
		controllerPartita1.getControllerTurno().incrementaTotaleArietiInRegione(regione12);
		assertEquals("Il numero totale di arieti nella regione12 è stato incrementato di uno quindi il numero di arieti nel tipo di terreno 5 sale di uno", 4, controllerPartita1.totaleArietiInTipoDiTerreno(5));
	}
	
	@Test
	public void testTotalePecoreInTipoDiTerreno() {
		//test su tipo di terreno 6, tipo di terreno associato a sheepsburg
		assertEquals("La pecora nera dev'essere posizionata a Sheepsburg", sheepsburg, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il numero totale di pecore dev'essere settato a 2, c'è una sola pecora nera che però vale 2 pecore bianche per il punteggio", 2, controllerPartita1.totalePecoreInTipoDiTerreno(6));
		
		//test su terreno di tipo 0
		assertEquals("Il totale delle pecore nel tipo di terreno 0 dev'essere 3. Ci sono 3 regioni di tipo 0, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(0));
		statoPartita1.setPosizionePecoraNera(regione00);
		assertEquals("La pecora nera dev'essere nella regione00", regione00, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 0 dev'essere salito a 5. Oltre alle pecore bianche, nella regione00 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(0));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione00);
		assertEquals("Il totale delle pecore nel tipo di terreno 0 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione00, regione di tipo 0", 6, controllerPartita1.totalePecoreInTipoDiTerreno(0));
		
		//test su terreno di tipo 1
		assertEquals("Il totale delle pecore nel tipo di terreno 1 dev'essere 3. Ci sono 3 regioni di tipo 1, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(1));
		statoPartita1.setPosizionePecoraNera(regione05);
		assertEquals("La pecora nera dev'essere nella regione05", regione05, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 1 dev'essere salito a 5. Oltre alle pecore bianche, nella regione05 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(1));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione05);
		assertEquals("Il totale delle pecore nel tipo di terreno 1 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione05, regione di tipo 1", 6, controllerPartita1.totalePecoreInTipoDiTerreno(1));
		
		//test su tipo terreno di tipo 2
		assertEquals("Il totale delle pecore nel tipo di terreno 2 dev'essere 3. Ci sono 3 regioni di tipo 2, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(2));
		statoPartita1.setPosizionePecoraNera(regione01);
		assertEquals("La pecora nera dev'essere nella regione01", regione01, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 2 dev'essere salito a 5. Oltre alle pecore bianche, nella regione01 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(2));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione01);
		assertEquals("Il totale delle pecore nel tipo di terreno 2 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione01, regione di tipo 2", 6, controllerPartita1.totalePecoreInTipoDiTerreno(2));
		
		//test su tipo terreno di tipo 3
		assertEquals("Il totale delle pecore nel tipo di terreno 3 dev'essere 3. Ci sono 3 regioni di tipo 2, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(3));
		statoPartita1.setPosizionePecoraNera(regione14);
		assertEquals("La pecora nera dev'essere nella regione14", regione14, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 3 dev'essere salito a 5. Oltre alle pecore bianche, nella regione01 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(3));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione14);
		assertEquals("Il totale delle pecore nel tipo di terreno 3 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione14, regione di tipo 3", 6, controllerPartita1.totalePecoreInTipoDiTerreno(3));
		
		//test su tipo terreno di tipo 4
		assertEquals("Il totale delle pecore nel tipo di terreno 4 dev'essere 3. Ci sono 3 regioni di tipo 2, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(4));
		statoPartita1.setPosizionePecoraNera(regione10);
		assertEquals("La pecora nera dev'essere nella regione10", regione10, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 4 dev'essere salito a 5. Oltre alle pecore bianche, nella regione01 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(4));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione10);
		assertEquals("Il totale delle pecore nel tipo di terreno 4 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione10, regione di tipo 4", 6, controllerPartita1.totalePecoreInTipoDiTerreno(4));
		
		//test su tipo terreno di tipo 5
		assertEquals("Il totale delle pecore nel tipo di terreno 5 dev'essere 3. Ci sono 3 regioni di tipo 2, e hanno una pecora bianca ciascuna che vale 1 e nessuna pecora nera", 3, controllerPartita1.totalePecoreInTipoDiTerreno(5));
		statoPartita1.setPosizionePecoraNera(regione12);
		assertEquals("La pecora nera dev'essere nella regione12", regione12, statoPartita1.getPosizionePecoraNera());
		assertEquals("Il totale delle pecore nel tipo di terreno 5 dev'essere salito a 5. Oltre alle pecore bianche, nella regione01 è stata messa una pecora nera che vale due", 5, controllerPartita1.totalePecoreInTipoDiTerreno(5));
		controllerPartita1.getControllerTurno().incrementaTotalePecoreBiancheInRegione(regione12);
		assertEquals("Il totale delle pecore nel tipo di terreno 5 dev'essere salito a 6. E' stata aggiunta una pecora bianca nella regione12, regione di tipo 5", 6, controllerPartita1.totalePecoreInTipoDiTerreno(5));
	}
	
	@Test 
	public void testTrovaGiocatoreDelTurnoSuccessivo() {
		//primo test
		giocatoreTurnoSuccessivo = controllerPartita1.trovaGiocatoreDelTurnoSuccessivo(giocatore1);
		assertEquals("Il giocatore del turno successivo dev'essere il giocatore2", giocatore2, giocatoreTurnoSuccessivo);
		
		giocatoreTurnoSuccessivo = controllerPartita1.trovaGiocatoreDelTurnoSuccessivo(giocatore2);
		assertEquals("Il giocatore del turno successivo dev'essere il giocatore3", giocatore3, giocatoreTurnoSuccessivo);
		
		giocatoreTurnoSuccessivo = controllerPartita1.trovaGiocatoreDelTurnoSuccessivo(giocatore3);
		assertEquals("Il giocatore del turno successivo dev'essere il giocatore1", giocatore1, giocatoreTurnoSuccessivo);
		
		//secondo test con due giocatori
		giocatoreTurnoSuccessivo = controllerPartita2.trovaGiocatoreDelTurnoSuccessivo(giocatore4);
		assertEquals("Il giocatore del turno successivo dev'essere il giocatore5", giocatore5, giocatoreTurnoSuccessivo);
		
		giocatoreTurnoSuccessivo = controllerPartita2.trovaGiocatoreDelTurnoSuccessivo(giocatore5);
		assertEquals("Il giocatore del turno successivo dev'essere il giocatore4", giocatore4, giocatoreTurnoSuccessivo);
	}
	
	@Test
	public void testTrovaPunteggioMassimo() {
		assertEquals("Il giocatore con punteggio massimo dev'essere il giocatore8", giocatore8.getPunteggioTotalizzato(), controllerPartita3.trovaPunteggioMassimo());
	}
	
	@Test
	public void testUpdate() {
		
		
		MossaSelezionata eventoMossaScelta = new MossaSelezionata(); 
		eventoMossaScelta.setMossaSelezionata("MUOVI_PEDINA");
		controllerPartita1.update(null, eventoMossaScelta);
		assertEquals("L'attributo mossaScelta è = MUOVI_PEDINA","MUOVI_PEDINA", controllerPartita1.getControllerTurno().getMossaScelta()); 
		
		StradaSelezionata eventoStradaSelezionata = new StradaSelezionata(); 
		eventoStradaSelezionata.setIdStrada("Strada35");
		controllerPartita1.update(null, eventoStradaSelezionata);
		assertEquals("Il numero di mosse rimanenti è sceso a 2", 2, statoPartita1.getTurno().getNumeroMosseRimanenti()); 
		
		controllerPartita1.getControllerTurno().resettaValori();
		
		MossaSelezionata eventoMossaScelta2 = new MossaSelezionata(); 
		eventoMossaScelta2.setMossaSelezionata("MUOVI_ANIMALE");
		controllerPartita1.update(null, eventoMossaScelta2);
		assertEquals("L'attributo mossaScelta è = MUOVI_ANIMALE","MUOVI_ANIMALE", controllerPartita1.getControllerTurno().getMossaScelta()); 
		
		statoPartita1.getTurno().setPosizioneGiocatore((Strada)statoPartita1.getMappa().getMappaVertici().get("Strada00"));
		RegioneSelezionata eventoRegioneSelezionata = new RegioneSelezionata(); 
		eventoRegioneSelezionata.setIdRegione("Regione00");
		controllerPartita1.update(null,  eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		
		AnimaleSelezionato eventoAnimaleSelezionato = new AnimaleSelezionato(); 
		eventoAnimaleSelezionato.setAnimaleSelezionato("PECORA_BIANCA");
		controllerPartita1.update(null, eventoAnimaleSelezionato);
		assertEquals("Il numero di mosse rimanenti è sceso a 1", 1,  statoPartita1.getTurno().getNumeroMosseRimanenti());
		
		statoPartita1.getTurno().setNumeroMosseRimanenti(3);
		
		controllerPartita1.update(null, eventoMossaScelta2);
		assertEquals("L'attributo mossaScelta è = MUOVI_ANIMALE","MUOVI_ANIMALE", controllerPartita1.getControllerTurno().getMossaScelta()); 
		
		statoPartita1.getTurno().setPosizioneGiocatore((Strada)statoPartita1.getMappa().getMappaVertici().get("Strada00"));
		controllerPartita1.update(null,  eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		
		AnimaleSelezionato eventoAnimaleSelezionato2 = new AnimaleSelezionato(); 
		eventoAnimaleSelezionato2.setAnimaleSelezionato("ARIETE");
		controllerPartita1.update(null, eventoAnimaleSelezionato2);
		assertEquals("Il numero di mosse rimanenti è sceso a 2", 2,  statoPartita1.getTurno().getNumeroMosseRimanenti());
		

		statoPartita1.setPosizionePecoraNera(regione00);
		controllerPartita1.update(null, eventoMossaScelta2);
		assertEquals("L'attributo mossaScelta è = MUOVI_ANIMALE","MUOVI_ANIMALE", controllerPartita1.getControllerTurno().getMossaScelta()); 
		
		statoPartita1.getTurno().setPosizioneGiocatore((Strada)statoPartita1.getMappa().getMappaVertici().get("Strada00"));
		controllerPartita1.update(null,  eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		
		AnimaleSelezionato eventoAnimaleSelezionato3 = new AnimaleSelezionato(); 
		eventoAnimaleSelezionato3.setAnimaleSelezionato("PECORA_NERA");
		controllerPartita1.update(null, eventoAnimaleSelezionato2);
		assertEquals("Il numero di mosse rimanenti è sceso a 1", 1,  statoPartita1.getTurno().getNumeroMosseRimanenti());
		
		statoPartita1.getTurno().setNumeroMosseRimanenti(3);
		
		MossaSelezionata eventoMossaScelta3 = new MossaSelezionata(); 
		eventoMossaScelta3.setMossaSelezionata("ACQUISTA_TESSERA_TERRENO");
		controllerPartita1.update(null, eventoMossaScelta3);
		assertEquals("L'attributo mossaScelta è = ACQUISTA_TESSERA_TERRENO","ACQUISTA_TESSERA_TERRENO", controllerPartita1.getControllerTurno().getMossaScelta()); 
		
		TesseraTerrenoSelezionata eventoTesseraTerrenoSelezionata = new TesseraTerrenoSelezionata(); 
		eventoTesseraTerrenoSelezionata.setTipoDiTerreno(0);
		controllerPartita1.update(null, eventoTesseraTerrenoSelezionata);
		assertEquals("Il numero di mosse rimanenti è sceso a 2", 2,  statoPartita1.getTurno().getNumeroMosseRimanenti());
		
		MossaSelezionata eventoMossaScelta4 = new MossaSelezionata(); 
		eventoMossaScelta4.setMossaSelezionata("EFFETTUA_ACCOPPIAMENTO");
		controllerPartita1.update(null, eventoMossaScelta4);
		assertEquals("L'attributo mossaScelta è = EFFETTUA_ACCOPPIAMENTO","EFFETTUA_ACCOPPIAMENTO", controllerPartita1.getControllerTurno().getMossaScelta());
		
		controllerPartita1.update(null,  eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		
		statoPartita1.getTurno().setNumeroMosseRimanenti(3);
		
		MossaSelezionata eventoMossaScelta5 = new MossaSelezionata(); 
		eventoMossaScelta5.setMossaSelezionata("EFFETTUA_SPARATORIA");
		controllerPartita1.update(null, eventoMossaScelta5);
		assertEquals("L'attributo mossaScelta è = EFFETTUA_SPARATORIA","EFFETTUA_SPARATORIA", controllerPartita1.getControllerTurno().getMossaScelta());
		
		controllerPartita1.update(null,  eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		
		controllerPartita1.update(null,eventoAnimaleSelezionato);
		assertEquals("L'attributo animaleScelto = PECORA_BIANCA", "PECORA_BIANCA", controllerPartita1.getControllerTurno().getAnimaleScelto()); 
		
		statoPartita1.getTurno().setNumeroMosseRimanenti(3);
		
		controllerPartita1.update(null, eventoMossaScelta5);
		assertEquals("L'attributo mossaScelta è = EFFETTUA_SPARATORIA","EFFETTUA_SPARATORIA", controllerPartita1.getControllerTurno().getMossaScelta());
		controllerPartita1.update(null, eventoRegioneSelezionata);
		assertEquals("L'attributo regioneScelta è = regione00",regione00, controllerPartita1.getControllerTurno().getRegioneScelta()); 
		controllerPartita1.update(null, eventoAnimaleSelezionato2);
		assertEquals("L'attributo animaleScelto = ARIETE", "ARIETE", controllerPartita1.getControllerTurno().getAnimaleScelto()); 
		
		statoPartita1.getTurno().setNumeroMosseRimanenti(2);
		controllerPartita1.getControllerTurno().resettaValori();
		controllerPartita1.update(null, eventoMossaScelta4);
		eventoRegioneSelezionata.setIdRegione("Regione41");
		controllerPartita1.update(null, eventoRegioneSelezionata);
		assertEquals("Il numero di mosse rimanenti è sempre 2, perchè la regione selezionata non è valida",  2, statoPartita1.getTurno().getNumeroMosseRimanenti());
		
		controllerPartita1.getControllerTurno().resettaValori();
		ConfermaRicezioneAvvisoNoMosse eventoNoMosse = new ConfermaRicezioneAvvisoNoMosse(); 
		controllerPartita1.update(null, eventoNoMosse);
		assertEquals("Il giocatore del turno non è più giocatore 1", true, statoPartita1.getTurno().getGiocatore()!=giocatore1);
		
		

	}
}
