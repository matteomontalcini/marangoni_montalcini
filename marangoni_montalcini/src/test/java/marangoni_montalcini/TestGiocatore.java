package marangoni_montalcini;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Giocatore;

import org.junit.Before;
import org.junit.Test;

public class TestGiocatore {

	//variabili per il test del costruttore Giocatore
	private Date data = new Date(); 
	private Date data1 = new Date(); 
	private Giocatore giocatoreTest;
	//variabili per il test di inizializzaTessereTerrenoInPossesso
	private List<Integer> tessereTerrenoInPossessoTest = new ArrayList<Integer>(); 
	
	@Before
	public void setUpTestGiocatore(){
		//2 è momentaneo: è il numero giocatori
		giocatoreTest = new Giocatore("Matteo", data, 2);
		data.setDate(20);
		data.setMonth(4);
		data.setYear(114); 
		data1.setDate(21); 
		data1.setMonth(4);
		data1.setYear(114);
		//aggiungo esattamente 6 "0" ad una lista di test, proprio come dovrebbe fare
		//il metodo inizializzaTesssereTerrenoInPossesso
		tessereTerrenoInPossessoTest.add(0,0);
		tessereTerrenoInPossessoTest.add(0,0);
		tessereTerrenoInPossessoTest.add(0,0);
		tessereTerrenoInPossessoTest.add(0,0);
		tessereTerrenoInPossessoTest.add(0,0);
		tessereTerrenoInPossessoTest.add(0,0);
	}
	
	@Test
	public void testGiocatore() {
		assertEquals("Il nome del giocatore dev'essere Matteo", "Matteo", giocatoreTest.getNomeGiocatore());
		assertEquals("La data dev'essere uguale a quella inserita", data, giocatoreTest.getUltimaCarezza());
	}
	
	@Test
	public void testInizializzaTessereTerrenoInPossesso() {
		Giocatore giocatoreTest = new Giocatore("Matteo", data, 2);
		assertEquals("L'array tessereTerrenoInPossesso dev'essere composto da sei 0", tessereTerrenoInPossessoTest , giocatoreTest.getTessereTerrenoInPossesso());
	}

}
